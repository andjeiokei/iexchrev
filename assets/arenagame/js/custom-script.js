$(".back-top").click(function() {
  $("html, body").animate({ scrollTop: 0 }, "slow");
  return false;
});
$( document ).ready(function() {

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.sticky-stopper');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function(){ // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
          $sticky.css({ position: 'absolute', top: diff });
      } else if (stickyTop < windowTop+stickOffset) {
          $sticky.css({ position: 'fixed', top: stickOffset });
      } else {
          $sticky.css({position: 'absolute', top: 'initial', right: '0'});
      }
    });

  }
});

// SLIDER SECTION
$(document).ready(function() {
  var owl = $('.owl-carousel');

  owl.owlCarousel({
    items: 1,
    loop: true,
    autoplay: true,
    autoplayTimeout: 5000,
    nav: true,
    margin: 5,
  });

  owl.on('changed.owl.carousel', function(event) {
      var item = event.item.index - 2;     // Position of the current item
      $('.B-logoImg').removeClass('animated fadeInRight');
      $('.owl-item').not('.cloned').eq(item).find('.B-logoImg').addClass('animated fadeInRight');
      $('.B-buy-btn').removeClass('animated fadeInRight');
      $('.owl-item').not('.cloned').eq(item).find('.B-buy-btn').addClass('animated fadeInRight');
      $('.sf-banner img').removeClass('hero-bkg-animated');
      $('.owl-item').not('.cloned').eq(item).find('.sf-banner img').addClass('hero-bkg-animated');
  });

});


