#!/bin/bash

DIR=$(cd "$( dirname "$0" )" && pwd)
F=$DIR/transactions/btc/${1}

echo ${1} >> ${F}
chown -R www-data:www-data $DIR/transactions/btc
php -q btctxs.php ${1}
