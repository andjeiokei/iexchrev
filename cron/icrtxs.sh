#!/bin/bash

DIR=$(cd "$( dirname "$0" )" && pwd)
F=$DIR/transactions/icr/${1}

echo ${1} >> ${F}
chown -R www-data:www-data $DIR/transactions/icr
php -q icrtxs.php ${1}