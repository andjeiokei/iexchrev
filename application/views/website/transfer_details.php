<?php
$settings = $this->db->select("*")
    ->get('setting')
    ->row();

    $data = json_decode($v->data);
?>
<div class="invoice">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div id="printableArea">
                   <div class="row mb-5">
                        <div class="col-sm-6">
                            <img src="<?php echo base_url(!empty($settings->logo)?$settings->logo:"assets/images/icons/logo.png"); ?>" class="img-responsive" alt="">
                            <br>
                            <address>
                                <strong><?= $settings->title ?></strong><br>
                                <?php echo $settings->description;?><br>
                            </address>
                        </div>
                        <div class="col-sm-6 text-right">
                            <h4 class="mb-3">Transfer No : <?php echo $this->uri->segment(2)?></h4>
                            <div><?php echo date('d-M-Y');?></div>
                            <address>
                                <strong><?php echo $my_info->first_name.' '.$my_info->last_name;?></strong><br>
                                <?php echo $my_info->email;?><br>
                                <?php echo $my_info->phone;?><br>
                                <abbr title="Phone"><?php echo display('account') ?> :</abbr> <?php echo $my_info->user_id;?>
                            </address>
                        </div>
                    </div> 
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th><?php echo display('receiver_name')?></th>
                                    <td><?php echo $u->first_name.' '. $u->last_name;?></td>
                                </tr>
                                <tr>
                                    <th><?php echo display('email');?></th>
                                    <td><?php echo $u->email;?></td>
                                </tr>
                                <tr>
                                    <th><?php echo display('user_id');?></th>
                                    <td><?php echo $u->user_id;?></td>
                                </tr>
                                <tr>
                                    <th><?php echo display('fees');?></th>
                                    <td><?php echo $data->fees; ?></td>
                                </tr>
                                <tr>
                                    <th><?php echo display('transfer_amount');?></th>
                                    <td><?php echo $data->currency_symbol.' '.$data->amount;?></td>
                                </tr>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="text-right">
                   <button type="button" class="btn btn-info" onclick="printContent('printableArea')"><span class="fa fa-print"></span></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    //print a div
    function printContent(el){
        var restorepage  = $('body').html();
        var printcontent = $('#' + el).clone();
        $('body').empty().html(printcontent);
        window.print();
        $('body').html(restorepage);
        location.reload();
    }
</script>