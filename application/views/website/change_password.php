<div class="password_change-content">
    <!-- alert message -->
    <?php if ($this->session->flashdata('message') != null) {  ?>
    <div class="alert alert-info alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('message'); ?>
    </div> 
    <?php } ?>
        
    <?php if ($this->session->flashdata('exception') != null) {  ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo $this->session->flashdata('exception'); ?>
    </div>
    <?php } ?>
        
    <?php if (validation_errors()) {  ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?php echo validation_errors(); ?>
    </div>
    <?php } ?> 
    <!-- /.alert message -->
    <h3 class="mb-3"><?php echo display('change_password');?></h3>
    <?php echo form_open("change-password") ?>
        <div class="form-group">
            <label><?php echo display("enter_old_password") ?> *</label>
            <input type="password" class="form-control" value="<?php echo (isset($set_old->old_pass)?$set_old->old_pass:'');?>" name="old_pass" placeholder="<?php echo display("enter_old_password") ?>">
        </div>
        <div class="form-group">
            <label><?php echo display("enter_new_password") ?> *</label>
            <input type="password"  class="form-control" value="<?php echo (isset($set_old->new_pass)?$set_old->new_pass:'');?>" name="new_pass" placeholder="<?php echo display("enter_new_password") ?>">
        </div>
        <div class="form-group">
            <label><?php echo display("enter_confirm_password") ?> *</label>
            <input type="password"  class="form-control" name="confirm_pass" value="<?php echo (isset($set_old->confirm_pass)?$set_old->confirm_pass:'');?>" placeholder="<?php echo display("enter_confirm_password") ?>">
        </div>
        
        <div class=" m-b-15">
            <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("change") ?></button>
            <a href="<?php echo base_url('profile');?>" class="btn btn-danger"><?php echo display('cancel')?></a>
        </div>
    <?php echo form_close();?>
</div>