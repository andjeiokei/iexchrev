
<!-- /.End of tricker -->
<div class="price-spike">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
            
            <?php if($this->session->flashdata('error_message')){?>
                <div class="alert alert-danger">
                    <strong>Sorry!</strong><ul><?php echo $this->session->flashdata('error_message');?></ul>
                </div>
            <?php }?>

            <?php if($this->session->flashdata('success_message')){?>
                <div class="alert alert-success">
                    <strong>Success!</strong><ul><?php echo $this->session->flashdata('success_message');?></ul>
                </div>
            <?php }?>

            </div>
        </div>
    </div>
</div>
<!--  ./End of price spike -->
