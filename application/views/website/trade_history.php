<style type="text/css">
        .table td, .table th {
            vertical-align: unset;
        }
</style>
<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/global-style.css') ?>" rel="stylesheet" type="text/css">

<div class="main-container">
    <div class="header-flex">
        <div class="header-title">
            <h4><?php echo display('trade_history') ?></h4>
        </div>
        <div class="ml_banner_img" style="width: 728px; height: 90px;">
            <?php echo adshow(array(
                'width'         => 728,
                'height'        => 90,
                'client'        => 'ca-pub-2238365415915736',
                'slot'          => '6978414426',
                'format'        => 'auto',
                'responsive'    => 'auto',
            ));?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
           
            <div class="dpt-row">

                <?php /*<div class="dpt-col-90">
                    
                    <!-- /.end of alert message -->
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr class="table-bg">
                                    <th><?php echo display('trade') ?></th>
                                    <th><?php echo display('rate') ?></th>
                                    <th><?php echo display('required_qty') ?></th>
                                    <th><?php echo display('available_qty') ?></th>
                                    <th><?php echo display('required_amount') ?></th>
                                    <th><?php echo display('available_amount') ?></th>
                                    <th><?php echo display('market') ?></th>
                                    <th><?php echo display('open') ?></th>
                                    <th><?php echo display('complete_qty') ?></th>
                                    <th><?php echo display('complete_amount') ?></th>
                                    <th><?php echo display('trade_time') ?></th>
                                    <th><?php echo display('status') ?></th>
                                </tr>
                            </thead>

                            <tbody id="usertradeHistory">
                                <?php  foreach ($user_trade_history as $key => $value) { ?>
                                	<tr>
                                		<td><?php echo $value->bid_type ?></td>
                                		<td><?php echo $value->bid_price ?></td>
                                        <td><?php echo $value->bid_qty ?></td>
                                		<td><?php echo $value->bid_qty_available ?></td>
                                		<td><?php echo $value->total_amount ?></td>
                                        <td><?php echo $value->amount_available ?></td>
                                        <td><?php echo $value->market_symbol ?></td>
                                        <td><?php echo $value->open_order ?></td>
                                        <td><?php echo $value->complete_qty ?></td>
                                        <td><?php echo $value->complete_amount ?></td>
                                        <td><?php echo $value->success_time ?></td>
                                		<td><?php echo $value->status==0?"<p class='bg-warning text-white text-center pb-1 pl-1 pr-1 mb-1 mt-1'>Canceled</p>":($value->status==1?"<p class='bg-success text-white text-center pb-1 pl-1 pr-1 mb-1 mt-1'>Completed</p>":"<p class='bg-primary text-white text-center pb-1 pl-1 pr-1 mb-1 mt-1'>Running</p>") ?></td>
                                	</tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                */?>
                


                <div class="dpt-col-4">
                
                <?php echo form_open('trade-history','id="frm_trade_history"'); ?>
                    
                    <div class="form-flexinline">
                        
                        <div class="frm-grup-flex80">
                            <div class="frm-grup-flex dateBx">
                                <label>Date:</label>
                                <div class="input-group date">
                                    <input type='text' class="form-control datepicker" name="start_date" value="<?php echo $search_data['start_date'];?>"/>
                                    <span class="input-group-addon datepicker_icon_js">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <label class="clrfnt">-</label>
                                <div class="input-group date">
                                    <input type='text' class="form-control datepicker" name="end_date" value="<?php echo $search_data['end_date'];?>"/>
                                    <span class="input-group-addon datepicker_icon_js">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="frm-grup-flex pairBx">
                                <label>Pair:</label>
                                <div class="input-group">
                                    <input type='text' class="form-control" placeholder="Coin" name="first_pair" value="<?php echo $search_data['first_pair'];?>"/>
                                </div>
                                <label class="clrfnt">/</label>
                                <div class="input-group">
                                    <select class="form-control" name="second_pair" id="second_pair">
                                    <option selected value="">All</option>
                                    <?php if(!empty($coin_list)){
                                        foreach($coin_list as $coin_details){
                                        ?>
                                        <option value="<?php echo $coin_details->symbol?>" <?php echo ($coin_details->symbol == $search_data['second_pair'])? "selected":"";?>><?php echo $coin_details->symbol?></option>
                                    <?php 
                                        }
                                    }?>
                                    </select>
                                </div>
                            </div>
                            <div class="frm-grup-flex sideBx">
                                <label>Side:</label>
                                <div class="input-group">
                                    <select class="form-control" name="side">
                                    <option value="">All</option>
                                    <option value="BUY" <?php echo ($search_data['side'] == 'BUY')? "selected":""?>>Buy</option>
                                    <option value="SELL" <?php echo ($search_data['side'] == 'SELL')? "selected":""?>>Sell</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-search" id="search" name="search" value="search">Search</button>
                            <button type="button" class="btn btn-reset" name="reset" id="reset">Reset</button>
                            <div class="checkbox">
                                <label><input type="checkbox" name="is_canclled_hide" value="1" id="is_canclled_hide" <?php echo ($search_data['is_canclled_hide'] == 1)? "checked":"";?>> Hide all canceled</label>
                            </div>
                        </div>
                        
                        <a class="btnpdf" href="<?php echo base_url('order_history_csv');?>">Export Complete Order History <i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                    </div>
                    <div class="hstTable ordrTable mrt30">
                        <table cellpadding="0" cellspacing="0" border="0" id="order_history_list">
                            <tr>
                                <th><?php echo display('date');?></th> 
                                <th><?php echo display('coinpair');?></th> 
                                <th><?php echo display('type');?></th> 
                                <th>Side</th> 
                                <th>Average</th>
                                <th><?php echo display('price');?></th> 
                                <th>Filled</th>
                                <th><?php echo display('amount');?></th>
                                <th><?php echo display('total');?></th>
                                <th><?php echo display('status');?></th>
                                <th>Operation</th>
                            </tr>
                            <?php  
                            if(!empty($user_trade_history)){
                            foreach ($user_trade_history as $key => $value) { ?>
                            <tr class="<?php echo ($value->status == 0)? 'canceled-row':''; ?>">
                                <td><?php echo date("m-d-Y H:i:s", strtotime($value->open_order));?></td>
                                <td><?php echo str_replace("_","/",$value->market_symbol);?></td>
                                <td> </td>
                                <td class="<?php echo ( strtolower($value->bid_type) == 'buy')? 'colorgreen':'colororange'?>"><?php echo ucfirst( strtolower( $value->bid_type) ); ?></td>
                                <td> </td>
                                <td><?php echo $value->bid_price;?></td>
                                <td><?php echo $value->amount_available;?></td>
                                <td><?php echo $value->bid_qty;?></td>
                                <td><?php echo $value->complete_amount;?></td>
                                <td><?php 
                                    switch($value->status){
                                        case 0:
                                            echo "<p class='bg-warning text-white text-center pb-1 pl-1 pr-1 mb-1 mt-1'>Canceled</p>";
                                            break;
                                        
                                        case 1:
                                            echo "<p class='bg-success text-white text-center pb-1 pl-1 pr-1 mb-1 mt-1'>Completed</p>";
                                            break;

                                        case 2:
                                            echo "<p class='bg-primary text-white text-center pb-1 pl-1 pr-1 mb-1 mt-1'>Running</p>";
                                            break;

                                        default:
                                            echo "Unknown";
                                            break;
                                    } 
                                    
                                    ?></td>
                                <td><a href="#" class="btnSmlDels">Details</a></td>
                            </tr>
                            <?php }
                            }?>
                        </table>
                    </div>

                    <?php echo $links;?>

                <?php echo form_close();?>  
 
                </div>


                <div class="dpt-col-3 sticky" style="width: 120px; height: 600px;">
                    <?php echo adshow(array(
                        'width'         => 120,
                        'height'        => 600,
                        'client'        => 'ca-pub-2238365415915736',
                        'slot'          => '6978414426',
                        'format'        => 'auto',
                        'responsive'    => 'auto',
                    ));?>
                </div>

            </div>
        <!-- End of Trade History -->
        </div>
    </div>
</div>