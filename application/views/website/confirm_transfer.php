<div class="container">
    <div class="row">
        <?php 
            $data = json_decode($v->data);
        ?>
        <div class="col-lg-6 offset-lg-3">
            <div class="confirm-transfer">
                <?php   $att = array('name'=>'verify'); echo form_open('#',$att); ?>
                <dl class="row">
                  <dt class="col-6"><?php echo display('receiver_name')?></dt>
                  <dd class="col-6"><?php echo $user->first_name.' '. $user->last_name;?></dd>

                  <dt class="col-6"><?php echo display('email');?></dt>
                  <dd class="col-6"><?php echo $user->email;?></dd>

                  <dt class="col-6"><?php echo display('user_id');?></dt>
                  <dd class="col-6"><?php echo $user->user_id;?></dd>

                  <dt class="col-6"><?php echo display('transfer_amount');?></dt>
                  <dd class="col-6"><?php echo $data->currency_symbol.' '.$data->amount;?></dd>

                  <dt class="col-6"><?php echo display('fees');?></dt>
                  <dd class="col-6"><?php echo $data->fees; ?></dd>

                  <dt class="col-6"><?php echo display('enter_verify_code');?></dt>
                  <dd class="col-6"><input class="form-control" type="text" name="code" id="code"></dd>
                </dl>
                <div class="text-center">
                    <button type="button" onclick="transfer('<?php echo $v->id;?>');" class="btn btn-kingfisher-daisy"><?php echo display('confirm') ?></button>
                    <button type="button" class="btn btn-danger"><?php echo display('cancle') ?></button>
                </div>
                <?php echo form_close();?>
            </div>    
        </div>
    </div>
</div>

