        <div class="form-body">

            <div class="form-content login">
                <div class="row">
                    <div class="col-md-12">
                        <!-- alert message -->
                        <?php if ($this->session->flashdata('message') != null) {  ?>
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $this->session->flashdata('message'); ?>
                        </div> 
                        <?php } ?>
                            
                        <?php if ($this->session->flashdata('exception') != null) {  ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $this->session->flashdata('exception'); ?>
                        </div>
                        <?php } ?>
                            
                        <?php if (validation_errors()) {  ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo validation_errors(); ?>
                        </div>
                        <?php } ?> 
                    </div>
                </div>
                <h3 class="user-login-title"><?php echo display('account_login') ?></h3>
                <div class="user-login">
                    <?php echo form_open('login','id="loginForm" '); ?>                        
                        <div class="form-group">
                            <label for="e_name"><i class="fas fa-user"></i><?php echo display('email') ?></label>
                            <input type="text" class="form-control" id="e_name" name="luseremail" aria-describedby="e_nameHelp" placeholder="Email" required>
                            <small id="e_nameHelp" class="form-text text-muted"><?php echo display('we_never_share_your_email_with_anyone_else') ?></small>
                        </div>
                        <div class="form-group">
                            <label for="pass"><i class="fas fa-lock"></i> <?php echo display('password') ?></label>
                            <input type="password" class="form-control" id="pass" name="lpassword" placeholder="Password" required>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="rememberMe">
                                <label class="custom-control-label" for="rememberMe"> <?php echo display('remember_me') ?></label>
                            </div>
                            <div class="foreget-password">
                                <a href="#" data-toggle="modal" data-target="#forgotModal" class="forgot"><?php echo display('forgot_password') ?>?</a>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display('submit') ?></button>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>


<!-- Modal -->
<div id="forgotModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-body">
            <div class="form-body">
                <div class="form-content forgotlogin">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="user-login-title"><?php echo display('account_login') ?></h3>
                    <div class="user-login">
                        <?php echo form_open('forgotPassword','id="forgotPassword" '); ?>
                            <div class="form-group">
                                <label for="e_name"><i class="fas fa-user"></i><?php echo display('email') ?></label>
                                <input type="text" class="form-control" id="e_name" name="luseremail" aria-describedby="e_nameHelp" placeholder="example@mail.com" required>
                                <small id="e_nameHelp" class="form-text text-muted"><?php echo display('we_never_share_your_email_with_anyone_else') ?></small>
                            </div>
                            <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display('submit') ?></button>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
    </div>

  </div>
</div>