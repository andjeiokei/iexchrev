        <div class="payout-content">
            <div class="container">
                <div class="row">
                    
                    <div class="col-lg-4 offset-lg-4">
                        <!-- alert message -->
                        <?php if ($this->session->flashdata('message') != null) {  ?>
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $this->session->flashdata('message'); ?>
                        </div> 
                        <?php } ?>
                            
                        <?php if ($this->session->flashdata('exception') != null) {  ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $this->session->flashdata('exception'); ?>
                        </div>
                        <?php } ?>
                            
                        <?php if (validation_errors()) {  ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo validation_errors(); ?>
                        </div>
                        <?php } ?> 
                        <!-- /.alert message -->
                    
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Bitcoin Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_btc->currency_symbol=='BTC'?@$bitcoin_btc->wallet_id:''; ?>" type="text" required>
                                <input class="form-control" name="currency_symbol" value="BTC" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Bitcoincash Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_bch->currency_symbol=='BCH'?@$bitcoin_bch->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="BCH" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Litecoin Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_ltc->currency_symbol=='LTC'?@$bitcoin_ltc->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="LTC" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Dash Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_dash->currency_symbol=='DASH'?@$bitcoin_dash->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="DASH" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Dogecoin Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_doge->currency_symbol=='DOGE'?@$bitcoin_doge->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="DOGE" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Speedcoin Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_spd->currency_symbol=='SPD'?@$bitcoin_spd->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="SPD" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Reddcoin Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_rdd->currency_symbol=='RDD'?@$bitcoin_rdd->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="RDD" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Potcoin Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_pot->currency_symbol=='POT'?@$bitcoin_pot->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="POT" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Feathercoin Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_ftc->currency_symbol=='FTC'?@$bitcoin_ftc->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="FTC" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Vertcoin Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_vtc->currency_symbol=='VTC'?@$bitcoin_vtc->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="VTC" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Peercoin Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_ppc->currency_symbol=='PPC'?@$bitcoin_ppc->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="PPC" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Monetaryunit Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_mue->currency_symbol=='MUE'?@$bitcoin_mue->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="MUE" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/bitcoin');?>
                            <label >Universalcurrency Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$bitcoin_unit->currency_symbol=='UNIT'?@$bitcoin_unit->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="UNIT" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>

                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/payeer');?>
                            <label>Payeer Wallet *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$payeer_btc->currency_symbol=='BTC'?@$payeer_btc->wallet_id:''; ?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="BTC" type="hidden">
                                <input class="form-control" name="currency_symbol1" value="USD" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div> 
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/paypal');?>
                            <label >Paypal *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$paypal->wallet_id;?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="USD" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div> 
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/stripe');?>
                            <label >Stripe *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$stripe->wallet_id;?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="USD" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div> 
                        <div class="mb-3">  
                            <?php echo form_open('payout-setting/phone');?>
                            <label ><?php echo display('mobile');?> *</label>
                            <div class="input-group">
                                <input class="form-control" name="wallet_id" value="<?php echo @$phone->wallet_id;?>" required type="text">
                                <input class="form-control" name="currency_symbol" value="USD" type="hidden">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display("update") ?></button>
                                </div>
                            </div>
                            <?php echo form_close();?>
                        </div>   
                    </div>

                </div>
            </div>
        </div>