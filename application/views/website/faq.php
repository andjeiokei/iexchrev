<?php
$cat_title1  = isset($lang) && $lang =="french"?$cat_info->cat_title1_fr:$cat_info->cat_title1_en;
$cat_title2  = isset($lang) && $lang =="french"?$cat_info->cat_title2_fr:$cat_info->cat_title2_en;
?>
        <div class="faq_content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="">
                            <h2 class="faq-title"><?php echo $cat_title1; ?></h2>
                            
                            <p class="faq-des"><?php echo $cat_title2; ?> </p>
                            <ul class="accordion">
                                <?php 
                                    $i=1; 
                                    foreach ($article as $key => $value) { ?>
                                     <li>
                                        <a><?php echo isset($lang) && $lang =="french"?$value->headline_fr:$value->headline_en; ?></a>
                                        <p><?php echo isset($lang) && $lang =="french"?$value->article1_fr:$value->article1_en; ?></p>
                                    </li>
                                <?php $i++; } ?>  
                            </ul>
                            <!-- / accordion -->
                        </div>
                    </div>
                </div>
            </div>
        </div>