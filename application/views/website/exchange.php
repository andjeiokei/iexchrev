<?php
$settings = $this->db->select("*")
    ->get('setting')
    ->row();

$market = $this->input->get('market');
$market = str_replace("_", "", $market);
?>
            <div class="exchange-content">
                <div class="container">
                    <div class="row">
                        
                        
                        <div class="col-md-2">
                            <div class="card orders">
                                <!-- <div class="card-header d-flex align-items-center justify-content-between">
                                    <div><?php echo display('sell_orders') ?></div>
                                    <div class="buy-sell-orders"><?php echo $coin_symbol[0] ?> <?php echo display('available') ?>: <span class="available_sell_coin"></span></div>
                                </div> -->
                                <div class="card-body">
                                    <div class="table-responsive sellRequest tableFixHead">
                                        <table class="table table-striped" >
                                            <thead>
                                                <tr class="table-bg">
                                                    <th><?php echo display('price') ?>(<?php echo $coin_symbol[0] ?>)</th>
                                                    <th class="text-right"><?php echo display('amount') ?>(<?php echo $coin_symbol[0] ?>)</th>
                                                    <th class="text-right"><?php echo display('total') ?>(<?php echo $coin_symbol[1] ?>)</th>
                                                </tr>
                                            </thead>
                                            <tbody id="selltrades">

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.End of Buy Orders table -->
                                </div>
                            </div>
                            <!-- /.End of sell order -->                            
                            <h5 class="text-center"><span class="price_updown">0.00</span></h5>
                            <div class="card orders">
                                <?php /*<div class="card-header d-flex align-items-center justify-content-between">
                                    <div><?php echo display('buy_orders') ?></div>
                                    <div class="buy-sell-orders"><?php echo $coin_symbol[0] ?> <?php echo display('available') ?>: <span class="available_buy_coin"></span> </div>
                                </div>*/?>
                                <div class="card-body">
                                    <div class="table-responsive buyOrder tableFixHead">
                                        <table class="table table-striped">
                                        <?php /*<thead>
                                                <tr class="table-bg">
                                                    <th><?php echo display('price') ?> <?php echo $coin_symbol[0] ?></th>
                                                    <th class="text-right"><?php echo $coin_symbol[0] ?> <?php echo display('amount') ?></th>
                                                    <th class="text-right"><?php echo display('total') ?>: (<?php echo $coin_symbol[1] ?>)</th>
                                                </tr>
                                            </thead>*/?>
                                            <tbody id="buytrades">
                                                 
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.End of Buy Orders table -->
                                </div>
                            </div>
                            <!-- /.End of buy order -->

                            <!-- <img src="<?php echo base_url("assets/website/img/add-3.png");?>" class="img-thumbnail" alt="..." style="margin-top: 15px;"> -->
                            <!-- <div class="ads_bottom">
                                <?php echo adshow(array(
                                    'width'         => 208,
                                    'height'        => 178,
                                    'client'        => 'ca-pub-2238365415915736',
                                    'slot'          => '9548452032',
                                    'format'        => false,
                                    'responsive'    => false,
                                ));?>
                            </div> -->
                            
                        </div>
                        <div class="col-lg-5">
                            <a href="<?php echo "https://www.intercrone.com/";?>" target="_blank">
                                <img src="<?php echo base_url("assets/website/img/add-2.png");?>" class="img-fluid" alt="">
                            </a>

                            <!-- <?php echo adshow(array(
                                'width'         => 0,
                                'height'        => 60,
                                'client'        => 'ca-pub-2238365415915736',
                                'slot'          => '6978414426',
                                'format'        => 'auto',
                                'responsive'    => 'auto',
                            ));?> -->

                            <div class="chart-panel mt-2" style="padding:0px 0px 0px 0px;">
                                <!-- <div class="row price-info">
                                    <div class="col-6 col-md-3">
                                        <div class="price-info-table">
                                            <span class="dollar"><?php echo $coin_symbol[0] ?></span>
                                            <span class="last-price coin-last-price">0.00</span>
                                            <span class="last-price-title"><?php echo display('last_price') ?></span>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-3">
                                        <div class="price-info-table">
                                            <span class="dollar"><?php echo $coin_symbol[0] ?></span>
                                            <span class="last-price coin-change-price positive">0.00%</span>
                                            <span class="last-price-title"><?php echo display('24hr_change') ?></span>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-3">
                                        <div class="price-info-table">
                                            <span class="dollar"><?php echo $coin_symbol[0] ?></span>
                                            <span class="last-price coin-price-high">0.00</span>
                                            <span class="last-price-title"><?php echo display('24hr_high') ?></span>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-3">
                                        <div class="price-info-table">
                                            <span class="dollar"><?php echo $coin_symbol[0] ?></span>
                                            <span class="last-price coin-price-low">0.00</span>
                                            <span class="last-price-title"><?php echo display('24hr_low') ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="total-volume">
                                        <?php echo display('24hr_volume') ?>: <span class='total_volume'></span> <?php echo $coin_symbol[0] ?> / <span class='price_updown'></span> <?php echo $coin_symbol[1] ?>
                                    </div>
                                </div> -->
                                <div id="exchangesChart"></div>

                                <?php /*?>
                                <!-- TradingView Widget BEGIN -->
                                <div class="tradingview-widget-container">
                                <div id="tradingview_e8501" style="height:395px; width:534px;"></div>
                                <!-- <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/NASDAQ-AAPL/" rel="noopener" target="_blank"><span class="blue-text">AAPL Chart</span></a> by TradingView</div> -->
                                <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                                <script type="text/javascript">
                                new TradingView.widget(
                                {
                                "autosize": true,
                                "symbol": "<?php echo $market;?>",
                                "interval": "30",
                                "timezone": "Etc/UTC",
                                "theme": "Light",
                                "style": "1",
                                "locale": "en",
                                "toolbar_bg": "#f1f3f6",
                                "enable_publishing": false,
                                "hide_top_toolbar": true,
                                "save_image": false,
                                "container_id": "tradingview_e8501"
                                }
                                );
                                </script>
                                </div>
                                <!-- TradingView Widget END -->
                                <?php */?>


                            </div>

                            <!-- /.End of exchange chart -->

    <script src="<?php echo base_url('assets/website/js/vendors.bundle.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/website/js/amcharts/amcharts.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/website/js/amcharts/serial.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/website/js/amcharts/amstock.js'); ?>" type="text/javascript"></script>

    <!-- Amchats js -->
    <script src="<?php echo base_url('assets/website/js/amcharts/plugins/dataloader/dataloader.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/website/js/amcharts/plugins/export/export.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/website/js/amcharts/patterns.js') ?>"></script>
    <script src="<?php echo base_url('assets/website/js/amcharts/dark.js') ?>"></script>

    <script>
      var chart = AmCharts.makeChart("exchangesChart", {
        "type": "stock",
        "color": "#000",
        "theme": "light",
        "dataSets": [{
        "title": "Market",
          "fieldMappings": [{
            "fromField": "open",
            "toField": "open"
          }, {
            "fromField": "price_high_24h",
            "toField": "high"
          }, {
            "fromField": "price_low_24h",
            "toField": "low"
          }, {
            "fromField": "close",
            "toField": "close"
          }, {
            "fromField": "total_coin_supply",
            "toField": "volume"
          }],
          "compared": false,
          "categoryField": "date",
          /**
           * data loader for data set data
           */
          "dataLoader": {
            "url": "<?php echo base_url('home/tradecharthistory'); ?>?market=<?php echo implode('_', $coin_symbol) ?>",
            "format": "json",
            "showCurtain": true,
            "showErrors": false,
            "async": true,
            "reverse": true,
            "delimiter": ",",
            "useColumnNames": true
         },
        }],
        "dataDateFormat": "YYYY-MM-DD",
        "panels": [{
            "title": "Value",
            "percentHeight": 70,
            "stockGraphs": [{
              "type": "candlestick",
              "id": "g1",
              "openField": "open",
              "closeField": "close",
              "highField": "high",
              "lowField": "low",
              "valueField": "close",
              "lineColor": "#7f8da9",
              "fillColors": "#7f8da9",
              "negativeLineColor": "#db4c3c",
              "negativeFillColors": "#db4c3c",
              "fillAlphas": 1,
              "comparedGraphLineThickness": 2,
              "columnWidth": 0.7,
              "useDataSetColors": false,
              "comparable": true,
              "compareField": "close",
              "showBalloon": false,
              "proCandlesticks": true
            }],
            "stockLegend": {
              "valueTextRegular": undefined,
              "periodValueTextComparing": "[[percents.value.close]]%"
            }
          },
          {
            "title": "Volume",
            "percentHeight": 30,
            "marginTop": 1,
            "columnWidth": 0.6,
            "showCategoryAxis": false,
            "stockGraphs": [{
              "valueField": "volume",
              "openField": "open",
              "type": "column",
              "showBalloon": false,
              "fillAlphas": 1,
              "lineColor": "#7f8da9",
              "fillColors": "#7f8da9",
              "negativeLineColor": "#db4c3c",
              "negativeFillColors": "#db4c3c",
              "useDataSetColors": false
            }],
            "stockLegend": {
              "markerType": "none",
              "markerSize": 0,
              "labelText": "",
              "periodValueTextRegular": "[[value.close]]"
            },
            "valueAxes": [{
              "usePrefixes": true
            }]
          }
        ],
        "panelsSettings": {
          "color": "#000",
          "plotAreaFillColors": "#333",
          "plotAreaFillAlphas": 0,
          "marginLeft": 60,
          "marginTop": 5,
          "marginBottom": 5
        },
        "chartScrollbarSettings": {
          "graph": "g1",
          "graphType": "line",
          "usePeriod": "WW",
          "backgroundColor": "#dfdfdf",
          "graphFillColor": "#666",
          "graphFillAlpha": 0.2,
          "gridColor": "#fff",
          "gridAlpha": 0.15,
          "selectedBackgroundColor": "#ebebeb",
          "selectedGraphFillAlpha": 1
        },
        "categoryAxesSettings": {
          "equalSpacing": true,
          "dashLength": "7",
          "gridColor": "#000",
          "gridAlpha": 0.1
        },
        "valueAxesSettings": {
          "gridColor": "#000",
          "dashLength": "7",
          "gridAlpha": 0.1,
          "inside": false,
          "showLastLabel": true
        },
        "chartCursorSettings": {
          "pan": true,
          "valueLineEnabled": true,
          "valueLineBalloonEnabled": true
        },
        "legendSettings": {
          "color": "#000",
        },
        "stockEventsSettings": {
          "showAt": "high",
          "type": "pin"
        },
        "balloon": {
          "textAlign": "left",
          "offsetY": 10
        },
        "periodSelector": {
          "position": "top",
          "periods": [{
            "period": "DD",
            "count": 1,
            "label": "1D"
          }, {
            "period": "MM",
            "count": 1,
            "selected": true,
            "label": "1M"
          }, {
            "period": "MM",
            "count": 6,
            "label": "6M"
          }, {
            "period": "YYYY",
            "count": 1,
            "label": "1Y"
          }, {
            "period": "MAX",
            "label": "MAX"
          }]
        }
      });

    </script> 
    
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card buy-order">
                                        <div class="card-header d-flex align-items-center justify-content-between">
                                            <div class=""><?php echo display('buy') ?> <?php echo $coin_symbol[0] ?></div>
                                            <div class="balance"><?php echo $coin_symbol[1] ?> <?php echo display('balance') ?>: <span id="balance_buy"><?php echo @$balance_to->balance?@(float)$balance_to->balance:'0.00' ?></span></div>
                                        </div>
                                        <?php echo form_open('home/buy','id="buyform" class="buy-form" name="buyform"'); ?>
                                        <?php echo form_hidden('market', @$market_details->symbol); ?> 
                                        <div class="card-body">
                                            <div class="">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <label for="buypricing"><?php echo display('price') ?></label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="<?php echo $coin_symbol[1] ?>"><?php echo $coin_symbol[1] ?></span>
                                                            </div>
                                                            <input type="text" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 46 || event.charCode == 0 " class="form-control" id="buypricing" name="buypricing" onchange="parseNumberToEight(this)" aria-describedby="<?php echo $coin_symbol[1] ?>" required>
                                                            <div class="invalid-feedback">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <label for="buyamount"><?php echo display('amount') ?></label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="<?php echo $coin_symbol[0] ?>"><?php echo $coin_symbol[0] ?></span>
                                                            </div>
                                                            <input type="text" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 46 || event.charCode == 0 " class="form-control" id="buyamount" aria-describedby="<?php echo $coin_symbol[0] ?>" name="buyamount" value="1" required>
                                                            <div class="invalid-feedback">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                                <div class="price-details d-flex align-items-center justify-content-between">
                                                    <div class=""><?php echo display('estimated_open_price') ?>:</div>
                                                    <div class="" id="buywithout_fees">0.00</div>
                                                    <input type="hidden" name="buywithout_feesval" id="buywithout_feesval" />
                                                </div>
                                                <div class="price-details d-flex align-items-center justify-content-between">
                                                    <div class=""><?php echo display('open_fees') ?>:</div>
                                                    <div class="" id="buyfees">0.00 <?php echo $coin_symbol[1] ?> (<?php echo @$fee_usd->fees ?>%)</div>
                                                    <input type="hidden" name="buyfeesval" id="buyfeesval" value="" />
                                                </div>
                                                <div class="price-details d-flex align-items-center justify-content-between">
                                                    <div class=""><?php echo display('total') ?>:</div>
                                                    <div class="total" id="buytotal">0.00</div>
                                                    <input type="hidden" name="buytotalval" id="buytotalval" value="" />
                                                </div>
                                            </div>
                                            <div class="text-center">
                                            <div class="buyloginMessage"></div>
                                            <?php if($this->session->userdata('user_id')!=NULL){?>
                                                <button type="submit" class="btn btn-block btn-success"><?php echo display('buy') ?> <?php echo $coin_symbol[0] ?></button>
                                            <?php } else{ ?>
                                                <a href="<?php echo base_url('login') ?>" class="standard"><?php echo display('sign_in') ?></a> or <a href="<?php echo base_url('register') ?>" class="standard">Create an Account</a> to  trade.
                                            <?php } ?>
                                            </div>
                                        </div>
                                        <?php echo form_close() ?>
                                    </div>
                                    <!-- /.End of buy BTC -->
                                </div>
                                <div class="col-md-6">
                                    <div class="card sell-order">
                                        <div class="card-header d-flex align-items-center justify-content-between">
                                            <div class=""><?php echo display('sell') ?> <?php echo $coin_symbol[0] ?></div>
                                            <div class="balance"><?php echo $coin_symbol[0] ?> <?php echo display('balance') ?>: <span id="balance_sell"><?php echo @$balance_from->balance?@(float)$balance_from->balance:'0.00' ?></span></div>
                                        </div>
                                        <?php echo form_open('home/sell','id="sellform" class="buy-form" name="sellform"'); ?>
                                        <?php echo form_hidden('market', @$market_details->symbol) ?> 
                                        <div class="card-body">                                        
                                            <div class="">                                                
                                                <div class="row">
                                                    <div class="col-12">
                                                        <label for="sellamount"><?php echo display('amount') ?></label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="<?php echo $coin_symbol[0] ?>"><?php echo $coin_symbol[0] ?></span>
                                                            </div>
                                                            <input type="text" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 46 || event.charCode == 0 " class="form-control" id="sellamount" aria-describedby="<?php echo $coin_symbol[0] ?>" name="sellamount" value="1" required>
                                                            <div class="invalid-feedback">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <label for="sellpricing"><?php echo display('price') ?></label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="<?php echo $coin_symbol[1] ?>"><?php echo $coin_symbol[1] ?></span>
                                                            </div>
                                                            <input type="text" onchange="parseNumberToEight(this)"  onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 46 || event.charCode == 0 " class="form-control" id="sellpricing" aria-describedby="<?php echo $coin_symbol[1] ?>" name="sellpricing" required>
                                                            <div class="invalid-feedback">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                
                                                <div class="price-details d-flex align-items-center justify-content-between">
                                                    <div class=""><?php echo display('estimated_open_price') ?>:</div>
                                                    <div class="" id="sellwithout_fees">0.00</div>
                                                    <input type="hidden" name="sellwithout_feesval" id="sellwithout_feesval" />
                                                </div>
                                                <div class="price-details d-flex align-items-center justify-content-between">
                                                    <div class=""><?php echo display('open_fees') ?>:</div>
                                                    <div class="" id="sellfees">0.00 <?php echo $coin_symbol[0] ?> (<?php echo @$fee_coin->fees ?>%)</div>
                                                    <input type="hidden" name="sellfeesval" id="sellfeesval" value="" />
                                                </div>
                                                <div class="price-details d-flex align-items-center justify-content-between">
                                                    <div class=""><?php echo display('total') ?>:</div>
                                                    <div class="total" id="selltotal">0.00</div>
                                                    <input type="hidden" name="selltotalval" id="selltotalval" value="" />
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <div class="sellloginMessage"></div>
                                                <?php if($this->session->userdata('user_id')!=NULL){?>
                                                <button type="submit" class="btn btn-block btn-danger"><?php echo display('sell') ?> <?php echo $coin_symbol[0] ?></button>
                                                <?php } else{ ?>
                                                    <a href="<?php echo base_url('login') ?>" class="standard"><?php echo display('sign_in') ?></a> or <a href="<?php echo base_url('register') ?>" class="standard">Create an Account</a> to  trade.
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php echo form_close() ?>
                                    </div>
                                    <!-- /.End of sell BTC -->
                                </div>
                            </div>

                            <!-- /.End of market depth -->
                            
                            
                        </div>

                        <div class="col-md-12 col-lg-5">

                            <div class="row">

                                <div class="col-md-5 col-lg-5">
                            
                                    <div class="card-header d-flex align-items-center justify-content-between">
                                        <div><?php echo display('market_trade_history') ?></div>
                                    </div>

                                    <div class="history-table tableFixHead">
                                        <table class="table table-striped">
                                            <!-- <thead>
                                                <tr class="table-bg">
                                                    <th class="date"><?php echo display('date') ?></th>
                                                    <th class="type"><?php echo display('type') ?></th>
                                                    <th class="amount text-right"><?php echo display('amount') ?> <?php echo $coin_symbol[0] ?></th>
                                                    <th class="price text-right"><?php echo display('price') ?> (<?php echo $coin_symbol[1] ?>)</th>
                                                    <th class="total text-right"><?php echo display('total') ?> (<?php echo $coin_symbol[1] ?>)</th>
                                                </tr>
                                            </thead> -->
                                            <tbody id="tradeHistory">
                                                
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="col-md-7 col-lg-7">
                                    <div class="markert-tabs">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">

                                        <?php foreach ($coin_markets as $key => $value) { $coin_symbol = explode('_', $this->input->get('market'));?>
                                            <li class="nav-item">
                                                <a class="nav-link <?php echo ($coin_symbol[1]==$value->name)?'active':'' ?>" id="baseTab<?php echo $value->name ?>" data-toggle="tab" href="#tab<?php echo $value->name ?>" role="tab" aria-controls="tab<?php echo $value->name ?>" aria-selected="true"><?php echo $value->name ?></a>
                                            </li>
                                        <?php } ?>
                                        </ul>
                                        <div class="tab-content" id="myTabContent">

                                            <?php foreach ($coin_markets as $key => $value) { ?>
                                            <div class="tab-pane fade show <?php echo ($coin_symbol[1]==$value->name)?'active':'' ?>" id="tab<?php echo $value->name ?>" role="tabpanel" aria-labelledby="baseTab<?php echo $value->name ?>">
                                                <div class="markert-table tableFixHead">
                                                    <table class="table table-striped table-hover">
                                                        <thead>
                                                            <tr class="table-bg">
                                                                <!-- <th class="star" colspan="1"></th> -->
                                                                <th class="coin"><?php echo display('coin') ?></th>
                                                                <th class="price text-right"><?php echo display('price') ?></th>
                                                                <th class="volume text-right"><?php echo display('volume') ?></th>
                                                                <th class="change text-center"><?php echo display('change') ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($coin_pairs as $keyp => $valuep) { if($valuep->market_symbol == $value->symbol){ ?>
                                                            <!-- <a href="<?php echo base_url("exchange/?market=$valuep->symbol") ?>"> -->
                                                            <tr onclick="window.location='<?php echo base_url("exchange/?market=$valuep->symbol") ?>';">
                                                                <!-- <th class="star"><i class="fas fa-star"></i></th> -->
                                                                <th class="coin"><?php echo $valuep->currency_symbol." / ".$valuep->market_symbol ?></th>
                                                                <td class="price text-right" id="price_<?php echo $valuep->symbol ?>"><?php echo $valuep->initial_price==''?'0.00':number_format($valuep->initial_price, '8', '', '.') ?></td>
                                                                <td class="volume text-right" id="volume_<?php echo $valuep->symbol ?>">0.00<?php //echo $valuep->symbol ?></td>
                                                                <td class="change text-center" id="price_change_<?php echo $valuep->symbol ?>">0%</td>
                                                            </tr>
                                                            <!-- </a> -->
                                                        <?php } } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <?php } ?>


                                        </div>
                                    </div>
                                    <!-- /.End of market table -->
                                    
                                </div>

                                <div class="col-md-12 col-lg-12">

                                    <div class="chart-panel">

                                        <h5><i class="fas fa-chart-line"></i> <?php echo display('market_depth') ?></h5>
                                        <div id="marketDepth"></div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        
                        <div class="col-md-12 col-lg-12" style="display:none;">
                            <div class="contTabbg">
                                <div class="contTabUlbg">
                                    <a href="#" class="showall">Show All</a>
                                    <div class="hideOther"><input type="checkbox"> Hide Other Pairs</div>
                                    <ul class="nav nav-tabs tradingTab">
                                        <li class="active"><a href="#tab1default" data-toggle="tab">Open Orders</a></li>
                                        <li><a href="#tab2default" data-toggle="tab">Order History</a></li>
                                        <li><a href="#tab3default" data-toggle="tab">Balance</a></li>
                                    </ul>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active show" id="tab1default">
                                        <table cellpadding=0 cellspacing=0>
                                            <tr>
                                                <th>Placed</th>
                                                <th>Symbol</th>
                                                <th>Type</th>
                                                <th>Price</th>
                                                <th>Order Qty</th>
                                                <th>Filled Qty</th>
                                                <th>Total</th>
                                                <th>Filled%</th>
                                                <th>Operation</th>
                                            </tr>
                                            <tr>
                                                <td><span>2019-02-09</span>18:21:57</td>
                                                <td>PLAY/BTC</td>
                                                <td class="colorgreen">Buy</td>
                                                <td>0.0000142</td>
                                                <td>1.00</td>
                                                <td>0</td>
                                                <td>0.0000142</td>
                                                <td>0%</td>
                                                <td><a href="#">Cancel</a></td>
                                            </tr>
                                            <tr>
                                                <td><span>2019-02-09</span>18:21:57</td>
                                                <td>PLAY/BTC</td>
                                                <td class="colorgreen">Buy</td>
                                                <td>0.0000142</td>
                                                <td>1.00</td>
                                                <td>0</td>
                                                <td>0.0000142</td>
                                                <td>0%</td>
                                                <td><a href="#">Cancel</a></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="tab2default">
                                    <table cellpadding=0 cellspacing=0>
                                            <tr>
                                                <th>Placed</th>
                                                <th>Symbol</th>
                                                <th>Type</th>
                                                <th>Price</th>
                                                <th>Order Qty</th>
                                                <th>Filled Qty</th>
                                                <th>Total</th>
                                                <th>Filled%</th>
                                                <th>Operation</th>
                                            </tr>
                                            <tr>
                                                <td><span>2019-02-09</span>18:21:57</td>
                                                <td>PLAY/BTC</td>
                                                <td class="colorgreen">Buy</td>
                                                <td>0.0000142</td>
                                                <td>1.00</td>
                                                <td>0</td>
                                                <td>0.0000142</td>
                                                <td>0%</td>
                                                <td><a href="#">Cancel</a></td>
                                            </tr>
                                            <tr>
                                                <td><span>2019-02-09</span>18:21:57</td>
                                                <td>PLAY/BTC</td>
                                                <td class="colorgreen">Buy</td>
                                                <td>0.0000142</td>
                                                <td>1.00</td>
                                                <td>0</td>
                                                <td>0.0000142</td>
                                                <td>0%</td>
                                                <td><a href="#">Cancel</a></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="tab3default">
                                    <table cellpadding=0 cellspacing=0>
                                            <tr>
                                                <th>Placed</th>
                                                <th>Symbol</th>
                                                <th>Type</th>
                                                <th>Price</th>
                                                <th>Order Qty</th>
                                                <th>Filled Qty</th>
                                                <th>Total</th>
                                                <th>Filled%</th>
                                                <th>Operation</th>
                                            </tr>
                                            <tr>
                                                <td><span>2019-02-09</span>18:21:57</td>
                                                <td>PLAY/BTC</td>
                                                <td class="colorgreen">Buy</td>
                                                <td>0.0000142</td>
                                                <td>1.00</td>
                                                <td>0</td>
                                                <td>0.0000142</td>
                                                <td>0%</td>
                                                <td><a href="#">Cancel</a></td>
                                            </tr>
                                            <tr>
                                                <td><span>2019-02-09</span>18:21:57</td>
                                                <td>PLAY/BTC</td>
                                                <td class="colorgreen">Buy</td>
                                                <td>0.0000142</td>
                                                <td>1.00</td>
                                                <td>0</td>
                                                <td>0.0000142</td>
                                                <td>0%</td>
                                                <td><a href="#">Cancel</a></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>


                        
                    </div>
                </div>
                
            </div>

            <script>
                function parseNumberToEight(thissss){
                    thissss.value = parseFloat(thissss.value).toFixed(8);
                }
            </script>
