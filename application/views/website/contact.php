<?php
$settings = $this->db->select("*")
    ->get('setting')
    ->row();

 
    $i=1; 
    foreach ($article as $con_key => $con_value) {
        $con_headline[]     =   isset($lang) && $lang =="french"?$con_value->headline_fr:$con_value->headline_en;
        $con_address[]      =   $con_value->article1_en;
        $con_phone[]        =   $con_value->article1_fr;
        $con_officetime[]   =   $con_value->article2_en;

    $i++;

    }

?>

 		<div class="contactPage-content">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-md-12 col-lg-7">
                        <div class="contect-des">
                            <div class="contact-header">
                                <h2>
                                    <span class="superheadline">Bdtask</span>
                                    <span class="headline"><?php echo @$con_address[0]; ?></span>
                                </h2>
                                <p><?php echo @$con_headline[0]; ?></p>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="media contact-service">
                                        <img src="<?php echo base_url("assets/website/img/icon/placeholder.png"); ?>" class="mr-3" alt="">
                                        <div class="media-body">
                                            <h4 class="mt-0">Address</h4>
                                            <div><?php echo @$con_address[0]; ?></div>
                                        </div>
                                    </div>
                                    <div class="media contact-service">
                                        <img src="<?php echo base_url("assets/website/img/icon/hours.png"); ?>" class="mr-3" alt="">
                                        <div class="media-body">
                                            <h4>Working hours</h4>
                                            <div>
                                                <?php echo @$con_officetime[0]; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="media contact-service">
                                        <img src="<?php echo base_url("assets/website/img/icon/multimedia.png"); ?>" class="mr-3" alt="">
                                        <div class="media-body">
                                            <h4>Email Address</h4>
                                            <div><?php echo $settings->email ?></div>
                                        </div>
                                    </div>
                                    <div class="media contact-service">
                                        <img src="<?php echo base_url("assets/website/img/icon/technology.png"); ?>" class="mr-3" alt="">
                                        <div class="media-body">
                                            <h4>Phone Number</h4>
                                            <div><?php echo @$con_phone[0]; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-5">
                        <div class="map-content">
                            <div id="map"></div>
                        </div>
                        <!-- /.End of map content -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="contact-form">
                            <?php echo form_open('home/contactMsg','id="contactForm"  class="contact_form" name="contactForm"'); ?>
                                <h2>Let's Talk!</h2>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo display('firstname'); ?></label>
                                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter your First Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo display('lastname'); ?></label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Your Last Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo display('email'); ?></label>
                                            <input type="text" class="form-control" id="email" name="email" placeholder="Enter Your Email" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><?php echo display('phone'); ?></label>
                                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label></label>
                                    <textarea class="form-control" id="comment" name="comment" placeholder="Your Comment" rows="5" required></textarea>
                                </div>
                                <button type="submit" class="btn btn-kingfisher-daisy "><?php echo display('submit') ?></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.End of contact content -->