<style type="text/css">
        .table td, .table th {
            vertical-align: unset;
        }
</style>
<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/global-style.css') ?>" rel="stylesheet" type="text/css">


<div class="main-container">
    <div class="header-flex">
        <div class="header-title">
            <h4><?php echo display('open_order_history') ?></h4>
        </div>
        <div class="ml_banner_img" style="width: 728px; height: 90px;">
            <?php echo adshow(array(
                'width'         => 728,
                'height'        => 90,
                'client'        => 'ca-pub-2238365415915736',
                'slot'          => '6978414426',
                'format'        => 'auto',
                'responsive'    => 'auto',
            ));?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
           
            <div class="dpt-row">

                <div class="dpt-col-90">
                    
                    <!-- /.end of alert message -->
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr class="table-bg">
                                    <th><?php echo display('trade') ?></th>
                                    <th><?php echo display('rate') ?></th>
                                    <th><?php echo display('required_qty') ?></th>
                                    <th><?php echo display('available_qty') ?></th>
                                    <th><?php echo display('required_amount') ?></th>
                                    <th><?php echo display('available_amount') ?></th>
                                    <th><?php echo display('market') ?></th>
                                    <th><?php echo display('open') ?></th>
                                    <th><?php echo display('status') ?></th>
                                    <th><?php echo display('action') ?></th>
                                </tr>
                            </thead>
                            <tbody id="useropenTrade">
                                <?php  foreach ($open_trade as $key => $value) { ?>
                                	<tr>
                                        <td><?php echo $value->bid_type; ?></td>
                                        <td><?php echo $value->bid_price; ?></td>
                                        <td><?php echo $value->bid_qty; ?></td>
                                        <td><?php echo $value->bid_qty_available; ?></td>
                                        <td><?php echo $value->total_amount; ?></td>
                                        <td><?php echo $value->amount_available; ?></td>
                                        <td><?php echo $value->market_symbol; ?></td>
                                        <td><?php echo $value->open_order; ?></td>
                                        <td><p class='bg-primary text-white text-center pb-1 pl-1 pr-1 mb-1 mt-1'><?php echo display('running') ?></p></td>
                                        <td><a href="<?php echo base_url("order-cancel/$value->id") ?>" class="bg-danger text-white text-center pt-1 pb-1 pl-1 pr-1 mb-1 mt-1"  data-toggle="tooltip" data-placement="left" title="Cancel"><?php echo display('cancel') ?></a></td>
                                	</tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="dpt-col-3 sticky" style="width: 120px; height: 600px;">
                    <?php echo adshow(array(
                        'width'         => 120,
                        'height'        => 600,
                        'client'        => 'ca-pub-2238365415915736',
                        'slot'          => '6978414426',
                        'format'        => 'auto',
                        'responsive'    => 'auto',
                    ));?>
                </div>

            </div>
        <!-- End of Trade History -->
        </div>
    </div>
</div>