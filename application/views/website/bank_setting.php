        <div class="payout-content">
            <div class="container">
                <div class="row">
                    
                    <div class="col-lg-6 offset-lg-3">
                    <h3 class="mb-3"><?php echo display('bank_setting');?></h3>
                    <!-- alert message -->
                    <?php if ($this->session->flashdata('message') != null) {  ?>
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $this->session->flashdata('message'); ?>
                    </div> 
                    <?php } ?>
                        
                    <?php if ($this->session->flashdata('exception') != null) {  ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo $this->session->flashdata('exception'); ?>
                    </div>
                    <?php } ?>
                        
                    <?php if (validation_errors()) {  ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?php echo validation_errors(); ?>
                    </div>
                    <?php } ?> 
                    <!-- /.alert message -->

                        <div class="mb-3">  
                            <?php echo form_open('bank-setting/bank');?>
                                <input class="form-control" name="currency_symbol" value="USD" type="hidden">

                                <div class="form-group row">
                                    <label for="acc_name" class="col-md-4 col-form-label"><?php echo display('account_name') ?><i class="text-danger">*</i></label>
                                    <div class="col-md-8">
                                        <input name="acc_name" type="text" class="form-control" id="acc_name" value="<?php echo @$acc_name; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="acc_no" class="col-md-4 col-form-label"><?php echo display('account_no') ?><i class="text-danger">*</i></label>
                                    <div class="col-md-8">
                                        <input name="acc_no" type="text" class="form-control" id="acc_no" value="<?php echo @$acc_no; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch_name" class="col-md-4 col-form-label"><?php echo display('branch_name') ?><i class="text-danger">*</i></label>
                                    <div class="col-md-8">
                                        <input name="branch_name" type="text" class="form-control" id="branch_name" value="<?php echo @$branch_name; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="swift_code" class="col-md-4 col-form-label"><?php echo display('swift_code') ?></label>
                                    <div class="col-md-8">
                                        <input name="swift_code" type="text" class="form-control" id="swift_code" value="<?php echo @$swift_code; ?>" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="abn_no" class="col-md-4 col-form-label"><?php echo display('abn_no') ?></label>
                                    <div class="col-md-8">
                                        <input name="abn_no" type="text" class="form-control" id="abn_no" value="<?php echo @$abn_no; ?>" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="country" class="col-md-4 col-form-label"><?php echo display('country') ?><i class="text-danger">*</i></label>
                                    <div class="col-md-8">
                                        <select class="custom-select" name="country" id="country">
                                            <option>Select Option</option>
                                            <?php foreach ($countrys as $key => $value) { ?>
                                                <option value="<?php echo $value->iso ?>" <?php echo $value->iso==@$country?'selected':null ?> ><?php echo $value->nicename ?></option>
                                            <?php } ?>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="bank_name" class="col-md-4 col-form-label"><?php echo display('bank_name') ?><i class="text-danger">*</i></label>
                                    <div class="col-md-8">
                                        <input name="bank_name" type="text" class="form-control" id="bank_name" value="<?php echo @$bank_name; ?>" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-kingfisher-daisy float-right"><?php echo display("update") ?></button>
                                    </div>
                                </div>

                            <?php echo form_close();?>
                        </div>   
                    </div>

                </div>
            </div>
        </div>