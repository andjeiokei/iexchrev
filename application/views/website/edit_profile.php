<div class="edit-profile">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <!-- alert message -->
                <?php if ($this->session->flashdata('message') != null) {  ?>
                <div class="alert alert-info alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('message'); ?>
                </div> 
                <?php } ?>
                    
                <?php if ($this->session->flashdata('exception') != null) {  ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('exception'); ?>
                </div>
                <?php } ?>
                    
                <?php if (validation_errors()) {  ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo validation_errors(); ?>
                </div>
                <?php } ?> 
                <!-- /.alert message -->
                <?php echo form_open_multipart("edit-profile") ?>
                <?php echo form_hidden('user_id',$user->user_id) ?>
                <div class="form-group">
                    <label for="first_name"><?php echo display('fullname') ?> *</label>
                    <input name="first_name" class="form-control" type="text" placeholder="<?php echo display('fullname') ?>" id="first_name"  value="<?php echo $user->first_name ?>">
                </div>
                <!-- <div class="form-group">
                    <label for="last_name"><?php echo display('lastname') ?> *</label>
                    <input name="last_name" class="form-control" type="text" placeholder="Last Name" id="last_name" value="<?php echo $user->last_name ?>">
                </div> -->

                <div class="form-group">
                    <label for="nick_name"><?php echo display('nickname') ?> </label>
                    <input name="nick_name" class="form-control" type="text" placeholder="Nick Name" id="nick_name" value="<?php echo $user->nick_name ?>">
                </div>

                <div class="form-group">
                    <label for="email"><?php echo display('email') ?> *</label>
                    <input name="email" class="form-control" type="text" placeholder="Email Address" id="email" value="<?php echo $user->email ?>">
                </div> 
                <div class="form-group">
                    <label for="phone"><?php echo display('phone') ?></label>
                    <input name="phone" class="form-control" type="text" placeholder="" id="phone" value="<?php echo $user->phone ?>">
                </div> 
                <div class="form-group">
                    <label for="password"><?php echo display('password') ?> *</label>
                    <input name="password" class="form-control" type="password" placeholder="Password" id="password">
                </div>
                <div class="form-group">
                    <label for="bio"><?php echo display('about') ?></label>
                    <textarea name="bio" placeholder="About" class="form-control" id="bio"><?php echo $user->bio ?></textarea>
                </div>
                <div class="form-group row">
                    <label for="preview" class="col-md-3"><?php echo display('preview') ?></label>
                    <div class="col-md-9">
                        <img src="<?php echo base_url(!empty($user->image)?$user->image: "./assets/images/icons/user.png") ?>" class="img-thumbnail" width="125" height="100">
                        <input type="hidden" name="old_image" value="<?php echo $user->image ?>">
                    </div> 
                </div> 
                <div class="form-group row">
                    <label for="image" class="col-md-3"><?php echo display('image') ?></label>
                    <div class="col-md-9">
                        <input type="file" name="image" id="image" aria-describedby="fileHelp">
                        <small id="fileHelp" class="text-muted"></small>
                    </div>
                </div> 
                <div class="form-group">
                    <button type="reset" class="btn btn-danger"><?php echo display('cancel') ?></button>
                    <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display('save') ?></button>
                </div>
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</div>