<?php
$settings = $this->db->select("*")
    ->get('setting')
    ->row();

    $home_menu = $this->uri->segment(1);
    $menu_cls = '';
    $div_start = '';
    //$div_end = '';
    if ($home_menu) {
        $menu_cls ='navbar-dark bg-kingfisher-daisy';   
                     
    }else{
        $menu_cls = 'bg-transparent';
       
    }
    if ($home_menu=='exchange') {
        $div_start = '<div class="exchange-wrapper">';
    }

    if( in_array($this->uri->segment(1), array("arena") ) )
    {
        $settings->title = "Welcome to the arena!";
    }

?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">
        <link rel="shortcut icon" href="<?php echo base_url($settings->favicon); ?>">
        

        <?php if($this->uri->segment(1) == 'arena'){?>
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="<?php echo base_url('assets/arenagame/css/bootstrap.css')?>" rel="stylesheet" type="text/css">

        <?php }?>


        <link href="<?php echo base_url('assets/website/css/vendors.bundle.min.css') ?>" rel="stylesheet"/>
        <link href="<?php echo base_url('assets/website/fontawesome/css/fontawesome-all.min.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/website/css/style.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/js/sweetalert/sweetalert.css')?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url('assets/website/css/global-style.css')?>" rel="stylesheet" type="text/css"/>
        

        <?php if($this->uri->segment(1) == 'arena'){?>
        <!-- Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/arenagame/css/arena.css')?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/arenagame/css/owl.carousel.min.css')?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/arenagame/css/animate.css')?>" rel="stylesheet" type="text/css">
        <!-- Media queries CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/arenagame/css/media-queries.css')?>" rel="stylesheet" type="text/css">

        <?php }?>



        
        <title><?php echo $settings->title ?></title>
        

        <?php 
        $pixcel_page_list = array('login', 'register', 'arena');
        if(in_array($this->uri->segment(1), $pixcel_page_list) || $this->uri->segment(1) == ''){?>
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '393811621108902');
        fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=393811621108902&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

        <?php }?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139964057-1"></script>
		<script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-139964057-1');
		</script>

    </head>
    <body class="<?php echo (in_array($this->uri->segment(1), array("arena")))? "inner-bg":"";?>">
        <?php echo $div_start ?>
            <nav class="navbar navbar-expand-lg <?php echo $menu_cls ?>">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo base_url() ?>"><img class="logo" src="<?php echo base_url($settings->logo_web) ?>" alt=""></a>
                    <button type="button" class="navbar-toggler" id="sidebarCollapse"  >
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="<?php echo base_url() ?>"><?php echo display('home') ?></a>
                            </li>
                            <?php 

                                $query_pair = $this->db->select('*')->from('dbt_coinpair')->where('status', 1)->order_by('id','asc')->get()->row(); 

                            ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url("exchange/?market=$query_pair->symbol") ?>"><?php echo display('exchange') ?></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo display('finance') ?>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo base_url('balances') ?>"><?php echo display('balance') ?></a>
                                    <a class="dropdown-item" href="<?php echo base_url('deposit') ?>"><?php echo display('deposit') ?></a>
                                    <a class="dropdown-item" href="<?php echo base_url('withdraw') ?>"><?php echo display('withdraw') ?></a>
                                    <!-- <a class="dropdown-item" href="<?php //echo base_url('transfer') ?>"><?php //echo display('transfer') ?></a> -->
                                    <a class="dropdown-item" href="<?php echo base_url('transactions') ?>"><?php echo display('transection') ?></a>
                                </div>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo display('trade') ?>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo base_url('open-order') ?>"><?php echo display('open_order') ?></a>
                                    <a class="dropdown-item" href="<?php echo base_url('complete-order') ?>"><?php echo display('complete_order') ?></a>
                                    <a class="dropdown-item" href="<?php echo base_url('trade-history') ?>"><?php echo display('trade_history') ?></a>
                                </div>
                            </li>

                            <?php if($this->session->userdata('user_id')!=NULL){?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo display('account') ?>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- <li><a href="<?php echo base_url('security'); ?>">Security</a></li> -->
                                    <!-- <li><a class="dropdown-item" href="<?php //echo base_url('bank-setting'); ?>"><?php //echo display('bank_setting') ?></a></li>
                                    <li><a class="dropdown-item" href="<?php //echo base_url('payout-setting'); ?>"><?php //echo display('payout_setup') ?></a></li> -->
                                    <li><a class="dropdown-item" href="<?php echo base_url('profile'); ?>"><?php echo display('profile') ?></a></li>
                                    <li><a class="dropdown-item" href="<?php echo base_url('customer/auth/logout'); ?>"><?php echo display('logout'); ?></a></li>
                                </ul>
                            </li>    
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url('arena') ?>">Enter Arena</a>
                            </li>                        
                            <?php } else{ ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url('login') ?>"><?php echo display('login') ?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url('register') ?>"><?php echo display('register') ?></a>
                                </li>
                            <?php } ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url('imba') ?>" >IMBA</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>  
            <!-- /.End of navbar -->
            <nav id="sidebar">
                <div id="dismiss">
                    <i class="fas fa-times"></i>
                </div>
                <ul class="metismenu list-unstyled" id="mobile-menu">
                    <li class="active"><a href="<?php echo base_url() ?>"><?php echo display('home') ?></a></li>
                    <?php 
                        $query_pair = $this->db->select('*')->from('dbt_coinpair')->where('status', 1)->order_by('id','asc')->get()->row();
                    ?>
                    <li><a href="<?php echo base_url("exchange/?market=$query_pair->symbol") ?>"><?php echo display('exchange') ?></a></li>
                    <li>
                        <a href="#" aria-expanded="false"><?php echo display('finance') ?><span class="fa arrow"></span></a>
                        <ul aria-expanded="false">
                            <li><a href="<?php echo base_url('balances') ?>"><?php echo display('balance') ?></a></li>
                            <li><a href="<?php echo base_url('deposit') ?>"><?php echo display('deposit') ?></a></li>
                            <li><a href="<?php echo base_url('withdraw') ?>"><?php echo display('withdraw') ?></a></li>
                            <li><a href="<?php echo base_url('transfer') ?>"><?php echo display('transfer') ?></a></li>
                            <li><a href="<?php echo base_url('transactions') ?>"><?php echo display('transactions') ?></a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" aria-expanded="false"><?php echo display('trade') ?><span class="fa arrow"></span></a>
                        <ul aria-expanded="false">
                            <li><a href="<?php echo base_url('open-order') ?>"><?php echo display('open_order') ?></a></li>
                            <li><a href="<?php echo base_url('complete-order') ?>"><?php echo display('complete_order') ?></a></li>
                            <li><a href="<?php echo base_url('trade-history') ?>"><?php echo display('trade_history') ?></a></li>
                        </ul>
                    </li>
                    <?php if($this->session->userdata('user_id')!=NULL){?>

                    <li>
                        <a href="#" aria-expanded="false"><?php echo display('account') ?><span class="fa arrow"></span></a>
                        <ul aria-expanded="false">
                            <!-- <li><a href="<?php// echo base_url('bank-setting'); ?>"><?php //echo display('bank_setting') ?></a></li>
                            <li><a href="<?php //echo base_url('payout-setting'); ?>"><?php// echo display('payout_setup') ?></a></li> -->
                            <li><a href="<?php echo base_url('profile'); ?>"><?php echo display('profile') ?></a></li>
                            <li><a href="<?php echo base_url('customer/auth/logout'); ?>"><?php echo display('logout'); ?></a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url('arena') ?>">Enter Arena</a></li>                        
                    <?php } else{ ?>
                    <li><a href="<?php echo base_url('login') ?>"><?php echo display('login') ?></a></li>
                    <li><a href="<?php echo base_url('register') ?>"><?php echo display('register') ?></a></li>
                    <?php } ?>                    
                    <li><a href="<?php echo base_url('imba') ?>">IMBA</a></li>                    
                </ul>
            </nav>
            <div class="overlay"></div>
            <!-- /.End of sidebar nav -->