<style type="text/css">
        .table td, .table th {
            vertical-align: unset;
        }
    .ads_custom1strow
    {
        width: 250px;
        height: 180px;
        min-width: 240px;
        max-width: 250px;
        /* border: 1px solid red; */
        margin: 4px 0px 10px 2px;
        padding: 2px;
        float:left;
        position:relative;
    }

    .ads_custom2ndrow
    {
        width: 150px;
        height: 150px;
        min-width: 150px;
        max-width: 150px;
        /* border: 1px solid red; */
        margin: 4px 4px 10px 3px;
        /* padding: 5px; */
        float: left;
        position: relative;
    }

</style>
<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/global-style.css') ?>" rel="stylesheet" type="text/css">


<div class="main-container">
    <div class="header-flex">
        <div class="header-title">
            <h4><?php echo display('balance') ?></h4>
        </div>
        <div class="ml_banner_img" style="width: 728px; height: 90px;">
            <?php echo adshow(array(
                'width'         => 0,
                'height'        => 90,
                'client'        => 'ca-pub-2238365415915736',
                'slot'          => '6978414426',
                'format'        => 'auto',
                'responsive'    => 'auto',
            ));?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
           
            
            <!-- alert message -->
            <?php if ($this->session->flashdata('message') != null) {  ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('message'); ?>
            </div> 
            <?php } ?>
                
            <?php if ($this->session->flashdata('exception') != null) {  ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('exception'); ?>
            </div>
            <?php } ?>
                
            <?php if (validation_errors()) {  ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo validation_errors(); ?>
            </div>
            <?php } ?> 

            <div class="dpt-row">

                <div class="dpt-col-90">
                    <div class="Pbdy-info-search">
                        <div class="bdy-info-sLFT">
                            <div class="seach-inpt">
                                <span class="fa fa-search seachico"></span>
                                <input type="text" class="form-control">
                            </div>
                              <ul class="bdy-info-bx">
                                    <li>
                                       <button class="Cnt-cmnBtn">
                                           Small balance <i class="fa fa-question-circle" aria-hidden="true"></i>   
                                        </button>
                                    </li>
                                    <li>
                                        <label class="Cnt-checkB"><input type="checkbox" value=""> Hide</label>
                                    </li>
                                    <li>
                                        <button class="Cnt-cmnBtn">
                                        <i class="fa fa-exchange" aria-hidden="true"></i> Convert to ICR
                                        </button>
                                    </li>
                                </ul>
                        </div>
                        <!-- <div class="bdy-info-sRGHT">
                            <p>Estimated value: 0123.54568 BTC / $3980.12 </p>
                            <p>24h Withdral limit 2 BTC in use : OBTC</p>
                        </div> -->
                    </div>
                    <!-- /.end of alert message -->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="table-bg">
                                    <th><?php echo display('cryptocoin') ?></th>
                                    <th><?php echo display('name') ?></th>
                                    <th><?php echo display('total_balance') ?></th>
                                    <th><?php echo display('available_balance') ?></th>
                                    <th><?php echo display('in_order') ?></th>
                                    <th><?php echo display('btc_value') ?></th>
                                    <th colspan="2"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($coin_list as $coin_key => $coin_value) {   
                                $inOrders = 0;                              
                                ?>
                            <?php $balance = '0.00'; foreach ($balances as $key => $value) { 
                                if ($value->currency_symbol== $coin_value->symbol) {
                                    $balance = $value->balance;
                                    $inOrders = 0;


                                    $inOrdersQuery = $this->db->select_sum('total_amount')
                                                ->from('dbt_biding')
                                                ->where('user_id', $value->user_id)
                                                ->where('bid_type','BUY')
                                                ->where('status',2)
                                                //->like('currency_symbol', $value->currency_symbol)
                                                ->like('market_symbol', "_".$value->currency_symbol, 'before')
                                                ->get()
                                                ->row();
                                    //echo $this->db->last_query();



                                    $inOrdersQuery2nd = $this->db->select_sum('total_amount')
                                                ->from('dbt_biding')
                                                ->where('user_id', $value->user_id)
                                                ->where('bid_type','SELL')
                                                ->where('status',2)
                                                //->like('currency_symbol', $value->currency_symbol)
                                                ->like('market_symbol', $value->currency_symbol."_", 'after')
                                                ->get()
                                                ->row();
                                    //echo $this->db->last_query();
                                
                                    if($inOrdersQuery->total_amount > 0){ 
                                        $inOrders = $inOrdersQuery->total_amount;
                                    } 
                                    else if($inOrdersQuery2nd->total_amount > 0){
                                        $inOrders = $inOrdersQuery2nd->total_amount;
                                    }


                                }

                            } ?>
                                <tr>
                                    <td><div class="d-flex marks-ico">
                                            <div><img src="<?php echo base_url("$coin_value->image") ?>" alt=""></div>
                                            <div class="ico-name">
                                                <font><?php echo $coin_value->symbol; ?></font>
                                                <span class="text-muted">(<?php echo $coin_value->coin_name; ?>)</span>
                                            </div>
                                        </div></td>
                                    <td><?php echo $coin_value->coin_name; ?></td>
                                    <td><?php echo number_format( ( $balance + $inOrders ) , 8 ); ?></td>
                                    <td><?php echo number_format( $balance , 8); ?></td>
                                    <td><?php echo number_format( $inOrders , 8); ?></td>
                                    <td><?php echo number_format(0 ,8); ?></td>
                                    <td>
                                        <a href="<?php echo base_url("deposit/$coin_value->symbol"); ?>" class="btn btn-tread"><?php echo display('deposit'); ?></a>
                                        
                                        <a href="<?php echo base_url("withdraw/$coin_value->symbol"); ?>" class="btn btn-tread"><?php echo display('withdraw') ?></a>

                                        <a href="<?php echo base_url("deposit/$coin_value->symbol"); ?>" class="btn btn-tread"><?php echo display('trade'); ?></a>
                                    </td>
                                </tr>                                
                            <?php } ?>

                        </table>
                    </div>


                    <div class="row">

                        <div class="ads_custom1strow">
                        <?php echo adshow(array(
                            'width'         => 240,
                            'height'        => 180,
                            'client'        => 'ca-pub-2238365415915736',
                            'slot'          => '6978414426',
                            'format'        => '',
                            'responsive'    => '',
                        ));?>
                        </div>


                        <div class="ads_custom1strow">
                        <?php echo adshow(array(
                            'width'         => 240,
                            'height'        => 180,
                            'client'        => 'ca-pub-2238365415915736',
                            'slot'          => '6978414426',
                            'format'        => '',
                            'responsive'    => '',
                        ));?>
                        </div>


                        <div class="ads_custom1strow">
                        <?php echo adshow(array(
                            'width'         => 240,
                            'height'        => 180,
                            'client'        => 'ca-pub-2238365415915736',
                            'slot'          => '6978414426',
                            'format'        => '',
                            'responsive'    => '',
                        ));?>
                        </div>


                        <div class="ads_custom1strow">
                        <?php echo adshow(array(
                            'width'         => 240,
                            'height'        => 200,
                            'client'        => 'ca-pub-2238365415915736',
                            'slot'          => '6978414426',
                            'format'        => '',
                            'responsive'    => '',
                        ));?>
                        </div>

                    </div>

                    <div class="row">

                        <div class="ads_custom2ndrow">
                        <?php echo adshow(array(
                            'width'         => 150,
                            'height'        => 150,
                            'client'        => 'ca-pub-2238365415915736',
                            'slot'          => '6978414426',
                            'format'        => '',
                            'responsive'    => '',
                        ));?>
                        </div>


                        <div class="ads_custom2ndrow">
                        <?php echo adshow(array(
                            'width'         => 150,
                            'height'        => 150,
                            'client'        => 'ca-pub-2238365415915736',
                            'slot'          => '6978414426',
                            'format'        => '',
                            'responsive'    => '',
                        ));?>
                        </div>


                        <div class="ads_custom2ndrow">
                        <?php echo adshow(array(
                            'width'         => 150,
                            'height'        => 150,
                            'client'        => 'ca-pub-2238365415915736',
                            'slot'          => '6978414426',
                            'format'        => '',
                            'responsive'    => '',
                        ));?>
                        </div>


                        <div class="ads_custom2ndrow">
                        <?php echo adshow(array(
                            'width'         => 150,
                            'height'        => 150,
                            'client'        => 'ca-pub-2238365415915736',
                            'slot'          => '6978414426',
                            'format'        => '',
                            'responsive'    => '',
                        ));?>
                        </div>

                    </div>

                </div>
                <div class="dpt-col-3 sticky" style="width: 120px; height: 600px;">
                    <?php echo adshow(array(
                        'width'         => 120,
                        'height'        => 600,
                        'client'        => 'ca-pub-2238365415915736',
                        'slot'          => '6978414426',
                        'format'        => 'auto',
                        'responsive'    => 'auto',
                    ));?>
                </div>

            </div>
            
        <!-- End of Trade History -->
        </div>
    </div>

</div>