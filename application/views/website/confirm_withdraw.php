<div class="container">
    <div class="row">
        <?php 
            $data = json_decode($v->data);
        ?>
        <div class="col-lg-6 offset-lg-3">
            <div class="confirm-withdraw">
                <?php   $att = array('name'=>'verify'); echo form_open('#',$att); ?>
                <dl class="row">
                  <dt class="col-6"><?php echo display('amount');?></dt>
                  <dd class="col-6"><?php echo $data->currency_symbol .' '.$data->amount;?></dd>

                  <dt class="col-6"><?php echo display('payment_method');?></dt>
                  <dd class="col-6"><?php echo $data->method;?></dd>

                  <dt class="col-6"><?php echo display('fees');?></dt>
                  <dd class="col-6"><?php echo $data->fees_amount;?></dd>

                  <dt class="col-6"><?php echo display('enter_verify_code');?></dt>
                  <dd class="col-6"><input class="form-control" type="text" name="code" id="code"></dd>
                </dl>
                    <div class="text-center">
                        <button type="button" onclick="withdraw('<?php echo $v->id;?>');" class="btn btn-kingfisher-daisy"><?php echo display('confirm') ?></button>
                        <button type="button" class="btn btn-danger"><?php echo display('cancel') ?></button>
                    </div>
                    <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
