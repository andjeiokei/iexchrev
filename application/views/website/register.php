        <div class="form-body">
            <div class="container text-center">
                <h3 style="margin-bottom:38px; font-size: 22px;">Register below to open your FREE account</h3>
                <h3 style="font-size: 18px;">* Please use your real name, as it will help restore your account in the event you lose access to your account.</h3>
                <h3 style="color: red; font-size: 22px;"><b>Do NOT signup multiple accounts. They will be detected and banned</b></h3>
            </div>
            <div class="form-content register">
                <div class="row">
                    <div class="col-md-12">
                        <!-- alert message -->
                        <?php if ($this->session->flashdata('message') != null) {  ?>
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $this->session->flashdata('message'); ?>
                        </div> 
                        <?php } ?>
                            
                        <?php if ($this->session->flashdata('exception') != null) {  ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $this->session->flashdata('exception'); ?>
                        </div>
                        <?php } ?>
                            
                        <?php if (validation_errors()) {  ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo validation_errors(); ?>
                        </div>
                        <?php } ?> 
                    </div>
                </div>
                <h3 class="user-login-title"><?php echo display('account_register') ?></h3>
                <div class="user-login">
                    <?php echo form_open('register','id="registerForm" name="registerForm" onsubmit="return validateForm()" '); ?>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="f_name"><?php echo display('fullname') ?></label>
                                <input type="text" class="form-control" id="f_name" name="rf_name" placeholder="<?php echo display('fullname') ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email"><?php echo display('email') ?></label>
                                <input type="text" class="form-control" id="email" name="remail" placeholder="example@mail.com" onkeydown="checkEmail()" required>
                            </div>
                        </div>
                        <div class="form-row password_valid">
                            <div class="form-group col-md-6">
                                <label for="pass"><?php echo display('password') ?></label>
                                <input type="password" class="form-control" id="pass" name="rpass" placeholder="Password" onkeyup="strongPassword()" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="r_pass"><?php echo display('confirm_password') ?></label>
                                <input type="password" class="form-control" id="r_pass" name="rr_pass" placeholder="Password" onkeyup="rePassword()" required>
                            </div>
                            <div id="message">
                              <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
                              <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
                              <p id="special" class="invalid">A <b>special</b></p>
                              <p id="number" class="invalid">A <b>number</b></p>
                              <p id="length" class="invalid">Minimum <b>8 characters</b></p>
                            </div>
                    
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="accept_terms" name="raccept_terms" value="ptConfirm" required>
                                <label class="custom-control-label" for="accept_terms"> <?php echo display('your_password_at_global_crypto_are_encrypted_and_secured'); ?></label>
                            </div>
                        </div>                       
                        <button type="submit" class="btn btn-block btn-kingfisher-daisy"><?php echo display('submit') ?></button>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
