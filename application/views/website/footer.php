<?php
    $settings = $this->db->select("*")
        ->get('setting')
        ->row();
?>

        <footer class="footer">
            
            <div class="sticky-stopper"></div>
            <div class="primary-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-scroll back-top" data-section="#top">
                                <i class="fas fa-chevron-up"></i>
                            </div>
                            <?php if ($social_link) { ?>                            
                            <ul class="social-link tt-animate ltr mt-20">
                                <?php   foreach ($social_link as $key => $value) { ?>
                                <li><a href="<?php echo $value->link; ?>"><i class="fab fa-<?php echo $value->icon; ?>"></i></a></li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                        </div>
                    </div>
                    <hr class="mt-15">
                    <div class="row justify-content-between">
                        <div class="col-md-4">
                            <div class="footer-logo">
                                <img src="<?php echo base_url($settings->logo_web) ?>" class="img-fluid" alt="<?php echo $settings->title ?>">
                                <p><?php echo $settings->description ?></p>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-6 col-sm-4 col-md-4">
                                    <div class="link-widgets">
                                        <h5 class="link-title"><?php echo display('footer_menu1') ?></h5>
                                        <ul>
                        <?php
                            foreach ($category as $cat_key => $cat_value) {
                                if ($cat_value->menu==2 || $cat_value->menu==3) { 
                                     $cat_name = isset($lang) && $lang =="french"?$cat_value->cat_name_fr:$cat_value->cat_name_en;
                                     $cat_slug = $cat_value->slug;
                        ?>
                                    <li><a href="<?php echo base_url($cat_slug); ?>"><?php echo  $cat_name ?></a></li>
                        <?php
                                }                               
                            }
                        ?>
                        
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-6 col-sm-4 col-md-4">
                                    <div class="link-widgets">
                                        <h5 class="link-title"><?php echo display('footer_menu2') ?></h5>
                                        <ul>
                                            <!-- <li><a href="<?php echo base_url('news') ?>"><?php echo display('news') ?></a></li>
                                            <li><a href="<?php echo base_url('terms') ?>"><?php echo display('terms_of_use') ?></a></li> -->
                                            <li><a href="<?php echo base_url('login'); ?>"><?php echo display('login') ?></a></li>
                                            <li><a href="<?php echo base_url('register'); ?>"><?php echo display('register') ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 col-md-4">
                                    <div class="link-widgets">
                                    <?php if ($social_link) { ?>
                                        <h5 class="link-title"><?php echo display('footer_menu3') ?></h5>
                                        <ul>
                                            <?php   foreach ($social_link as $key => $value) { ?>
                                        <li><a href="<?php echo $value->link; ?>"><i class="fab fa-<?php echo $value->icon; ?>"></i> <?php echo $value->name; ?></a></li>
                                        <?php } ?>

                                        </ul>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.primary-footer -->
            <div class="secondary-footer">
                <div class="container">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-md-10">
                            <p class="footer-copyright"><?php echo $settings->footer_text; ?></a></p>
                        </div>
                        <div class="col-md-2">
                            <div class="language">
                                <select class="custom-select" id="lang-change" data-width="fit">
                                    <option value="english" <?php echo isset($lang) && $lang =="english"?'Selected':''; ?>>English</option>
                                    <option value="french" <?php echo isset($lang) && $lang =="french"?'Selected':''; ?>><?php echo $web_language->name; ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.secondary-footer -->
        </footer><!-- /.End of footer -->
        <!-- Optional JavaScript -->
<?php
    $home_menu = $this->uri->segment(1);

    $div_end = '';
    if ($home_menu=='exchange') {
        echo $div_end = '</div>';
    }
?>
        <script src="<?php  if($this->uri->segment(1)!='exchange' && $this->uri->segment(1)!='payment-process')  { echo base_url('assets/website/js/vendors.bundle.min.js'); } ?>"></script>


<?php if ($this->uri->segment(1) == '') { ?>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.2/socket.io.js"></script>
        <script src="<?php echo base_url("assets/website/js/ccc-streamer-utilities.js"); ?>"></script>
        <style type="text/css">
            .up {
                color: green;
            }

            .down {
                color: red;
            }
            #crypto  table tbody tr.upbg {
                background-color: rgba(255, 78, 34, .2);
            }

            #crypto  table tbody tr.downbg {
                background-color: rgba(37, 37, 142, 0.2);
            }

            #crypto  table tbody tr td > .exchange {
                color: #42f492;
            }
        </style>

    <script type="text/javascript">

        $(document).ready(function() {

            var currentPrice = {};
            var socket = io.connect('https://streamer.cryptocompare.com/');
            //Format: {SubscriptionId}~{ExchangeName}~{FromSymbol}~{ToSymbol}
            //Use SubscriptionId 0 for TRADE, 2 for CURRENT and 5 for CURRENTAGG
            //For aggregate quote updates use CCCAGG as market

            <?php 

            $coin_stream="";
            foreach ($coin as $coin_key => $coin_value) {
                $coin_stream .= "'5~CCCAGG~".$coin_value->symbol."~USD',";
            }
            ?>
            var subscription = [<?php echo rtrim($coin_stream, ','); ?>];
            socket.emit('SubAdd', { subs: subscription });
            socket.on("m", function(message) {
                var messageType = message.substring(0, message.indexOf("~"));
                var res = {};
                if (messageType == CCC.STATIC.TYPE.CURRENTAGG) {
                    res = CCC.CURRENT.unpack(message);
                    dataUnpack(res);
                }
            });

            var dataUnpack = function(data) {
                var from = data['FROMSYMBOL'];
                var to = data['TOSYMBOL'];
                var fsym = CCC.STATIC.CURRENCY.getSymbol(from);
                var tsym = CCC.STATIC.CURRENCY.getSymbol(to);
                var pair = from + to;

                if (!currentPrice.hasOwnProperty(pair)) {
                    currentPrice[pair] = {};
                }

                for (var key in data) {
                    currentPrice[pair][key] = data[key];
                }

                if (currentPrice[pair]['LASTTRADEID']) {
                    currentPrice[pair]['LASTTRADEID'] = parseInt(currentPrice[pair]['LASTTRADEID']).toFixed(0);
                }
                currentPrice[pair]['CHANGE24HOUR'] = CCC.convertValueToDisplay(tsym, (currentPrice[pair]['PRICE'] - currentPrice[pair]['OPEN24HOUR']));
                currentPrice[pair]['CHANGE24HOURPCT'] = ((currentPrice[pair]['PRICE'] - currentPrice[pair]['OPEN24HOUR']) / currentPrice[pair]['OPEN24HOUR'] * 100).toFixed(2) + "%";;
                displayData(currentPrice[pair], from, tsym, fsym);
            };

            var displayData = function(current, from, tsym, fsym) {
                var priceDirection = current.FLAGS;
                for (var key in current) {
                    if (key == 'CHANGE24HOURPCT') {
                        $('#' + key + '_' + from).text(' (' + current[key] + ')');
                    }
                    else if (key == 'LASTVOLUMETO' || key == 'VOLUME24HOURTO') {
                        $('#' + key + '_' + from).text(CCC.convertValueToDisplay(tsym, current[key]));
                    }
                    else if (key == 'LASTVOLUME' || key == 'VOLUME24HOUR' || key == 'OPEN24HOUR' || key == 'OPENHOUR' || key == 'HIGH24HOUR' || key == 'HIGHHOUR' || key == 'LOWHOUR' || key == 'LOW24HOUR') {
                        $('#' + key + '_' + from).text(CCC.convertValueToDisplay(fsym, current[key]));
                    }
                    else {
                        $('#' + key + '_' + from).text(current[key]);
                    }
                }

                $('#PRICE_' + from).removeClass();
                $('#BGCOLOR_' + from).removeClass();
                if (priceDirection & 1) {
                    $('#PRICE_' + from).addClass("up");
                    $('#BGCOLOR_' + from).addClass("upbg");
                }
                else if (priceDirection & 2) {
                    $('#PRICE_' + from).addClass("down");
                    $('#BGCOLOR_' + from).addClass("downbg");
                }
                if (current['PRICE'] > current['OPEN24HOUR']) {
                    $('#CHANGE24HOURPCT_' + from).removeClass();
                    $('#CHANGE24HOURPCT_' + from).addClass("up");
                }
                else if (current['PRICE'] < current['OPEN24HOUR']) {
                    $('#CHANGE24HOURPCT_' + from).removeClass();
                    $('#CHANGE24HOURPCT_' + from).addClass("down");
                }
            };
        });

    </script>
    <script src="<?php echo base_url('assets/js/sparkline.min.js'); ?>"></script>
        
    <!-- Ajax Subscription -->
    <script type="text/javascript">
        function isValidEmailAddress(emailAddress) {
            var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return pattern.test(emailAddress);
        }

        $(function(){
            $("#subscribeForm").on("submit", function(event) {
                event.preventDefault();
                var inputdata = $("#subscribeForm").serialize();
                var email = $('input[name=subscribe_email]').val();

                if (email == "") {
                    alert("<?php echo display('please_enter_valid_email') ?>");
                    return false;
                }
                if (!isValidEmailAddress(email)) {
                    alert("<?php echo display('please_enter_valid_email') ?>");
                    return false;
                }

                $.ajax({
                    url: "<?php echo base_url('home/subscribe'); ?>",
                    type: "post",
                    data: inputdata,
                    success: function(result,status,xhr) {
                        alert("Subscribtion complete");
                        location.reload();
                    },
                    error: function (xhr,status,error) {
                        if (xhr.status===500) {
                            alert("<?php echo display('already_subscribe') ?>");
                        }
                    }
                });
            });
        }); 
    </script>
<?php } ?>

    <!-- sweetalert -->
    <script src="<?php echo base_url('assets/js/sweetalert/sweetalert.min.js')?>"></script>
    <!-- Custom js -->
    <script src="<?php echo base_url('assets/website/js/script.js') ?>"></script>

<?php if ($this->uri->segment(1)=='contact') { ?>

    <script>
        // When the window has finished loading create our google map below
        //google.maps.event.addDomListener(window, 'load', initMap);

        function initMap() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 11,
                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(<?php echo $settings->latitude ?>), // New York

                // How you would like to style the map. 
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{"featureType": "all", "elementType": "labels.text.fill", "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]}, {"featureType": "all", "elementType": "labels.text.stroke", "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]}, {"featureType": "all", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "administrative", "elementType": "geometry.fill", "stylers": [{"color": "#fefefe"}, {"lightness": 20}]}, {"featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]}, {"featureType": "landscape", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 20}]}, {"featureType": "poi", "elementType": "geometry", "stylers": [{"color": "#f1f1f1"}, {"lightness": 21}]}, {"featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{"color": "#dedede"}, {"lightness": 17}]}, {"featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{"color": "#dedede"}, {"lightness": 29}, {"weight": 0.2}]}, {"featureType": "road.arterial", "elementType": "geometry", "stylers": [{"color": "#dedede"}, {"lightness": 18}]}, {"featureType": "road.local", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 16}]}, {"featureType": "transit", "elementType": "geometry", "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]}, {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#a0d6d1"}, {"lightness": 17}]}, {"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#d4d4e8"}]}]
            };

            // Get the HTML DOM element that will contain your map 
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');
            // AIzaSyAUmj7I0GuGJWRcol-pMUmM4rrnHS90DE8
            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(<?php echo $settings->latitude ?>),
                map: map,
                title: 'Snazzy!'
            });
        }
        
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUmj7I0GuGJWRcol-pMUmM4rrnHS90DE8&callback=initMap" type="text/javascript"></script>
    <!-- Ajax Contract From -->
    <script type="text/javascript">
        $(function(){
            $("#contactForm").on("submit", function(event) {
                event.preventDefault();
                var inputdata = $("#contactForm").serialize();
                $.ajax({
                    url: "<?php echo base_url('home/contactMsg'); ?>",
                    type: "post",
                    data: inputdata,
                    success: function(d) {
                        alert("<?php echo display('message_send_successfuly') ?>");
                        location.reload();
                    },
                    error: function(){
                        alert("<?php echo display('message_send_fail') ?>");
                    }
                });
            });
        }); 
    </script>
<?php } ?>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Verify profile -->
    <script type="text/javascript">
        $(function(){
            $("#verify_type").on("change", function(event) {
                event.preventDefault();
                var verify_type = $("#verify_type").val();

                if (verify_type == 'passport') {

                    $("#verify_field").html("<div class='form-group row'><label for='document1' class='col-md-4 col-form-label'>Passport Cover </label><div class='col-md-8'><input name='document1' type='file' class='form-control' id='document1' required></div></div><div class='form-group row'><label for='document2' class='col-md-4 col-form-label'>Passport Inner </label><div class='col-md-8'><input name='document2' type='file' class='form-control' id='document2' required></div></div>");

                }else if (verify_type == 'driving_license') {
                    $("#verify_field").html("<div class='form-group row'><label for='document1' class='col-md-4 col-form-label'>Driving License </label><div class='col-md-8'><input name='document1' type='file' class='form-control' id='document1' required></div></div>");
                    
                }else if (verify_type == 'nid') {
                    $("#verify_field").html("<div class='form-group row'><label for='document1' class='col-md-4 col-form-label'>NID With selfie </label><div class='col-md-8'><input name='document1' type='file' class='form-control' id='document1' required></div></div>");
                    
                }else{
                    $("#verify_field").html();

                }


            });

            $( ".datepicker" ).datepicker({ dateFormat: 'mm-dd-yy',  maxDate: 0 });
            $(".datepicker_icon_js").on('click',function(){
                $(this).siblings('input[type=text]').focus();
            });

            $("#is_canclled_hide").on('click change',function(){
                $("#search").click();
            });
            $("#reset").on('click',function(){
                $("#frm_trade_history").find('input[type=text]').val('');
                $("#second_pair").val($("#second_pair option:first").val());  
                $("#search").click();
            });
            
            
        }); 
    </script>

<?php if ($this->uri->segment(1)=='exchange') { ?>
    <script type="text/javascript">

        //Chat comment POst Function
        $(function(){
            $("#message_form").on("submit", function(event) {
                event.preventDefault();
                var inputdata = $("#message_form").serialize();

                $.ajax({
                    url: "<?php echo base_url('home/ajaxMessageChat'); ?>",
                    type: "post",
                    data: inputdata,
                    dataType : "json",
                    success: function(data) {     
                        $("#live_chat_list").append("<li> <div class='message-top'><img src='<?php echo base_url('assets/website/img/img-user.png') ?>' alt='User'/><span>"+data.datetime+"</span></div><p class='msg-text'>"+data.message+"</p></li>");                
                        document.getElementById("message_form").reset();
                    },
                    error: function(data){
                        $("#live_chat").append("<pre>"+data+"</pre>");
                    }

                });
            });
        });



        $(document).ready( function() {
            done();
            done01();
            done1();
            //done2();
            done02();

            //////////////

            updates_buy();
            updates_sell();
            marketupdates();
            tradehistoryupdates();

        });
         
        function done() {
            setTimeout( function() { 
            // updates();
            updates_buy();
            done();
            }, 15000);
        }
         
        function done01() {
            setTimeout( function() { 
            // updates();
            updates_sell();
            done01();
            }, 20000);
        }
         
        function done1() {
            setTimeout( function() {
            marketupdates();
            done1();
            }, 15000);
        }
         
        function done2() {
            setTimeout( function() { 
            messageChat(); 
            done2();
            }, 18000);
        }
         
        function done02() {
            setTimeout( function() {         
            tradehistoryupdates(); 
            done02();
            }, 20000);
        }
         
         //Message Ajax load
        function messageChat() {
            $.getJSON("<?php echo base_url('home/jsonMessageStream'); ?>", function(data) {
                $("#live_chat_list").empty();

                // console.log(data);
                $.each(data, function(index, element){
                   $("#live_chat_list").prepend("<li> <div class='message-top'><img src='<?php echo base_url('assets/website/img/img-user.png') ?>' alt='User'/><span>"+element.datetime+"</span></div><p class='msg-text'>"+element.message+"</p></li>");

                });
            });
        }
         
        //Market coinpair load 
        function marketupdates() {
            $.getJSON("<?php echo base_url('home/market_streamer'); ?>?market=<?php echo implode('_', $coin_symbol) ?>", function(data) {

                $.each(data.marketstreamer, function(index, element){                

                    $('#price_'+element.market_symbol).text(parseFloat(element.last_price).toFixed(8));
                    $('#volume_'+element.market_symbol).text(Math.round(element.total_coin_supply*100)/100);

                    var change = element.price_change_24h/element.last_price;
                    var price_change_percent = (Math.round(change*100)/100)*100;

                    $('#price_change_'+element.market_symbol).text(parseFloat(price_change_percent.toFixed(2)).toString()+'%');
                    if (change>0) {
                        $('#price_change_'+element.market_symbol).addClass("positive");
                        $('#price_change_'+element.market_symbol).removeClass('negative');

                    }
                    else if (change<0) {
                        $('#price_change_'+element.market_symbol).addClass("negative");
                        $('#price_change_'+element.market_symbol).removeClass('positive');

                    }
                    else {
                        $('#price_change_'+element.market_symbol).removeClass('positive');
                        $('#price_change_'+element.market_symbol).removeClass('negative');

                    };

                });
            });
        }
        //Buy Orders load
        function updates_buy() {
            $.getJSON("<?php echo base_url('home/streamer_buy'); ?>?market=<?php echo implode('_', $coin_symbol) ?>", function(data) {
                $("#buytrades").empty();
                let price = data.price;

                $.each(data.trades, function(index, element){
                    var tradeType = "BAD_REQUEST";
                    var cls     = "";
                    if (element.bid_type=='BUY') {
                        tradeType   = "BUY";
                        cls       ="positive";
                        let html = '';

                        if(price.indexOf(element.bid_price) != -1 ){
                            html = "<tr  style='background: #d4edd9'><td class='buy_price'><strong>"+ parseFloat(element.bid_price).toFixed(8) +"</strong></td><td class='buy_qty text-right'><strong>"+ parseFloat(element.total_qty).toFixed(6) +"</strong></td><td class='text-right'><strong>"+ parseFloat(parseFloat(element.total_price).toFixed(8)).toFixed(6) +"</strong></td></tr>";
                        } else {
                            html = "<tr><td class='buy_price'>"+ parseFloat(element.bid_price).toFixed(8) +"</td><td class='buy_qty text-right'>"+ parseFloat(element.total_qty).toFixed(6) +"</td><td class='text-right'>"+ parseFloat(parseFloat(element.total_price).toFixed(8)).toFixed(6) +"</td></tr>";
                        }

                        $("#buytrades").prepend(html);
                    }
                    else {

                        tradeType   = "BAD_REQUEST";
                        cls       = "";

                    }

                    //Max Row Show From Stemar
                    var maxTableRow = 22;
                    var length = $('table tbody#buytrades tr').length;
                    if (length >= (maxTableRow)) {
                        $('table tbody#buytrades tr:last').remove();
                    }

                });
            });
        }
         
        //Sell Orders load
        function updates_sell() {
            $.getJSON("<?php echo base_url('home/streamer_sell'); ?>?market=<?php echo implode('_', $coin_symbol) ?>", function(data) {
                $("#selltrades").empty();
                let price = data.price;

                $.each(data.trades, function(index, element){

                    var tradeType = "BAD_REQUEST";
                    var cls     = "";
                    if (element.bid_type=='SELL') {
                        tradeType   = "SELL";
                        cls       ="negative";
                        let html = '';
                        if(price.indexOf(element.bid_price) != -1 ){
                            html = "<tr style='background: #f8d6d9'><td class='sell_price'><strong>"+  parseFloat(element.bid_price).toFixed(8) +"</strong></td><td class='sell_qty text-right'><strong>"+ parseFloat(element.total_qty).toFixed(6) +"</strong></td><td class='text-right'><strong>"+ parseFloat(parseFloat(element.total_price).toFixed(8)).toFixed(6) +"</strong></td></tr>";
                        } else {
                            html = "<tr><td class='sell_price'>"+  parseFloat(element.bid_price).toFixed(8) +"</td><td class='sell_qty text-right'>"+ parseFloat(element.total_qty).toFixed(6) +"</td><td class='text-right'>"+ parseFloat(parseFloat(element.total_price).toFixed(8)).toFixed(6) +"</td></tr>";
                        }
                        
                        $("#selltrades").prepend(html);
                    }
                    else {

                        tradeType   = "BAD_REQUEST";
                        cls       = "";

                    }

                    //Max Row Show From Stemar
                    var maxTableRow = 22;
                    var length1 = $('table tbody#selltrades tr').length;
                    if (length1 >= (maxTableRow)) {
                        $('table tbody#selltrades tr:last').remove();
                    }

                });
            });
        }
         
        //Historycal data load
        function tradehistoryupdates() {
            $.getJSON('<?php echo base_url("home/tradehistory/") ?>?market=<?php echo implode('_', $coin_symbol) ?>', function(data) {
                $("#tradeHistory").empty();   

                var lastprice;
                if (data.available_buy_coin!=null) {
                    $(".available_buy_coin").html(parseFloat(data.available_buy_coin.bid_qty_available||0).toFixed(8));

                }else{
                    $(".available_buy_coin").html(0.00);
                    
                }
                if (data.available_sell_coin!=null) {
                    $(".available_sell_coin").html(parseFloat(data.available_sell_coin.bid_qty_available||0).toFixed(8));

                }else{
                    $(".available_sell_coin").html(0.00);

                }

                if (data.coinhistory) {

                    var change = data.coinhistory.price_change_24h/data.coinhistory.last_price;
                    var price_change_percent = (Math.round(change*100)/100)*100;

                    if (change>0) {
                        $(".price_updown").html(parseFloat(data.coinhistory.last_price).toFixed(8)+' <i class="fa fa-arrow-up" aria-hidden="true"></i>');
                        $('.price_updown').addClass("positive");
                        $('.coin-change-price').addClass("positive");
                        $('.price_updown').removeClass("negative");
                        $('.coin-change-price').removeClass("negative");
                    }
                    else if(change<0) {
                        $(".price_updown").html(parseFloat(data.coinhistory.last_price).toFixed(8)+' <i class="fa fa-arrow-down" aria-hidden="true"></i>');
                        $('.price_updown').addClass("negative");
                        $('.coin-change-price').removeClass("positive");
                        $('.price_updown').addClass("negative");
                        $('.coin-change-price').addClass("negative");
                    }else{

                        $(".price_updown").html(parseFloat(data.coinhistory.last_price).toFixed(8));
                        $('.price_updown').removeClass('positive');
                        $('.price_updown').removeClass("coin-change-price");
                        $('.price_updown').removeClass('positive');
                        $('.price_updown').removeClass("coin-change-price");
                    }

                    if (typeof(data.coinhistory.last_price)!=='undefined' || typeof(data.coinhistory.last_price)!='null') {
                        var last_price = data.coinhistory.last_price||0;
                        $(".coin-last-price").html(parseFloat(last_price).toFixed(8));
                    };
                    if (typeof(data.coinhistory.volume_24h)!=='undefined' || typeof(data.coinhistory.volume_24h)!='null') {
                        var volume_24h = data.coinhistory.volume_24h;
                        $(".total_volume").html(parseFloat(volume_24h).toFixed(8));

                    };
                    if (typeof(data.coinhistory.price_change_24h)!=='undefined' || typeof(data.coinhistory.price_change_24h)!='null') {
                        var price_change_24h = data.coinhistory.price_change_24h||0;
                        var price_change_percent = (Math.round((price_change_24h/last_price)*100)/100)*100;
                        $(".coin-change-price").html(parseFloat(price_change_percent.toFixed(2)).toFixed(8)+'%');

                    };
                    if (typeof(data.coinhistory.price_high_24h)!=='undefined' || typeof(data.coinhistory.price_high_24h)!='null') {
                        var price_high_24h = data.coinhistory.price_high_24h||0;
                        $(".coin-price-high").html(parseFloat(price_high_24h).toFixed(8));
                    };
                    if (typeof(data.coinhistory.price_low_24h)!=='undefined' || typeof(data.coinhistory.price_low_24h)!='null') {
                        var price_low_24h = data.coinhistory.price_low_24h||0;
                        $(".coin-price-low").html(parseFloat(price_low_24h).toFixed(8));
                    };
                };

                // console.log(data);
                $.each(data.tradehistory, function(index, element){
                    // console.log(element);

                    var tradeType = "BAD_REQUEST";
                    var cls     = "";
                    var cls1     = "";
                    if (element.bid_type=='BUY') {
                        tradeType   = "BUY";
                        cls       ="positive";
                        cls1       ="buy_price";

                    }
                    else if (element.bid_type=='SELL') {
                        tradeType   = "SELL";
                        cls       ="negative";
                        cls1       ="sell_price";

                    }
                    else {

                        tradeType   = "BAD_REQUEST";
                        cls       = "";

                    }

                    $("#tradeHistory").append("<tr><td class='date "+cls+"'>"+element.success_time+"</td><td class='amount text-right'>"+ parseFloat(element.complete_qty).toFixed(6) +"</td><td class='price text-right "+cls1+"'>"+ parseFloat(element.bid_price).toFixed(8) +"</td></tr>");

                    //<td class='total text-right'>"+ parseFloat(element.complete_amount).toFixed(8) +"</td>

                    //Max Row Show From Stemar
                    var maxTableRow = 18;
                    var length = $('table tbody#tradeHistory tr').length;
                    if (length >= (maxTableRow)) {
                        $('table tbody#tradeHistory tr:last').remove();
                    }

                });
            });
        }
    </script>

    <!-- Market Price From Market place -->
    <script type="text/javascript">
        $(document).ready( function() {

            $.getJSON("<?php echo base_url('home/coin_pairs'); ?>", function(data) {
                $.each(data.coin_pairs, function(index, element){
                    var cryptolistfrom = element.market_symbol; 
                    var cryptolistto = element.currency_symbol;

                    $.getJSON("https://min-api.cryptocompare.com/data/price?fsym="+cryptolistto+"&tsyms="+cryptolistfrom+"", function(result) {
                            if (result[Object.keys(result)[0]]=='Error') {
                                $('#price_'+element.market_symbol).text("<?php echo @$market_details->initial_price; ?>");
                                
                            }
                            else if ($('#price_'+cryptolistto+'_'+cryptolistfrom).text() == '0.00' || $('#price_'+cryptolistto+'_'+cryptolistfrom).text() == '0') {
                                $('#price_'+cryptolistto+'_'+cryptolistfrom).text(result[Object.keys(result)[0]]);

                            };

                    });

                });
            });
            
        });
        
    </script>


    <!-- Buy Sell market/Initial price -->
    <script type="text/javascript">

        $(document).ready( function() {
            var cryptolistfrom = "<?php echo $coin_symbol[0] ?>"; 
            var cryptolistto = "<?php echo $coin_symbol[1] ?>";

            $.getJSON("https://min-api.cryptocompare.com/data/price?fsym="+cryptolistfrom+"&tsyms="+cryptolistto+"", function(result) {

                    var rate = 1;
                    if (result[Object.keys(result)[0]]=='Error') {
                        rate = "<?php echo @$market_details->initial_price==''?0:@$market_details->initial_price; ?>";

                    }else{
                        rate = parseFloat(parseFloat(result[Object.keys(result)[0]]).toFixed(8)).toFixed(8);
                    };


                    $( "#buypricing").val(rate);
                    $( "#sellpricing").val(rate);

                    var buyfees = <?php echo (@$fee_to->fees=='')?0:@$fee_to->fees; ?>;
                    var sellfees = <?php echo (@$fee_from->fees=='')?0:@$fee_from->fees; ?>;


                    var buywithout_feesval = rate*1;              
                    buywithout_feesval = buywithout_feesval.toFixed(8);       

                    $("#buywithout_fees").text(parseFloat(buywithout_feesval).toFixed(8));
                    $('#buywithout_feesval').val(parseFloat(buywithout_feesval).toFixed(8));
                    var feetxt = (buyfees/100)*(buywithout_feesval);
                    feetxt = feetxt.toFixed(8);
                    var fees = $("#buyfees").text(parseFloat(feetxt).toFixed(8)+' <?php echo $coin_symbol[1] ?> ('+buyfees+'%)');
                    $('#buyfeesval').val(feetxt);
                    var total = +buywithout_feesval + +feetxt;
                    $("#buytotal").text(parseFloat(total.toFixed(8)).toFixed(8));
                    $('#buytotalval').val(parseFloat(total.toFixed(8)).toFixed(8));




                    var sellwithout_fees =    rate*1;           
                    var sellwithout_fees =    sellwithout_fees.toFixed(8);           

                    $("#sellwithout_fees").text(parseFloat(sellwithout_fees).toFixed(8));
                    $('#sellwithout_feesval').val(1);
                    var feetxt = (sellfees/100)*1;
                    var feetxt2 = (sellfees/100)*sellwithout_fees;
                    feetxt = feetxt.toFixed(8);
                    feetxt2 = feetxt2.toFixed(8);
                    //var fees = $("#sellfees").text(feetxt+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
                    $("#sellfees").text(parseFloat(feetxt2).toFixed(8)+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
                    $('#sellfeesval').val(parseFloat(feetxt).toFixed(8));

                    var total = 1 + +feetxt;        
                    var total2 = +sellwithout_fees + +feetxt2;
                    $("#selltotal").text(parseFloat(total2.toFixed(8)).toFixed(8));
                    //$("#selltotal").text(total2);
                    $('#selltotalval').val(parseFloat(total).toFixed(8));
               
            });
        });


        $('body').on('click','.buy_price, .sell_price', function() {
            var buy_price = $( this ).text();
            $( "#buypricing").val(buy_price);
            $( "#sellpricing").val(buy_price);

        });
        $('body').on('click','.buy_qty', function() {
            var buy_qty = $( this ).text();
            $( "#buyamount").val(buy_qty);
            $( "#sellamount").val(buy_qty);


            var sellwithout_fees =    buy_qty*1;           
            var sellwithout_fees =    sellwithout_fees.toFixed(8);           

            $("#sellwithout_fees").text(parseFloat(sellwithout_fees).toFixed(8));
            $('#sellwithout_feesval').val(1);
            var feetxt = (sellfees/100)*1;
            var feetxt2 = (sellfees/100)*sellwithout_fees;
            feetxt = feetxt.toFixed(8);
            feetxt2 = feetxt2.toFixed(8);
            //var fees = $("#sellfees").text(feetxt+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
            $("#sellfees").text(parseFloat(feetxt2).toFixed(8)+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
            $('#sellfeesval').val(parseFloat(feetxt).toFixed(8));

            var total = 1 + +feetxt;        
            var total2 = +sellwithout_fees + +feetxt2;
            $("#selltotal").text(parseFloat(total2.toFixed(8)).toFixed(8));
            //$("#selltotal").text(total2);
            $('#selltotalval').val(parseFloat(total).toFixed(8));

        });
        $('body').on('click','.sell_qty', function() {
            var buy_qty = $( this ).text();
            $( "#buyamount").val(buy_qty);
            $( "#sellamount").val(buy_qty);


            var sellwithout_fees =    buy_qty*1;           
            var sellwithout_fees =    sellwithout_fees.toFixed(8);           

            $("#sellwithout_fees").text(parseFloat(sellwithout_fees).toFixed(8));
            $('#sellwithout_feesval').val(1);
            var feetxt = (sellfees/100)*1;
            var feetxt2 = (sellfees/100)*sellwithout_fees;
            feetxt = feetxt.toFixed(8);
            feetxt2 = feetxt2.toFixed(8);
            //var fees = $("#sellfees").text(feetxt+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
            $("#sellfees").text(parseFloat(feetxt2).toFixed(8)+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
            $('#sellfeesval').val(parseFloat(feetxt).toFixed(8));

            var total = 1 + +feetxt;        
            var total2 = +sellwithout_fees + +feetxt2;
            $("#selltotal").text(parseFloat(total2.toFixed(8)).toFixed(8));
            //$("#selltotal").text(total2);
            $('#selltotalval').val(parseFloat(total).toFixed(8));

        });

        var buyfees = <?php echo (@$fee_to->fees=='')?0:@$fee_to->fees; ?>;
        $("#buypricing").on("keyup", function(event) {
            event.preventDefault();

            var buypricing = parseFloat($("#buypricing").val())||1;
            var buyamount = parseFloat($("#buyamount").val())||1;

            var buywithout_feesval = buypricing*buyamount;              
            buywithout_feesval = buywithout_feesval.toFixed(8);       

            $("#buywithout_fees").text(parseFloat(buywithout_feesval).toFixed(8));
            $('#buywithout_feesval').val(parseFloat(buywithout_feesval).toFixed(8));
            var feetxt = (buyfees/100)*(buywithout_feesval);
            feetxt = feetxt.toFixed(8);
            var fees = $("#buyfees").text(parseFloat(feetxt).toFixed(8)+' <?php echo $coin_symbol[1] ?> ('+buyfees+'%)');
            $('#buyfeesval').val(parseFloat(feetxt).toFixed(8));
            var total = +buywithout_feesval + +feetxt;
            $("#buytotal").text(parseFloat(total.toFixed(8)).toFixed(8));
            $('#buytotalval').val(parseFloat(total.toFixed(8)).toFixed(8));

        }); 
        $("#buyamount").on("keyup", function(event) {
            event.preventDefault();

            var buypricing = parseFloat($("#buypricing").val())||1;
            var buyamount = parseFloat($("#buyamount").val())||1;

            var buywithout_feesval = buypricing*buyamount;              
            buywithout_feesval = buywithout_feesval.toFixed(8); 

            $("#buywithout_fees").text(parseFloat(buywithout_feesval).toFixed(8));
            $('#buywithout_feesval').val(parseFloat(buywithout_feesval).toFixed(8));
            var feetxt = (buyfees/100)*(buywithout_feesval);
            feetxt = feetxt.toFixed(8);
            var fees = $("#buyfees").text(parseFloat(feetxt).toFixed(8)+' <?php echo $coin_symbol[1] ?> ('+buyfees+'%)');
            $('#buyfeesval').val(feetxt);
            var total = +buywithout_feesval + +feetxt;
            $("#buytotal").text(parseFloat(total.toFixed(8)).toFixed(8));
            $('#buytotalval').val(parseFloat(total.toFixed(8)).toFixed(8));

        });

        $(function(){
            $("#buyform").on("submit", function(event) {
                event.preventDefault();
                var inputdata = $("#buyform").serialize();
                var buyPrice = $( "#buypricing").val();

                $.ajax({
                    url: "<?php echo base_url('home/buy'); ?>",
                    type: "post",
                    data: inputdata,
                    success: function(data) {

                        console.log(data);

                        if (data==0) {
                            $(".buyloginMessage").html("<p class='alert-danger'>Trade dose not submited</p>");
                            
                        }else if (data==1) {
                            $(".buyloginMessage").html("<p class='alert-warning'>Please Login/Register!</p>");

                        }else if (data==2) {
                            $(".buyloginMessage").html("<p class='alert-warning'>You Have not sufficent Balance!</p>");

                        } else if (data == 99) {
                            $(".buyloginMessage").html("<p class='alert-warning'>Order failed: Total cannot be lower than 0.00001</p>");
                        }else {

                            $(".buyloginMessage").html("<p class='alert-success'>Your Order Requested Successfully</p>");

                            var trade = JSON.parse(data);                        
                            $("#balance_buy").text(parseFloat(trade.balance).toFixed(8));
                            $("#balance_sell").text(parseFloat(trade.balance_up_to).toFixed(8));
                            // $("#buytrades").prepend("<tr><td class='buy_price text-right'>"+trade.trades.bid_price+"</td><td class=' text-right'>"+trade.trades.bid_qty+"</td><td class=' text-right'>"+trade.trades.total_amount+"</td></tr>");

                        }
                        
                        document.getElementById("buyform").reset();


                        var cryptolistfrom = "<?php echo $coin_symbol[0] ?>"; 
                        var cryptolistto = "<?php echo $coin_symbol[1] ?>";

                        $.getJSON("https://min-api.cryptocompare.com/data/price?fsym="+cryptolistfrom+"&tsyms="+cryptolistto+"", function(result) {

                                var rate = 1;
                                if (result[Object.keys(result)[0]]=='Error') {
                                    rate = "<?php echo @$market_details->initial_price==''?0:@$market_details->initial_price; ?>";

                                }else{
                                    rate = parseFloat(parseFloat(result[Object.keys(result)[0]]).toFixed(8)).toFixed(8);
                                };


                                $( "#buypricing").val(buyPrice);
                                // $( "#sellpricing").val(rate);

                                var buyfees = <?php echo (@$fee_to->fees=='')?0:@$fee_to->fees; ?>;
                                var sellfees = <?php echo (@$fee_from->fees=='')?0:@$fee_from->fees; ?>;

                                var buywithout_feesval = rate*1;              
                                buywithout_feesval = buywithout_feesval.toFixed(8);       

                                $("#buywithout_fees").text(parseFloat(buywithout_feesval).toFixed(8));
                                $('#buywithout_feesval').val(parseFloat(buywithout_feesval).toFixed(8));
                                var feetxt = (buyfees/100)*(buywithout_feesval);
                                feetxt = feetxt.toFixed(8);
                                var fees = $("#buyfees").text(parseFloat(feetxt).toFixed(8)+' <?php echo $coin_symbol[1] ?> ('+buyfees+'%)');
                                $('#buyfeesval').val(feetxt);
                                var total = +buywithout_feesval + +feetxt;
                                $("#buytotal").text(parseFloat(total.toFixed(8)).toFixed(8));
                                $('#buytotalval').val(parseFloat(total.toFixed(8)).toFixed(8));




                                var sellwithout_fees =    rate*1;           
                                var sellwithout_fees =    sellwithout_fees.toFixed(8);           

                                $("#sellwithout_fees").text(parseFloat(sellwithout_fees).toFixed(8));
                                $('#sellwithout_feesval').val(1);
                                var feetxt = (sellfees/100)*1;
                                var feetxt2 = (sellfees/100)*sellwithout_fees;
                                feetxt = feetxt.toFixed(8);
                                feetxt2 = feetxt2.toFixed(8);
                                //var fees = $("#sellfees").text(feetxt+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
                                $("#sellfees").text(parseFloat(feetxt2).toFixed(8)+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
                                $('#sellfeesval').val(parseFloat(feetxt).toFixed(8));

                                var total = 1 + +feetxt;        
                                var total2 = +sellwithout_fees + +feetxt2;
                                $("#selltotal").text(parseFloat(total2.toFixed(8)).toFixed(8));
                                //$("#selltotal").text(total2);
                                $('#selltotalval').val(parseFloat(total).toFixed(8));

                        });
                    },
                    error: function(data){
                        $(".buyloginMessage").prepend("<pre>"+data+"</pre>");
                    }

                });
            });
        });
    </script>
    <!-- Ajax Sell -->
    <script type="text/javascript">   
        var sellfees = <?php echo (@$fee_from->fees=='')?0:@$fee_from->fees; ?>;
        $("#sellpricing").on("keyup", function(event) {
            event.preventDefault();

            var sellpricing = parseFloat($("#sellpricing").val())||0;
            var sellamount = parseFloat($("#sellamount").val())||0; 

            var sellwithout_fees =    sellpricing*sellamount;           
            var sellwithout_fees =    sellwithout_fees.toFixed(8);           

            $("#sellwithout_fees").text(parseFloat(sellwithout_fees).toFixed(8));
            $('#sellwithout_feesval').val(parseFloat(sellamount.toFixed(8)).toFixed(8));
            var feetxt = (sellfees/100)*sellamount;
            var feetxt2 = (sellfees/100)*sellwithout_fees;
            feetxt = feetxt.toFixed(8);
            feetxt2 = feetxt2.toFixed(8);
            //var fees = $("#sellfees").text(feetxt+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
            $("#sellfees").text(parseFloat(feetxt2).toFixed(8)+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
            $('#sellfeesval').val(parseFloat(feetxt).toFixed(8));

            var total = +sellamount + +feetxt;        
            var total2 = +sellwithout_fees + +feetxt2;
            $("#selltotal").text(parseFloat(total2.toFixed(8)).toFixed(8));
            //$("#selltotal").text(total2);
            $('#selltotalval').val(parseFloat(total).toFixed(8));


        }); 
        $("#sellamount").on("keyup", function(event) {
            event.preventDefault();

            var sellpricing = parseFloat($("#sellpricing").val())||1;
            var sellamount = parseFloat($("#sellamount").val())||1;

            var sellwithout_fees =    sellpricing*sellamount;           
            var sellwithout_fees =    sellwithout_fees.toFixed(8);

            $("#sellwithout_fees").text(parseFloat(sellwithout_fees).toFixed(8));
            $('#sellwithout_feesval').val(parseFloat(sellamount.toFixed(8)).toFixed(8));
            var feetxt = (sellfees/100)*sellamount;
            var feetxt2 = (sellfees/100)*sellwithout_fees;
            feetxt = feetxt.toFixed(8);
            feetxt2 = feetxt2.toFixed(8);
            // var fees = $("#sellfees").text(feetxt2+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
            $("#sellfees").text(parseFloat(feetxt2).toFixed(8)+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
            $('#sellfeesval').val(parseFloat(feetxt).toFixed(8));

            var total = +sellamount + +feetxt;
            var total2 = +sellwithout_fees + +feetxt2;
            $("#selltotal").text(parseFloat(total2).toFixed(8));
            $('#selltotalval').val(parseFloat(total).toFixed(8));

        });



        $(function(){
            $("#sellform").on("submit", function(event) {
                event.preventDefault();
                var inputdata = $("#sellform").serialize();
                var sellPrice = $("#sellpricing").val();
                $.ajax({
                    url: "<?php echo base_url('home/sell'); ?>",
                    type: "post",
                    data: inputdata,
                    success: function(data) {                            

                        //console.log(data);

                        if (data==0) {
                            $(".sellloginMessage").html("<p class='alert-danger'>Trade dose not submited</p>");
                            
                        }else if (data==1) {
                            $(".sellloginMessage").html("<p class='alert-warning'>Please Login/Register!</p>");

                        }else if (data==2) {
                            $(".sellloginMessage").html("<p class='alert-warning'>You Have not sufficent Balance!</p>");

                        } else if (data == 99) {
                            $(".sellloginMessage").html("<p class='alert-warning'>Order failed: Total cannot be lower than 0.00001</p>");
                        }else {

                            $(".sellloginMessage").html("<p class='alert-success'>Your Order Requested Successfully</p>");

                            var trade = JSON.parse(data);
                            //console.log(trade);
                            $("#balance_sell").text(parseFloat(trade.balance).toFixed(8));
                            $("#balance_buy").text(parseFloat(trade.balance_up_to).toFixed(8));
                            // $("#selltrades").prepend("<tr><td class='sell_price'>"+trade.trades.bid_price+"</td><td>"+trade.trades.bid_qty+"</td><td>"+trade.trades.total_amount+"</td></tr>");

                        }
                        
                        document.getElementById("sellform").reset();

                        var cryptolistfrom = "<?php echo $coin_symbol[0] ?>"; 
                        var cryptolistto = "<?php echo $coin_symbol[1] ?>";

                        $.getJSON("https://min-api.cryptocompare.com/data/price?fsym="+cryptolistfrom+"&tsyms="+cryptolistto+"", function(result) {

                                var rate = 1;
                                if (result[Object.keys(result)[0]]=='Error') {
                                    rate = "<?php echo @$market_details->initial_price==''?0:@$market_details->initial_price; ?>";

                                }else{
                                    rate = parseFloat(parseFloat(result[Object.keys(result)[0]]).toFixed(8)).toFixed(8);
                                };

                                // $( "#buypricing").val(rate);
                                $( "#sellpricing").val(sellPrice);


                                var buyfees = <?php echo (@$fee_to->fees=='')?0:@$fee_to->fees; ?>;
                                var sellfees = <?php echo (@$fee_from->fees=='')?0:@$fee_from->fees; ?>;

                                var buywithout_feesval = rate*1;              
                                buywithout_feesval = buywithout_feesval.toFixed(8);       

                                $("#buywithout_fees").text(parseFloat(buywithout_feesval).toFixed(8));
                                $('#buywithout_feesval').val(parseFloat(buywithout_feesval).toFixed(8));
                                var feetxt = (buyfees/100)*(buywithout_feesval);
                                feetxt = feetxt.toFixed(8);
                                var fees = $("#buyfees").text(parseFloat(feetxt).toFixed(8)+' <?php echo $coin_symbol[1] ?> ('+buyfees+'%)');
                                $('#buyfeesval').val(feetxt);
                                var total = +buywithout_feesval + +feetxt;
                                $("#buytotal").text(parseFloat(total.toFixed(8)).toFixed(8));
                                $('#buytotalval').val(parseFloat(total.toFixed(8)).toFixed(8));




                                var sellwithout_fees =    rate*1;           
                                var sellwithout_fees =    sellwithout_fees.toFixed(8);           

                                $("#sellwithout_fees").text(parseFloat(sellwithout_fees).toFixed(8));
                                $('#sellwithout_feesval').val(1);
                                var feetxt = (sellfees/100)*1;
                                var feetxt2 = (sellfees/100)*sellwithout_fees;
                                feetxt = feetxt.toFixed(8);
                                feetxt2 = feetxt2.toFixed(8);
                                //var fees = $("#sellfees").text(feetxt+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
                                $("#sellfees").text(parseFloat(feetxt2).toFixed(8)+' <?php echo $coin_symbol[1] ?> ('+sellfees+'%)');
                                $('#sellfeesval').val(parseFloat(feetxt).toFixed(8));

                                var total = 1 + +feetxt;        
                                var total2 = +sellwithout_fees + +feetxt2;
                                $("#selltotal").text(parseFloat(total2.toFixed(8)).toFixed(8));
                                //$("#selltotal").text(total2);
                                $('#selltotalval').val(parseFloat(total).toFixed(8));

                            
                        });

                    },
                    error: function(data){
                        $(".sellloginMessage").prepend("<pre>"+data+"</pre>");
                    }
                });
            });
        });

    </script>

    <script type="text/javascript">

        //Market Depth
        var chart = AmCharts.makeChart("marketDepth", {
            "type": "serial",
            "theme": "patterns",
            "dataLoader": {
                "url": '<?php echo base_url("home/market_depth/") ?>?market=<?php echo implode('_', $coin_symbol) ?>',
                "format": "json",
                "reload": 120,
                "showErrors": false,
                "postProcess": function (data) {

                    // Function to process (sort and calculate cummulative volume)
                    function processData(list, type, desc) {

                        // Convert to data points
                        for (var i = 0; i < list.length; i++) {
                            list[i] = {
                                value: Number(list[i][0]),
                                volume: Number(list[i][1])
                            };
                        }

                        // Sort list just in case
                        list.sort(function (a, b) {
                            if (a.value > b.value) {
                                return 1;
                            } else if (a.value < b.value) {
                                return -1;
                            } else {
                                return 0;
                            }
                        });

                        // Calculate cummulative volume
                        if (desc) {
                            for (var i = list.length - 1; i >= 0; i--) {
                                if (i < (list.length - 1)) {
                                    list[i].totalvolume = list[i + 1].totalvolume + list[i].volume;
                                } else {
                                    list[i].totalvolume = list[i].volume;
                                }
                                var dp = {};
                                dp["value"] = list[i].value;
                                dp[type + "volume"] = list[i].volume;
                                dp[type + "totalvolume"] = list[i].totalvolume;
                                res.unshift(dp);
                            }
                        } else {
                            for (var i = 0; i < list.length; i++) {
                                if (i > 0) {
                                    list[i].totalvolume = list[i - 1].totalvolume + list[i].volume;
                                } else {
                                    list[i].totalvolume = list[i].volume;
                                }
                                var dp = {};
                                dp["value"] = list[i].value;
                                dp[type + "volume"] = list[i].volume;
                                dp[type + "totalvolume"] = list[i].totalvolume;
                                res.push(dp);
                            }
                        }

                    }

                    // Init
                    var res = [];
                    processData(data.bids, "bids", true);
                    processData(data.asks, "asks", false);

                    //console.log(res);
                    return res;
                }
            },
            "graphs": [{
                    "id": "bids",
                    "fillAlphas": 0.2,
                    "lineAlpha": 1,
                    "lineThickness": 2,
                    "lineColor": "#0f0",
                    "type": "step",
                    "valueField": "bidstotalvolume",
                    "balloonFunction": balloon
                }, {
                    "id": "asks",
                    "fillAlphas": 0.2,
                    "lineAlpha": 1,
                    "lineThickness": 2,
                    "lineColor": "#f00",
                    "type": "step",
                    "valueField": "askstotalvolume",
                    "balloonFunction": balloon
                }, {
                    "lineAlpha": 0,
                    "fillAlphas": 0.2,
                    "lineColor": "#0f0",
                    "type": "column",
                    "clustered": false,
                    "valueField": "bidsvolume",
                    "showBalloon": true
                }, {
                    "lineAlpha": 0,
                    "fillAlphas": 0.2,
                    "lineColor": "#f00",
                    "type": "column",
                    "clustered": false,
                    "valueField": "asksvolume",
                    "showBalloon": true
                }],
            "categoryField": "value",
            "chartCursor": {},
            "balloon": {
                "textAlign": "left"
            },
            "valueAxes": [{
                    "title": "Volume"
                }],
            "categoryAxis": {
                "title": "Price (<?php echo $coin_symbol[0] ?>/<?php echo $coin_symbol[1] ?>)",
                "minHorizontalGap": 100,
                "startOnAxis": true,
                "showFirstLabel": false,
                "showLastLabel": false
            },
            "export": {
                "enabled": true
            }
        });

        function balloon(item, graph) {
            var txt;
            if (graph.id === "asks") {
                txt = "Ask: <strong>" + formatNumber(item.dataContext.value, graph.chart, 4) + "</strong><br />"
                        + "Total volume: <strong>" + formatNumber(item.dataContext.askstotalvolume, graph.chart, 4) + "</strong><br />"
                        + "Volume: <strong>" + formatNumber(item.dataContext.asksvolume, graph.chart, 4) + "</strong>";
            } else {
                txt = "Bid: <strong>" + formatNumber(item.dataContext.value, graph.chart, 4) + "</strong><br />"
                        + "Total volume: <strong>" + formatNumber(item.dataContext.bidstotalvolume, graph.chart, 4) + "</strong><br />"
                        + "Volume: <strong>" + formatNumber(item.dataContext.bidsvolume, graph.chart, 4) + "</strong>";
            }
            return txt;
        }

        function formatNumber(val, chart, precision) {
            return AmCharts.formatNumber(
                val,
                {
                    precision: precision ? precision : chart.precision,
                    decimalSeparator: chart.decimalSeparator,
                    thousandsSeparator: chart.thousandsSeparator
                }
            );
        }

    </script>
<?php } ?>


<?php if($this->uri->segment(1)=='register'){ ?>
    <style>
        #message {
            display:none;
            position: relative;
            padding: 20px;
            margin-top: 10px;
        }
        #message p {
            margin-bottom: 0;
        }
        .password_valid .valid {
            color: green;
        }
        .password_valid .valid:before {
            position: relative;
            left: -10px;
            content: "✔";
        }
        .password_valid .invalid {
            color: red;
        }
        .password_valid .invalid:before {
            position: relative;
            left: -10px;
            content: "✖";
        }
    </style>
    <script type="text/javascript">
        var myInput = document.getElementById("pass");
        var letter  = document.getElementById("letter");
        var capital = document.getElementById("capital");
        var special = document.getElementById("special");
        var number  = document.getElementById("number");
        var length  = document.getElementById("length");

        myInput.onfocus = function() {
            document.getElementById("message").style.display = "block";
        }
        myInput.onblur = function() {
            document.getElementById("message").style.display = "none";
        }

        myInput.onkeyup = function() {

          var lowerCaseLetters = /[a-z]/g;
          if(myInput.value.match(lowerCaseLetters)) {  
            letter.classList.remove("invalid");
            letter.classList.add("valid");
          } else {
            letter.classList.remove("valid");
            letter.classList.add("invalid");
          }

          var upperCaseLetters = /[A-Z]/g;
          if(myInput.value.match(upperCaseLetters)) {  
            capital.classList.remove("invalid");
            capital.classList.add("valid");
          } else {
            capital.classList.remove("valid");
            capital.classList.add("invalid");
          }

          var specialCharacter = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g;
          if(myInput.value.match(specialCharacter)) {  
            special.classList.remove("invalid");
            special.classList.add("valid");
          } else {
            special.classList.remove("valid");
            special.classList.add("invalid");
          }

          var numbers = /[0-9]/g;
          if(myInput.value.match(numbers)) {  
            number.classList.remove("invalid");
            number.classList.add("valid");
          } else {
            number.classList.remove("valid");
            number.classList.add("invalid");
          }

          if(myInput.value.length >= 8) {
            length.classList.remove("invalid");
            length.classList.add("valid");
          } else {
            length.classList.remove("valid");
            length.classList.add("invalid");
          }
        }

        //Confirm Password check
        function rePassword() {
            var pass = document.getElementById("pass").value;
            var r_pass = document.getElementById("r_pass").value;

            if (pass !== r_pass) {
                document.getElementById("r_pass").style.borderColor = '#f00';
                document.getElementById("r_pass").style.boxShadow = '0 0 0 0.2rem rgba(255, 0, 0,.25)';
                return false;
            }
            else{
                document.getElementById("r_pass").style.borderColor = '#ced4da';
                document.getElementById("r_pass").style.boxShadow = 'unset';
                return true;
            }
        }
        //Valid Email Address Check
        function checkEmail() {
            var email = document.getElementById('email');
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if (!filter.test(email.value)) {
                document.getElementById("email").style.borderColor = '#f00';
                document.getElementById("email").style.boxShadow = '0 0 0 0.2rem rgba(255, 0, 0,.25)';
                return false;
            }
            else{
                document.getElementById("email").style.borderColor = '#ced4da';
                document.getElementById("email").style.boxShadow = 'unset';
                return true;
            }
        }
        //Registration From validation check
        function validateForm() {
            var email     = document.forms["registerForm"]["remail"].value;
            var pass      = document.forms["registerForm"]["rpass"].value;
            var r_pass    = document.forms["registerForm"]["rr_pass"].value;
            var checkbox  = document.forms["registerForm"]["raccept_terms"].value;

            
            if (email == "") {
                alert("Email Required");
                return false;
            }
            if (pass == "") {
                alert("Password Required.");
                return false;
            }
            if (pass.length < 8) {
                alert("Please Enter at least 8 Characters input");
                return false;
            }
            if (r_pass == "") {
                alert("Confirm Password must be filled out");
                return false;
            }
            if (checkbox == "") {
                alert("Must Confirm Privacy Policy and Terms and Conditions");
                return false;
            }
        }
    </script>
<?php } ?>

<?php if($this->uri->segment(1)=='deposit'){ ?>
    <script type="text/javascript">
        function Fee(method){
            
            var amount = document.forms['deposit_form'].elements['deposit_amount'].value;
            var method = document.forms['deposit_form'].elements['method'].value;
            var crypto_coin = document.forms['deposit_form'].elements['crypto_coin'].value;
            var level = document.forms['deposit_form'].elements['level'].value;
            var csrf_test_name = document.forms['deposit_form'].elements['csrf_test_name'].value;

            if (amount!="" || amount==0) {
                $("#payment_method" ).prop("disabled", false);
            }
            if (amount=="" || amount==0) {
                $('#fee').text("Fees is "+0);
            }
            if (amount!="" && method!=""){
                $.ajax({
                    'url': '<?php echo base_url("customer/ajaxload/fees_load");?>',
                    'type': 'POST', //the way you want to send data to your URL
                    'data': {'method': method,'level':level,'amount':amount,'crypto_coin':crypto_coin,'csrf_test_name':csrf_test_name },
                    'dataType': "JSON",
                    'success': function(data) { 
                        if(data){
                            $('[name="amount"]').val(data.amount);
                            $('[name="fees"]').val(data.fees);
                            $('#fee').text("Fees is "+data.fees);                    
                        } else {
                            alert('Error!');
                        }  
                    }
                });
            } 
        }
    </script>
<?php 
$gateway = $this->db->select('*')->from('payment_gateway')->where('identity', 'phone')->where('status',1)->get()->row();
$gateway_bank = $this->db->select('*')->from('payment_gateway')->where('identity', 'bank')->where('status',1)->get()->row();
?>
    <!-- Ajax Payable -->
    <script type="text/javascript">
        $(function(){
            $("#deposit_type").on("change", function(event) {
                event.preventDefault();
                var deposit_type = $("#deposit_type").val()|| 0;

                if (deposit_type=='coin') {
                    $( "#crypto_coin").html(' <option><?php echo display("select_option");?></option><?php foreach ($coin_list as $key => $value) { if($value->symbol!="USD") { ?><option value="<?php echo $value->symbol; ?>" ><?php echo $value->full_name; ?></option><?php } } ?>');

                    $("#crypto_coin").on("change", function(event) {
                        var crypto_coin = $("#crypto_coin").val()|| 0;

                        if (crypto_coin=='BTC') {
                            $( "#payment_method").html('<option><?php echo display("deposit_method");?></option><?php foreach ($payment_gateway as $key => $value) { if ($value->identity=="bitcoin" || $value->identity=="coinpayment" || $value->identity=="payeer" || $value->identity=="token") { ?><option value="<?php echo $value->identity; ?>"><?php echo $value->agent; ?></option><?php } } ?>');
                        }else if(crypto_coin=='BTC' || crypto_coin=='BCH' || crypto_coin=='LTC' || crypto_coin=='LTCT' || crypto_coin=='DASH' || crypto_coin=='DOGE' || crypto_coin=='SPD' || crypto_coin=='RDD' || crypto_coin=='POT' || crypto_coin=='FTC' || crypto_coin=='VTC' || crypto_coin=='PPC' || crypto_coin=='MUE' || crypto_coin=='UNIT'){

                            $( "#payment_method").html('<option><?php echo display("deposit_method");?></option><?php foreach ($payment_gateway as $key => $value) { if ($value->identity=="bitcoin" || $value->identity=="coinpayment" || $value->identity=="token") { ?><option value="<?php echo $value->identity; ?>"><?php echo $value->agent; ?></option><?php } } ?>');

                        }
                        else{
                            $( "#payment_method").html('<option><?php echo display("deposit_method");?></option><?php foreach ($payment_gateway as $key => $value) { if ($value->identity=="coinpayment" || $value->identity=="token") { ?><option value="<?php echo $value->identity; ?>"><?php echo $value->agent; ?></option><?php } } ?>');
                        }

                    });
                }
                else{
                    $( "#crypto_coin").html('<option><?php echo display("select_option");?></option><option value="USD" >US Dollar</option>');
                    $("#crypto_coin").on("change", function(event) {
                        var crypto_coin = $("#crypto_coin").val()|| 0;

                        if(crypto_coin=='USD'){

                            $( "#payment_method").html('<option><?php echo display("deposit_method");?></option><?php foreach ($payment_gateway as $key => $value) { if ($value->identity!="bitcoin" && $value->identity!="coinpayment") { ?><option value="<?php echo $value->identity; ?>"><?php echo $value->agent; ?></option><?php } } ?>');
                            
                        }
                        else{
                            $( "#payment_method").html('<option><?php echo display("deposit_method");?></option>');
                        }
                    });
                }

            });

        });
    <?php
        $json_decode_bank = json_decode(@$gateway_bank->public_key, true);

        $acc_name       = @$json_decode_bank['acc_name'];
        $acc_no         = @$json_decode_bank['acc_no'];
        $branch_name    = @$json_decode_bank['branch_name'];
        $swift_code     = @$json_decode_bank['swift_code'];
        $abn_no         = @$json_decode_bank['abn_no'];
        $country        = @$json_decode_bank['country'];
        $bank_name      = @$json_decode_bank['bank_name'];
    ?>
        $(function(){
            $("#payment_method").on("change", function(event) {
                event.preventDefault();
                var payment_method = $("#payment_method").val()|| 0;

                if (payment_method=='phone') {
                    $( ".payment_info").html("<div class='form-group'><label for='send_money'>Send Money</label><h2><a href='tel:<?=@$gateway->public_key?>'><?=@$gateway->public_key?></a></h2></div><div class='form-group'><label for='om_name'><?php echo display("om_name") ?></label><input name='om_name' class='form-control om_name' type='text' id='om_name' autocomplete='off'></div><div class='form-group'><label for='om_mobile'><?php echo display("om_mobile_no") ?></label><input name='om_mobile' class='form-control om_mobile' type='text' id='om_mobile' autocomplete='off'></div><div class='form-group'><label for='transaction_no'><?php echo display("transaction_no") ?></label><input name='transaction_no' class='form-control transaction_no' type='text' id='transaction_no' autocomplete='off'></div><div class='form-group'><label for='idcard_no'><?php echo display("idcard_no") ?></label><input name='idcard_no' class='form-control idcard_no' type='text' id='idcard_no' autocomplete='off'></div>");

                }else if (payment_method=='bank') {

                    $( ".payment_info").html("<div class='form-group'><label for='send_money'>Account Name</label><h4><?=$acc_name?></h4></div><div class='form-group'><label for='send_money'>Account No</label><h4><?=$acc_no?></h4></div><div class='form-group'><label for='send_money'>Branch Name</label><h4><?=$branch_name?></h4></div><div class='form-group'><label for='send_money'>SWIFT Code</label><h4><?=$swift_code?></h4></div><div class='form-group'><label for='send_money'>ABN No</label><h4><?=$abn_no?></h4></div><div class='form-group'><label for='send_money'>Country</label><h4><?=$country?></h4></div><div class='form-group'><label for='send_money'>Bank Name</label><h4><?=$bank_name?></h4></div><div class='form-group'><label for='document'>Document</label><input name='document' class='form-control document' type='file' id='document' autocomplete='off'></div>");

                }else if (payment_method=='token') {

                    $( ".payment_info").html("<div class='form-group'><label for='comment' class=''>Your Wallet</label><textarea class='form-control' name='comment' id='comment' rows='1'></textarea></div>");
                }
                else{
                    $( ".payment_info").html("<div class='form-group'><label for='comment' class=''><?php echo display('comments');?></label><textarea class='form-control' name='comment' id='comment' rows='3'></textarea></div>");
                }

            });

        });
    </script>

<?php } ?>

<?php if($this->uri->segment(1)=='withdraw'){ ?>

    <!-- Ajax Payable -->
    <script type="text/javascript">
        $(function(){
            $("#withdraw_type").on("change", function(event) {
                event.preventDefault();
                var withdraw_type = $("#withdraw_type").val()|| 0;

                if (withdraw_type=='coin') {
                    $( "#crypto_coin").html(' <option><?php echo display("select_option");?></option><?php foreach ($coin_list as $key => $value) { if($value->symbol!="USD") { ?><option value="<?php echo $value->symbol; ?>" ><?php echo $value->full_name; ?></option><?php } } ?>');

                    $("#crypto_coin").on("change", function(event) {
                        var crypto_coin = $("#crypto_coin").val()|| 0;

                        if (crypto_coin=='BTC') {
                            $( "#payment_method").html('<option><?php echo display("withdraw_method");?></option><?php foreach ($payment_gateway as $key => $value) { if ($value->identity=="bitcoin" || $value->identity=="coinpayment" || $value->identity=="payeer" || $value->identity=="token") { ?><option value="<?php echo $value->identity; ?>"><?php echo $value->agent; ?></option><?php } } ?>');
                        }else if(crypto_coin=='BTC' || crypto_coin=='BCH' || crypto_coin=='LTC' || crypto_coin=='DASH' || crypto_coin=='DOGE' || crypto_coin=='SPD' || crypto_coin=='RDD' || crypto_coin=='POT' || crypto_coin=='FTC' || crypto_coin=='VTC' || crypto_coin=='PPC' || crypto_coin=='MUE' || crypto_coin=='UNIT'){

                            $( "#payment_method").html('<option><?php echo display("withdraw_method");?></option><?php foreach ($payment_gateway as $key => $value) { if ($value->identity=="bitcoin" || $value->identity=="coinpayment" || $value->identity=="token") { ?><option value="<?php echo $value->identity; ?>"><?php echo $value->agent; ?></option><?php } } ?>');

                        }
                        else{
                            $( "#payment_method").html('<option><?php echo display("withdraw_method");?></option><?php foreach ($payment_gateway as $key => $value) { if ($value->identity=="coinpayment" || $value->identity=="token") { ?><option value="<?php echo $value->identity; ?>"><?php echo $value->agent; ?></option><?php } } ?>');
                        }

                    });
                }
                else{
                    $( "#crypto_coin").html('<option><?php echo display("select_option");?></option><option value="USD" >US Dollar</option>');
                    $("#crypto_coin").on("change", function(event) {
                        var crypto_coin = $("#crypto_coin").val()|| 0;

                        if(crypto_coin=='USD'){

                            $( "#payment_method").html('<option><?php echo display("withdraw_method");?></option><?php foreach ($payment_gateway as $key => $value) { if ($value->identity!="bitcoin" && $value->identity!="coinpayment") { ?><option value="<?php echo $value->identity; ?>"><?php echo $value->agent; ?></option><?php } } ?>');
                            
                        }
                        else{
                            $( "#payment_method").html('<option><?php echo display("withdraw_method");?></option>');
                        }
                    });
                }

            });

        });

    </script>

    <script type="text/javascript">
        function WalletId(method){
            
            var csrf_test_name = document.forms['withdraw'].elements['csrf_test_name'].value;
            var crypto_coin = document.forms['withdraw'].elements['crypto_coin'].value;

            $.ajax({
                url: '<?php echo base_url("customer/ajaxload/walletid"); ?>',
                type: 'POST', //the way you want to send data to your URL
                data: {'method': method,'crypto_coin': crypto_coin,'csrf_test_name':csrf_test_name },
                dataType:'JSON',
                success: function(data) { 

                    if(data){
                        if (method=='bank') {
                            var bank = JSON.parse(data.wallet_id);
                            $('[name="walletid"]').val(data.wallet_id);
                            $('button[type=submit]').prop('disabled', false);
                            $('#walletidis').html("<small>Account Name: "+ bank.acc_name +"</small><br><small>Account No: "+ bank.acc_no +"</small><br><small>Branch Name: "+ bank.branch_name +"</small><br><small>SWIFT Code: "+ bank.swift_code +"</small><br><small>ABN No: "+ bank.abn_no +"</small><br><small>Country: "+ bank.country +"</small><br><small>Bank Name: "+ bank.bank_name +"</small><br>");

                        }else{
                            $('[name="walletid"]').val(data.wallet_id);
                            $('button[type=submit]').prop('disabled', false);
                            $('#walletidis').text('Your Wallet Id Is '+data.wallet_id);

                        };
                         $('#coinwallet').html("");
                    
                    } else {

                        if(method=='coinpayment' || method=='token'){
                            $('button[type=submit]').prop('disabled', false);
                            $('#coinwallet').html("<div class='form-group'><label for='amount'>Your Address</label><input class='form-control' name='walet_address' type='text' id='walet_address'></div>");
                            $('#walletidis').text('');

                        }else{
                            $('#coinwallet').html("");
                            $('button[type=submit]').prop('disabled', true);
                            $('#walletidis').text('Your have no withdrawal account');
                        }

                    }  
                }
            });
        }
    </script>
<?php } ?>
    <script type="text/javascript">
        function withdraw(id){

            var code = document.forms['verify'].elements['code'].value;
            var csrf_test_name = document.forms['verify'].elements['csrf_test_name'].value;

            swal({
                title: 'Please Wait......',
                type: 'warning',
                showConfirmButton: false,
                onOpen: function () {
                    swal.showLoading()
                  }
            });


            $.ajax({
                url: '<?php echo base_url("home/withdraw_verify"); ?>',
                type: 'POST', //the way you want to send data to your URL
                data: {'id': id,'code':code,'csrf_test_name':csrf_test_name },
                success: function(data) { 


                    if(data!=''){
                        
                        swal({
                            title: "Good job!",
                            text: "Your Custom Email Send Successfully",
                            type: "success",
                            showConfirmButton: false,
                            timer: 1500,

                        });

                       window.location.href = "<?php echo base_url('withdraw-details/'); ?>"+data;
                        
                    } else {

                        swal({
                            title: "Wops!",
                            text: "Error Message",
                            type: "error",
                            showConfirmButton: false,
                            timer: 1500
                        });

                    }
                    
                }
            });
        }
    </script>
<?php if($this->uri->segment(1)=='transfer'){ ?>

    <script type="text/javascript">
        function ReciverChack(receiver_id){

            var csrf_test_name = document.forms['transfer_form'].elements['csrf_test_name'].value;

            $.ajax({
                url: '<?php echo base_url("customer/ajaxload/checke_reciver_id");?>',
                type: 'POST', //the way you want to send data to your URL
                data: {'receiver_id': receiver_id,'csrf_test_name':csrf_test_name },
                success: function(data) { 
                    
                    if(data!=0){
                        $('#receiver_id').css("border","1px green solid");
                        $('#receiver_alert').css("color","green");
                        $('button[type=submit]').prop('disabled', false);
                    } else {
                        $('button[type=submit]').prop('disabled', true);
                        $('#receiver_id').css("border","1px red solid");
                        $('#receiver_alert').css("color","red");
                    }  
                },
            });
        }
    </script>

    <style type="text/css">
        .red{
            border:1px red solid;
        }
    </style>

<?php } ?>

    <script type="text/javascript">
        function transfer(id){

    var code = document.forms['verify'].elements['code'].value;
    var csrf_test_name = document.forms['verify'].elements['csrf_test_name'].value;

            swal({
                title: 'Please Wait......',
                type: 'warning',
                showConfirmButton: false,
                onOpen: function () {
                    swal.showLoading()
                  }
            });

            $.ajax({
                url: '<?php echo base_url("home/transfer_verify"); ?>',
                type: 'POST', //the way you want to send data to your URL
                data: {'id': id,'code':code,'csrf_test_name':csrf_test_name },
                success: function(data) { 

                    if(data!=''){

                        swal({
                            title: "Good job!",
                            text: "Your Custom Email Send Successfully",
                            type: "success",
                            showConfirmButton: false,
                            timer: 1500,

                        });
                        window.location.href = "<?php echo base_url('transfer-details/'); ?>"+data;
                        
                    } else {

                        swal({
                            title: "Wops!",
                            text: "Error Message",
                            type: "error",
                            showConfirmButton: false,
                            timer: 1500
                        });

                    }
                    
                }
            });
        }
    </script>

    <!-- Ajax Language Change -->
    <script type="text/javascript">
        $(function(){
            $("#lang-change").on("change", function(event) {
                event.preventDefault();

                var lang = $("#lang-change").val();

                var token   = "<?php echo $this->security->get_csrf_hash(); ?>";
                var inputdata = "lang="+lang+"&<?php echo $this->security->get_csrf_token_name(); ?>="+token;
                $.ajax({
                    url: "<?php echo base_url('home/langChange'); ?>",
                    type: "post",
                    data: inputdata,
                    success: function(result,status,xhr) {
                        location.reload();
                    },
                    error: function(xhr,status,error){
                        location.reload();
                    }
                });
            });

        });
    </script>

    <script type="text/javascript">
        function copyFunction() {
          var copyText = document.getElementById("copyed");
          copyText.select();
          document.execCommand("Copy");
        }
    </script>

    <?php echo (!empty($theme_settings)?$theme_settings:null); ?>

    <?php if($this->uri->segment(1) == 'arena'){?>
    <!-- Script for custom script-->
    <script src="<?php echo base_url('assets/arenagame/js/custom-script.js')?>"></script>
    <script src="<?php echo base_url('assets/arenagame/js/owl.carousel.min.js')?>" ></script>
    <?php }?>
    
    </body>
</html>
