
        <div class="deposit-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 offset-lg-4">
                        <!-- alert message -->
                        <?php if ($this->session->flashdata('message') != null) {  ?>
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $this->session->flashdata('message'); ?>
                        </div> 
                        <?php } ?>
                            
                        <?php if ($this->session->flashdata('exception') != null) {  ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $this->session->flashdata('exception'); ?>
                        </div>
                        <?php } ?>
                            
                        <?php if (validation_errors()) {  ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo validation_errors(); ?>
                        </div>
                        <?php } ?> 
                        <!-- /.end of alert message -->
                        <div class="deposit-info mb-0">
                         <h3 class="mb-3"><?php echo display('deposit');?></h3>
                            <?php echo form_open_multipart('deposit', array('name'=>'deposit_form', 'id'=>'deposit_form'));?>
                            <div class="form-group">
                                <label for="deposit_type" class=""><?php echo display('deposit_crypto_dollar') ?></label>
                                <select class="form-control basic-single" name="deposit_type" required id="deposit_type">
                                    <option><?php echo display('select_option');?></option>
                                    <option value="coin"><?php echo display('cryptocurrency') ?></option>
                                    <option value="usdollar"><?php echo display('us_dollar') ?></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="crypto_coin" class="">Coin</label>
                                <select class="form-control basic-single" name="crypto_coin" id="crypto_coin" onchange="Fee()" required>
                                    <option><?php echo display('select_option');?></option>
                                    <?php //foreach ($coin_list as $key => $value) {  ?>
                                    <!-- <option value="<?php //echo $value->symbol; ?>" <?php //echo ($value->symbol==$this->uri->segment(2))?'selected':'' ?> ><?php //echo $value->full_name; ?></option> -->
                                    <?php //} ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="p_name" class=""><?php echo display('deposit_amount');?></label>
                                <input class="form-control" name="deposit_amount" type="text" id="p_name" onkeyup="Fee()" autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <label for="p_name" class=""><?php echo display('deposit_method');?></label>
                                <select class="form-control basic-single" name="method" required onchange="Fee()" id="payment_method" disabled>
                                    <option><?php echo display('deposit_method');?></option>
                                    <?php //foreach ($payment_gateway as $key => $value) {  ?>
                                    <!-- <option value="<?php //echo $value->identity; ?>"><?php //echo $value->agent; ?></option> -->
                                    <?php //} ?>
                                </select>
                                <span id="fee" class="form-text text-success"></span>
                            </div>
                            <span class="payment_info">
                                <div class="form-group">
                                    <label for="comment" class=""><?php echo display('comments');?></label>
                                    <textarea class="form-control" name="comment" id="comment" rows="3"></textarea>
                                </div>
                            </span>
                            <button type="submit" class="btn btn-kingfisher-daisy w-md m-b-5"><?php echo display('deposit');?></button>
                            <a href="<?php echo base_url();?>" class="btn btn-danger w-md m-b-5"><?php echo display('cancel')?></a>
                        </div>
                           
                            <input type="hidden" name="level" value="deposit">
                            <input type="hidden" name="fees" value="">
                            <?php echo form_close();?>  
                        </div>
                    </div>
                </div>
            </div>
        </div>

