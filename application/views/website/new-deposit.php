<style type="text/css">
        .table td, .table th {
            vertical-align: unset;
        }
</style>
<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/global-style.css') ?>" rel="stylesheet" type="text/css">


<div class="main-container">
    <div class="header-flex">
        <div class="header-title">
            <h4><?php echo display('deposit') ?></h4>
        </div>
        <div class="ml_banner_img" style="width: 728px; height: 90px;">
            <?php echo adshow(array(
                'width'         => 728,
                'height'        => 90,
                'client'        => 'ca-pub-2238365415915736',
                'slot'          => '6978414426',
                'format'        => 'auto',
                'responsive'    => 'auto',
            ));?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            
                <?php if($this->session->flashdata('error_message')){?>
                    <div class="alert alert-danger">
                        <strong>Failed!</strong><ul><?php echo $this->session->flashdata('error_message');?></ul>
                    </div>
                <?php }?>

                <?php if($this->session->flashdata('success_message')){?>
                    <div class="alert alert-success">
                        <strong>Success!</strong><ul><?php echo $this->session->flashdata('success_message');?></ul>
                    </div>
                <?php }?>
            <div class="dpt-row">
                <div class="dpt-col-1">
                    <div class="bg-white dpt-box">
                        <div class="btcSlct">
                            <div class="btcIcon">
                                <img id="coinImage" width="22px" height="22px" src="<?php echo base_url().$coins[$selectedCoin]['coin_dp']; ?>" alt="" />
                                <strong id="coinTicker"><?php echo $selectedCoin; ?></strong>
                            </div>
                            <select class="form-control" onchange="updateCoinData(this.value)">
                                <?php foreach($coins as $coin){ ?>
                                    <option value="<?php echo $coin['ticker'] ?>" <?php if($coin['ticker'] == $selectedCoin){ echo 'selected'; } ?> ><?php echo $coin['coin_name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="bb-lst">
                            <div class="bb-cl-1">
                                <div class="bbi-lst">
                                    <div class="bbi-cl-1 txtCmn">Total balance</div>
                                    <div class="bbi-cl-1 txtCmn" id="totalBalance"><?php echo number_format($coins[$selectedCoin]['balance'] + $coins[$selectedCoin]['inOrders'], 8, '.', '').' '. $selectedCoin; ?></div>
                                </div>
                                <div class="bbi-lst">
                                    <div class="bbi-cl-1 txtCmn">In order</div>
                                    <div class="bbi-cl-1 txtCmn" id="inOrdersBalance"><?php echo number_format($coins[$selectedCoin]['inOrders'], 8, '.', '').' '. $selectedCoin; ?></div>
                                </div>
                                <div class="bbi-lst">
                                    <div class="bbi-cl-1 txtCmn">Available balance</div>
                                    <div class="bbi-cl-1 txtCmn" id="availableBalance"><?php echo number_format($coins[$selectedCoin]['balance'], 8, '.', '').' '. $selectedCoin; ?></div>
                                </div>
                            </div>
                            <div class="bb-cl-2 txtCmn">
                                <i class="fa fa-info-circle" aria-hidden="true"></i> What's <span class="tickerForChange"><?php echo $selectedCoin; ?><span>?
                            </div>
                        </div>
                        <div class="imp-msgBx">
                            <p>Important</p>
                            <ul>
                                <li>Send only <span class="tickerForChange"><?php echo $selectedCoin; ?></span> to this deposit address. Sending any another coin or token to this address may result in the loss of your deposit.</li>
                            </ul>
                            <div class="frm-grup clearfix">
                                <label><span class="tickerForChange"><?php echo $selectedCoin; ?></span> Deposit Address</label>
                                <input type="text" class="form-control" id="coinAddress" value="">
                                <div class="text-right">
                                <button type="button" onclick="ShowQrSwal()" class="btn btn-wqcd"><i class="fa fa-qrcode" aria-hidden="true"></i> Show QR code</button>
                                <div class="tooltip2">
                                <button type="button" onclick="copyText()" onmouseout="outFunc()" class="btn btn-wqcd"><i class="fa fa-clipboard" aria-hidden="true"></i> Copy address</button>
                                <span class="tooltiptext" id="myTooltip">Copy to clipboard</span>
                                </div>
                                
                                </div>
                            </div>
                        </div>
                        <div class="pNotebg">
                            <p>Please note</p>
                            <ul>
                                <li>Coins will be deposited immediately after <span>2</span> network confirmations</li>
                                <li>After making a deposit, you can track its progress on the <span>history</span> page.</li>
                            </ul>
                        </div>
                        <?php /*?><div class="goTrade">
                            <p>Go to Trade</p>
                            <p>
                                <a href="#">BTC/PAX <i class="fa fa-angle-right"></i></a>
                                <a href="#">BTC/USDC <i class="fa fa-angle-right"></i></a>
                                <a href="#">BTC/USDS <i class="fa fa-angle-right"></i></a>
                                <a href="#">BTC/USDT <i class="fa fa-angle-right"></i></a>
                            </p>
                        </div>
                        <?php */?>
                        
                    </div>
                </div>
                <div class="dpt-col-2">
                    <div class="hstTable">
                        <table cellpadding="0"; cellspacing="0"; border="0" id="accordion" >
                            <tr>
                                <th colspan="4">History</th>
                                <th>View All</th>
                            </tr>
                            <tbody id="depositLists">
                            <?php if($coins[$selectedCoin]['deposits']){ ?>
                            
                                <?php foreach($coins[$selectedCoin]['deposits'] as $deposit){ ?>
                                    <tr>
                                        <td>
                                            <?php if($deposit->confirmed == 1){
                                                ?>
                                                Completed
                                                <?php
                                            } else {
                                                ?>
                                                Incomplete
                                                <?php
                                            } ?>
                                        </td>
                                        <td><?php echo $selectedCoin; ?></td>
                                        <td><?php echo $deposit->amount; ?></td>
                                        <td><span><?php echo $deposit->created_at; ?></span></td>
                                        <td><a class="accordion-toggle fa" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $deposit->id; ?>"><i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></td>
                                    </tr>
                                    <tr id="collapse_<?php echo $deposit->id; ?>" class="panel-collapse collapse">
                                        <td colspan="4">
                                            <p>Address: <?php echo $deposit->address;?></p>
                                            <p>Txid: <?php echo ($deposit->txid == NULL)? "":$deposit->txid;?></p>
                                        </td>
										<td>&nbsp;</td>
                                    </tr>
                                <?php } ?>
                                
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="5">No Deposits to show</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="stick-here"></div>
                <div class="dpt-col-3" id="stickThis" style="width: 120px; height: 600px;">
                    <?php echo adshow(array(
                        'width'         => 120,
                        'height'        => 600,
                        'client'        => 'ca-pub-2238365415915736',
                        'slot'          => '6978414426',
                        'format'        => 'auto',
                        'responsive'    => 'auto',
                    ));?>
                </div>
            </div>
                <!-- End of Trade History -->
        </div>
    </div>
</div>
<style>
    .tooltip2 {
  position: relative;
  display: inline-block;
}

/* Tooltip text */
.tooltip2 .tooltiptext {
  visibility: hidden;
  width: 380px;
  background-color: #555;
  color: #fff;
  text-align: center;
  padding: 7px 10px;
  border-radius: 4px;
  font-size:14px;
 
  /* Position the tooltip text - see examples below! */
  position: absolute;
  z-index: 1;
  top: 8px;
  left: 105%;
}

/* Show the tooltip text when you mouse over the tooltip container */
.tooltip2:hover .tooltiptext {
  visibility: visible;
}
    </style>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo base_url('assets/js/jquery-1.11.0.js') ?>"></script>
<!-- Script for detects HTML5 and CSS3 features in the userâ€™s browser -->
<script src="<?php echo base_url('assets/js/modernizr.js') ?>"></script>
<!-- Include all compiled plugins, or include individual files as needed -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- Script for custom script-->
<script src="<?php echo base_url('assets/js/custom-script.js') ?>"></script>

<script>
    $(".accordion-toggle").click(function() {
        $(this).parent().parent().siblings().removeClass("in");  
        // $(this).parent().parent().next(".panel-collapse").toggle();
    });
	
    function ShowQrSwal(){ 
	    var qraddr = document.getElementById("coinAddress").value;
        swal({
            title: "Address",
            text: "<img src='http://api.qrserver.com/v1/create-qr-code/?color=000000&amp;bgcolor=FFFFFF&amp;data=" + qraddr + "&amp;qzone=1&amp;margin=0&amp;size=200x200&amp;ecc=L' alt='qr code' />",
            html: true,
        });
    }
</script>
<script>
    var coins = '<?php echo json_encode($coins); ?>';
    coins = JSON.parse(coins)
    function updateCoinData(ticker){
        if(coins[ticker]){
            $("#coinTicker").html(coins[ticker].ticker);
            $("#coinImage").attr('src', '<?php echo base_url() ?>'+coins[ticker].coin_dp);
            $("#coinAddress").val('');
            var totalBalance = ((parseFloat(coins[ticker].balance)) ? parseFloat(coins[ticker].balance) : 0) + ((parseFloat(coins[ticker].inOrders)) ? parseFloat(coins[ticker].inOrders) : 0);
            if(totalBalance){
                $("#totalBalance").html(totalBalance.toFixed(8) + ' ' + ticker);
            } else {
                $("#totalBalance").html('0.00000000 ' + ticker);
            }
            if(coins[ticker].inOrders){
                $("#inOrdersBalance").html(parseFloat(coins[ticker].inOrders).toFixed(8) + ' ' + ticker);
            } else {
                $("#inOrdersBalance").html('0.00000000 ' + ticker);
            }
            if(coins[ticker].balance){
                $("#availableBalance").html(parseFloat(coins[ticker].balance).toFixed(8) + ' ' + ticker);
            } else {
                $("#availableBalance").html('0.00000000 ' + ticker);
            }
            $(".tickerForChange").html(ticker);
            
            var html = '';
            // depositLists
            if(coins[ticker].deposits){
                for(let i = 0; i < coins[ticker].deposits.length; i++){
                    html += `<tr>
                                    <td>Completed</td>
                                    <td>${ticker}</td>
                                    <td>${coins[ticker].deposits[i].amount}</td>
                                    <td><span>${coins[ticker].deposits[i].created_at}</span></td>
                                    <td><a class="accordion-toggle fa" data-toggle="collapse" data-parent="#accordion" href="#collapse_${coins[ticker].deposits[i].id}"><i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></td>
                                </tr>
                                <tr id="collapse_${coins[ticker].deposits[i].id}" class="panel-collapse collapse">
                                    <td>ss</td>
                                    <td>ss</td>
                                    <td>ss</td>
                                    <td>ss</td>
                                    <td>&nbsp;</td>
                                </tr>`;
                }
            } else {
                html    = `<tr>
                            <td colspan="5">No Deposits to show</td>
                        </tr>`;
            }
            $("#depositLists").html(html);
            updateAddress(ticker);
        }
    }
</script>
<script>
    function copyText(){
        var copyText = document.getElementById("coinAddress");
        copyText.select();
        document.execCommand("Copy");
        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copied: " + copyText.value;
    }
    function outFunc() {
        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copy to clipboard";
    }
</script>

<script>
    // getting adress from ajax and updating it to the address bar.
    function updateAddress(ticker){
        if(ticker){
            $.ajax({
                url: '<?php echo base_url(); ?>/getAddress',
                type: 'GET',
                data: { symbol: ticker },
                success: function (data){
                    data = JSON.parse(data);
                    if(data.result){
                        $("#coinAddress").val(data.address)
                    } else {

                    }
                }
            });
        }
    }

    window.onload = updateAddress('<?php echo $selectedCoin; ?>');
</script>