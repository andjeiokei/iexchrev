<style type="text/css">
        .table td, .table th {
            vertical-align: unset;
        }
</style>
<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/global-style.css') ?>" rel="stylesheet" type="text/css">


<div class="main-container">
    <div class="header-flex">
        <div class="header-title">
            <h4><?php echo display('transection') ?></h4>
        </div>
        <div class="ml_banner_img" style="width: 728px; height: 90px;">
            <?php echo adshow(array(
                'width'         => 728,
                'height'        => 90,
                'client'        => 'ca-pub-2238365415915736',
                'slot'          => '6978414426',
                'format'        => 'auto',
                'responsive'    => 'auto',
            ));?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
           
            <div class="dpt-row">

                <div class="dpt-col-90">
                    
                    <!-- /.end of alert message -->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="table-bg">
                                    <th><?php echo display('sl_no') ?></th>
                                    <th><?php echo display('transection') ?></th>
                                    <th><?php echo display('amount') ?></th>
                                    <th><?php echo display('fees') ?></th>
                                    <th><?php echo display('crypto_dollar_currency') ?></th>
                                </tr>
                            </thead>
                            <tbody>

                            <?php $i=1; foreach ($balance_log as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $value->transaction_type ?></td>
                                    <td><?php echo $value->transaction_amount ?></td>
                                    <td><?php echo $value->transaction_fees ?></td>
                                    <td><div class="d-flex marks-ico">
                                            <div><img src="<?php echo base_url($value->image) ?>" alt="<?php echo $value->currency_symbol ?>"></div>
                                            <div class="ico-name">
                                                <font><?php echo $value->full_name ?></font>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php $i++; } ?>

                        </table>
                    </div>
                </div>
                <div class="dpt-col-3 sticky" style="width: 120px; height: 600px;">
                    <?php echo adshow(array(
                        'width'         => 120,
                        'height'        => 600,
                        'client'        => 'ca-pub-2238365415915736',
                        'slot'          => '6978414426',
                        'format'        => 'auto',
                        'responsive'    => 'auto',
                    ));?>
                </div>

            </div>
        <!-- End of Trade History -->
        </div>
    </div>
</div>