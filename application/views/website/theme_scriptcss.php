<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style type="text/css">
<?php
    $menu_bg_color      ='#03a9f4';
    $menu_font_color    ='#ffffff';
	$footer_bg_color 	='#0099de';
	$footer_font_color 	='#ffffff';
	$btn_bg_color 		='#03a9f4';
	$btn_font_color 	='#ffffff';
	$theme_color 		='#03a9f4';
	$newslatter_font 	='#ffffff';
	$newslatter_bg 		='#FAF7FF';
	$newslatter_img 	=base_url('assets/website/img/newslatter-bg.jpg');

	if ($theme) {
		$theme_data = json_decode($theme->settings, true);
        $menu_bg_color      = $theme_data['menu_bg_color']!=""?$theme_data['menu_bg_color']:$menu_bg_color;
        $menu_font_color    = $theme_data['menu_font_color']!=""?$theme_data['menu_font_color']:$menu_font_color;
		$footer_bg_color 	= $theme_data['footer_bg_color']!=""?$theme_data['footer_bg_color']:$footer_bg_color;
		$footer_font_color 	= $theme_data['footer_font_color']!=""?$theme_data['footer_font_color']:$footer_font_color;
		$btn_bg_color 		= $theme_data['btn_bg_color']!=""?$theme_data['btn_bg_color']:$btn_bg_color;
		$btn_font_color 	= $theme_data['btn_font_color']!=""?$theme_data['btn_font_color']:$btn_font_color;
		$theme_color 		= $theme_data['theme_color']!=""?$theme_data['theme_color']:$theme_color;
		$newslatter_font 	= $theme_data['newslatter_font']!=""?$theme_data['newslatter_font']:$newslatter_font;
		$newslatter_bg 		= $theme_data['newslatter_bg']!=""?$theme_data['newslatter_bg']:$newslatter_bg;
		$newslatter_img 	= base_url($theme_data['newslatter_img']);
	}
?>

	/* footer font color*/
    .bg-kingfisher-daisy {
        background-color: <?php echo $menu_bg_color; ?>;
    }
    .navbar-dark .navbar-nav .nav-link {
        color: <?php echo $menu_font_color; ?>!important;
    }
	.page-scroll {
	    color: <?php echo $footer_font_color;  ?>;	
	}
	.footer .social-link li a i {
		color: <?php echo $footer_font_color;  ?>;
	}
	.footer p {
    	color: <?php echo $footer_font_color;  ?>;
	}
	.link-widgets .link-title {
	    color: <?php echo $footer_font_color;  ?>;
	}
	.link-widgets ul li a {
	    color: <?php echo $footer_font_color;  ?>;
	}

	/* footer background color*/
	.secondary-footer {
        background: rgba(0, 0, 0, .1);
    }
	.page-scroll {
	    background-color: <?php echo $footer_bg_color;  ?>;
	}
	.footer {
		background: <?php echo $footer_bg_color;  ?>;
	}


	/*Button primary color*/	
	.btn-kingfisher-daisy {
	    background-color: <?php echo $btn_bg_color;  ?>;
	    border-color: <?php echo $btn_bg_color;  ?>;
	}
	.btn-kingfisher-daisy:hover{
	    background-color: <?php echo $btn_bg_color;  ?>;
	    border-color: ?php echo $btn_bg_color;  ?>;
	}
	.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
	    background-color: <?php echo $btn_bg_color;  ?>;
	    border: 1px solid <?php echo $btn_bg_color;  ?>;
	}
	.animation-slide.owl-theme .owl-nav .owl-next:hover {
	    -webkit-box-shadow: -100px 0 0 <?php echo $btn_bg_color;  ?> inset;
	}
	
	/*btn font color*/
	.btn-kingfisher-daisy {
	    color: <?php echo $btn_font_color;  ?>;
	}
	.btn-kingfisher-daisy:hover{
	    color: <?php echo $btn_font_color;  ?>;
	}
	.nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
	    color: <?php echo $btn_font_color;  ?>;
	}





	/*Theme color*/
	.section_title h3 span {
	    color: <?php echo $theme_color;  ?>;
	}
	.coin-name {
	    border-left: 6px solid <?php echo $theme_color;  ?>;
	}
	.coin-name h5 {
	    color: <?php echo $theme_color;  ?>;
	}
	.content-title {
	    border-left: 6px solid <?php echo $theme_color;  ?>;
	}
	.post-date {
	    color: <?php echo $theme_color;  ?>;
	}	
    .notices .card-header{
        background-color:  <?php echo $theme_color;  ?>;
        color: <?php echo $btn_font_color;  ?>;
    }
    .widget_title::before {

        background-color: <?php echo $theme_color;  ?>;
    }
    .widget_category li span {
        color: <?php echo $theme_color;  ?>;
    }
    .breadcrumb-item + .breadcrumb-item::before{
        border-left-color: <?php echo $theme_color;  ?>;
    }
    .amChartsButton, .amChartsButtonSelected {
        background-color: <?php echo $theme_color;  ?>!important;
    }

    a {
        color: <?php echo $theme_color;  ?>;
    }
    a:hover {
        color: <?php echo $theme_color;  ?>;
    }





	.newslatter {
	    color: <?php echo $newslatter_font;  ?>;
	    background-color: <?php echo $newslatter_bg;  ?>;
	    background-attachment: fixed;
	    background-image: url(<?php echo $newslatter_img;  ?>);
	}



</style>
<script type="text/javascript">
	$(document).ready(function () {

    //SlimScroll
    $('.markert-table').slimScroll({
        height: '353px',
        color: '<?php echo $theme_color;  ?>',
        allowPageScroll: true,
        size: '8px',
        distance: '0px'

    });
    $('.history-table').slimScroll({
        height: '361px',
        color: '<?php echo $theme_color;  ?>',
        allowPageScroll: true,
        size: '8px',
        distance: '0px'

    });
    $('.buyOrder, .sellOrder, .sellRequest').slimScroll({
        height: '266px',
        color: '<?php echo $theme_color;  ?>',
        allowPageScroll: true,
        size: '8px',
        distance: '0px'
    });
    $('.notice').slimScroll({
        height: '358px',
        color: '<?php echo $theme_color;  ?>',
        allowPageScroll: true,
        size: '8px',
        distance: '0px'
    });
    $('#live_chat_list').slimScroll({
        height: '300px',
        color: '<?php echo $theme_color;  ?>',
        allowPageScroll: true,
        size: '8px',
        distance: '0px'
    });

 
    var $particles_js = $('#banner_bg_effect');
    if ($particles_js.length > 0) {
        particlesJS('banner_bg_effect',
        // Update your personal code.
        {
            "particles": {
                "number": {
                    "value": 120,
                    "density": {
                        "enable": true,
                        "value_area": 800
                    }
                },
                "color": {
                    "value": "<?php echo $theme_color;  ?>"
                },
                "shape": {
                    "type": "polygon",
                    "stroke": {
                        "width": 0,
                        "color": "#000000"
                    },
                    "polygon": {
                        "nb_sides": 5
                    },
                    "image": {
                        "src": "img/github.svg",
                        "width": 100,
                        "height": 100
                    }
                },
                "opacity": {
                    "value": 0.4,
                    "random": false,
                    "anim": {
                        "enable": false,
                        "speed": 1,
                        "opacity_min": 0.1,
                        "sync": false
                    }
                },
                "size": {
                    "value": 3,
                    "random": true,
                    "anim": {
                        "enable": false,
                        "speed": 40,
                        "size_min": 0.1,
                        "sync": false
                    }
                },
                "line_linked": {
                    "enable": true,
                    "distance": 150,
                    "color": "<?php echo $theme_color;  ?>",
                    "opacity": 0.1,
                    "width": 1
                },
                "move": {
                    "enable": true,
                    "speed": 6,
                    "direction": "none",
                    "random": false,
                    "straight": false,
                    "out_mode": "out",
                    "bounce": false,
                    "attract": {
                        "enable": false,
                        "rotateX": 600,
                        "rotateY": 1200
                    }
                }
            },
            "interactivity": {
                "detect_on": "canvas",
                "events": {
                    "onhover": {
                        "enable": true,
                        "mode": "repulse"
                    },
                    "onclick": {
                        "enable": true,
                        "mode": "push"
                    },
                    "resize": true
                },
                "modes": {
                    "grab": {
                        "distance": 400,
                        "line_linked": {
                            "opacity": 1
                        }
                    },
                    "bubble": {
                        "distance": 400,
                        "size": 40,
                        "duration": 2,
                        "opacity": 8,
                        "speed": 3
                    },
                    "repulse": {
                        "distance": 200,
                        "duration": 0.4
                    },
                    "push": {
                        "particles_nb": 4
                    },
                    "remove": {
                        "particles_nb": 2
                    }
                }
            },
            "retina_detect": true
        });
    }



});
</script>