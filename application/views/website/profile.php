        <div class="profile-header">
            <div id="author-header">
                <img src="<?php echo base_url("assets/website/img/author-header.jpg") ?>" alt="">
            </div>
            <div class="container text-center">
                <div class="author-avatar">
                    <img src="<?php echo $user_info->image==''?base_url("assets/website/img/img-user.png"):$user_info->image ?>" alt="<?php echo $user_info->first_name; ?>">
                </div>
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <h2 class="author-name"><?php echo $user_info->first_name; ?> <?php echo $user_info->last_name; ?></h2>
                        <p><?php echo $user_info->bio; ?></p>

                          <div class="form-group row">
                            <div class="col-sm-12">
                            
                                <div class="input-group mb-3">
                                <label for="inputPassword" class="col-form-label labelcpy"><?php echo display('affiliate_url ');?>:</label>
                                    <input type="text" class="form-control inpyB" id="copyed" aria-label="Recipient's username" aria-describedby="button-addon2" value="<?php echo base_url()?>register?ref=<?php echo $this->session->userdata('user_id')?>">
                                    <div class="input-group-append">
                                        <button class=" btn btn-copy-B" type="button" onclick="copyFunction()"><?php //echo display('copy');?><i class="fa fa-copy" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.End of profile header -->
        <div class="profile-info">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="bio-info">
                            <dl class="dl-horizontal">
                                <dt><?php echo display('google_authentication') ?>:</dt>
                                <dd><a class="btn btn-kingfisher-daisy" href="<?php echo base_url("googleauth") ?>" class="btn btn-kingfisher-daisy"><?php echo ($user_info->googleauth=='')?'Disabled':'Enabled' ?></a></dd>
                                <dt><?php echo display('user_id') ?> :</dt>
                                <dd><?php echo $user_info->user_id; ?></dd>
                                <dt><?php echo display('firstname') ?> :</dt>
                                <dd><?php echo $user_info->first_name; ?></dd>
                                <dt><?php echo display('lastname') ?> :</dt>
                                <dd><?php echo $user_info->nick_name; ?></dd>
                                <dt><?php echo display('email') ?> :</dt>
                                <dd><?php echo $user_info->email; ?></dd>
                                <dt><?php echo display('phone') ?> :</dt>
                                <dd><?php echo $user_info->phone; ?></dd>
                                <dt><?php echo display('referral_id') ?> :</dt>
                                <dd><?php echo $user_info->referral_id; ?></dd>
                                <dt><?php echo display('language') ?></dt>
                                <dd><?php echo $user_info->language; ?></dd>
                                <dt><?php echo display('address') ?> :</dt>
                                <dd><?php echo $user_info->address; ?></dd>
                                <?php /*<dt><?php echo display('verify') ?></dt>
                                <dd><?php echo ($user_info->verified==0)?"<a href='".base_url('profile-verify')."'>Verify Account</a>":(($user_info->verified==1)?"<span style='color:#4caf50'>Verified</span>":(($user_info->verified==2)?"<span style='color:#f44336'>Cancel</span>":"<span style='color:#3f51b5'>Processing</span>")); ?></dd> */?>
                                <dt><?php echo display('account_created') ?> :</dt>
                                <dd><?php $date=date_create($user_info->created); echo date_format($date,"jS F Y"); ?></dd>
                                <dt><?php echo display('registered_ip') ?> :</dt>
                                <dd><?php echo $user_info->ip; ?></dd>
                                <dt><a class="btn btn-kingfisher-daisy" href="<?php echo base_url("edit-profile") ?>" class="btn btn-kingfisher-daisy"><?php echo display('edit_profile') ?></a></dt>
                                <dd><a class="btn btn-kingfisher-daisy" href="<?php echo base_url("change-password") ?>" class="btn btn-kingfisher-daisy"><?php echo display('change_password') ?></a></dd>
                            </dl>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-8">
                        <div class="table-responsive profile-table">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col"><?php echo display('access_time') ?></th>
                                        <th scope="col"><?php echo display('logo_type') ?></th>
                                        <th scope="col"><?php echo display('user_agent') ?></th>
                                        <th scope="col"><?php echo display('user_id') ?></th>
                                        <th scope="col"><?php echo display('ip') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($user_log as $key => $value) { ?>
                                    <tr>
                                        <th><?php $date=date_create($value->access_time); echo date_format($date,"jS F Y"); ?></th>
                                        <td><?php echo $value->log_type; ?></td>
                                        <td><?php $user_agent = json_decode($value->user_agent, true); echo " Browser: ".$user_agent['browser']." <br>Platform: ".$user_agent['platform'] ?></td>
                                        <td><?php echo $value->user_id; ?></td>
                                        <td><?php echo $value->ip; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="bio-info">
                        <dl class="dl-horizontal">
                            <dt><?php echo 'Your referrals'?>:</dt>
                            <?php
                                foreach($user_referral as $ur){
                                    $coded_mail = substr($ur->email, 0, 6);
                                    $coded_mail .= '******';
                                    $coded_mail .= substr($ur->email,-6);?>
                            <dd><?php echo $coded_mail; } ?></dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>