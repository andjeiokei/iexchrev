<style type="text/css">
        .table td, .table th {
            vertical-align: unset;
        }
</style>
<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/global-style.css') ?>" rel="stylesheet" type="text/css">


<div class="main-container">
    <div class="header-flex">
        <div class="header-title">
            <h4><?php echo display('withdraw') ?></h4>
        </div>
        <div class="ml_banner_img" style="width: 728px; height: 90px;">
            <?php echo adshow(array(
                'width'         => 728,
                'height'        => 90,
                'client'        => 'ca-pub-2238365415915736',
                'slot'          => '6978414426',
                'format'        => 'auto',
                'responsive'    => 'auto',
            ));?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            
            
            <?php if($this->session->flashdata('error_message')){?>
                <div class="alert alert-danger">
                    <strong>Failed!</strong><ul><?php echo $this->session->flashdata('error_message');?></ul>
                </div>
            <?php }?>

            <?php if($this->session->flashdata('success_message')){?>
                <div class="alert alert-success">
                    <strong>Success!</strong><ul><?php echo $this->session->flashdata('success_message');?></ul>
                </div>
            <?php }?>

            <div class="dpt-row">
        
                <div class="dpt-col-1">
                <?php echo form_open('withdraw/submit_withdraw_data','id="frm_withdr"'); ?>
                    <div class="bg-white dpt-box">
                        <div class="btcSlct">
                            <div class="btcIcon">
                            <img id="coinImage" width="22px" height="22px" src="<?php echo base_url().$coins[$selectedCoin]['coin_dp']; ?>" alt="" />
                            <strong id="coinTicker"><?php echo $selectedCoin; ?></strong>
                            </div>
                            <select class="form-control" onchange="updateCoinData(this.value)" id="currency_name" name="currency_name">
                                <?php foreach($coins as $coin){ ?>
                                    <option value="<?php echo $coin['ticker'] ?>" <?php if($coin['ticker'] == $selectedCoin){ echo 'selected'; } ?>><?php echo $coin['coin_name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="bb-lst">
                            <div class="bb-cl-1">
                                <div class="bbi-lst">
                                    <div class="bbi-cl-1 txtCmn">Total balance</div>
                                    <div class="bbi-cl-1 txtCmn" id="totalBalance"><?php echo number_format($coins[$selectedCoin]['balance'] + $coins[$selectedCoin]['inOrders'], 8, '.', '').' '. $selectedCoin; ?></div>
                                </div>
                                <div class="bbi-lst">
                                    <div class="bbi-cl-1 txtCmn">In order</div>
                                    <div class="bbi-cl-1 txtCmn" id="inOrdersBalance"><?php echo number_format($coins[$selectedCoin]['inOrders'], 8, '.', '').' '. $selectedCoin; ?></div>
                                </div>
                                <div class="bbi-lst">
                                    <div class="bbi-cl-1 txtCmn">Available balance</div>
                                    <div class="bbi-cl-1 txtCmn" id="availableBalance"><?php echo number_format($coins[$selectedCoin]['balance'], 8, '.', '').' '. $selectedCoin; ?></div>
                                </div>
                            </div>
                            <div class="bb-cl-2 txtCmn">
                                <i class="fa fa-info-circle" aria-hidden="true"></i> What's <span class="tickerForChange"><?php echo $selectedCoin; ?></span>?
                            </div>
                        </div>
                        <div class="imp-msgBx">
                            <p>Important</p>
                            <ul>
                            <li>Minimum withdrawal <span id="minimum_withdraw_val">0.002</span> <span class="tickerForChange"><?php echo $selectedCoin; ?></span></li>
                            <li>Do not withdraw directly to a crowdfund or ICO address, as your account will not be credited with tokens for such sales.</li>
                            </ul>
                            <div class="frm-grup clearfix">

                                
                                
                                <div class="form-group">
                                    <span class="font-weight-bold text-dark">
                                        <strong><span class="tickerForChange"><?php echo $selectedCoin; ?></span> Withdrawal Address</strong>
                                    </span>
                                    <input class="form-control input-sm head_mrg" type="text" name="address" id="address" autocomplete="off" >
                                    <span class="text-danger" id="error_address"></span>
                                </div>



                                <div class="form-group mb-3">
                                    <span class="font-weight-bold text-dark">
                                        24h Withdrawal Limit: 0 / 2 <span class="tickerForChange"><?php echo $selectedCoin; ?></span>
                                    </span>
                                    <div class="input-group head_mrg">
                                        <input type="text" class="form-control txt_amount" placeholder="Available <?php echo $coins[$selectedCoin]['balance'].' '. $selectedCoin; ?>" aria-describedby="basic-addon2" name="amount" id="amount" step="0.00000001" class="form-control" min="0.00200000" max="<?php echo $coins[$selectedCoin]['balance'].' '. $selectedCoin; ?>" autocomplete="off">
                                        
                                        <div class="input-group-append">
                                            <span class="input-group-text tickerForChange" id="basic-addon2"><?php echo $selectedCoin; ?></span>
                                        </div>
                                        
                                    </div>
                                    <span class="text-danger" id="error_amount"></span>
                                </div>


                                <div class="form-group">
                                    <span class="font-weight-bold text-dark">
                                        Transaction Fee: <span id="fees">0.00000000</span>
                                    </span>

                                    <span class="font-weight-bold text-dark span_trans_right
                                    " >
                                        You will get: <span id="get_amount">0.00000000</span>
                                    </span>

                                    <div class="input-group head_mrg">
                                        <button type="button" class="btn btn-primary btn-block btn btn-kingfisher-daisy withdraw_btn" id="withdraw_btn">Submit</button>
                                    </div>

                                </div>
                                    
                                
                            </div>
                        </div>
                        <div class="pNotebg">
                            <p>Please note</p>
                            <ul>
                                <li>Once you nave submitted your withdrawal request, we send a confirmation email Please then click on the confirmation link in your email.</li>
                                <li>After making a withdrawal, you can track its progress on the <a>history</a> page.</li>
                            </ul>
                        </div>
                        <?php /*?>
                        
                        <div class="goTrade">
                            <p>Go to Trade</p>
                            <p>
                                <a href="#"><span class="tickerForChange"><?php echo $selectedCoin; ?></span>/PAX <i class="fa fa-angle-right"></i></a>
                                <a href="#"><span class="tickerForChange"><?php echo $selectedCoin; ?></span>/USDC <i class="fa fa-angle-right"></i></a>
                                <a href="#"><span class="tickerForChange"><?php echo $selectedCoin; ?></span>/USDS <i class="fa fa-angle-right"></i></a>
                                <a href="#"><span class="tickerForChange"><?php echo $selectedCoin; ?></span>/USDT <i class="fa fa-angle-right"></i></a>
                            </p>
                        </div>
                        <?php */?>

                    </div>
                    <input type="hidden" name="amount_deduct_fee" id="amount_deduct_fee">
                    <input type="hidden" name="amount_balance" id="amount_balance">
                    <?php echo form_close();?>
                </div>
                <div class="dpt-col-2">
                    <div class="hstTable">
                        <table cellpadding="0"; cellspacing="0"; border="0" id="accordion">
                            <tr>
                                <th colspan="4">History</th>
                                <th>View All</th>
                            </tr>
                            <tbody id="depositLists">
                            <?php 
                            if(!empty($coins[$selectedCoin]['withdraw'])){
                            foreach($coins[$selectedCoin]['withdraw'] as $withdraw){ ?>
                            <tr>
                                <td>
                                <?php 
                                if($withdraw['confirmed'] == 1)
                                {
                                    echo "Completed";
                                }
                                else if($withdraw['confirmed'] == 0)
                                {
                                    switch ($withdraw['status']) {
                                        case 0:
                                            echo "Canceled";
                                            break;
    
                                        case 1:
                                            echo 'Approved <i class="fa fa-info-circle" aria-hidden="true" title="Approved by admin and wating for server approval."></i>';
                                            break;
                                        
                                        case 2:
                                            echo "Pending";
                                            break;
    
                                        case 3:
                                            echo 'Approval Pending <i class="fa fa-info-circle" aria-hidden="true" title="Please confirm your request from your mail!"></i>';
                                            break;
                                        
                                        default:
                                            echo "Unknown";
                                            break;
                                    } 
                                }
                                ?>
                                </td>
                                <td><?php echo $withdraw['currency_symbol'];?></td>
                                <td><?php echo number_format( $withdraw['amount'] , 8);?></td>
                                <td>
                                <span><?php echo date("d-m-Y", strtotime($withdraw['request_date']));?></span>
                                <br>
                                <span><?php echo date("h:i:s A", strtotime($withdraw['request_date']));?></span></td>
                                <td><a class="accordion-toggle fa" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></td>
                            </tr>
                            <tr id="collapseOne" class="panel-collapse collapse">
                                <td colspan="4">
                                    <p>Address: <?php echo $withdraw['address'];?></p>
                                    <p>Txid: <?php echo ($withdraw['txid'] == NUL)? "":$withdraw['txid'];?></p>
                                </td>
                                
                                <td>&nbsp;</td>
                            </tr>
                            <?php }
                            }else{?>
                            <tr>
                                <td colspan="5">There is not withdraw has been made yet!</td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="dpt-col-3 sticky" style="width: 120px; height: 600px;">
                    <?php echo adshow(array(
                        'width'         => 120,
                        'height'        => 600,
                        'client'        => 'ca-pub-2238365415915736',
                        'slot'          => '6978414426',
                        'format'        => 'auto',
                        'responsive'    => 'auto',
                    ));?>
                </div>

            </div>

            <!-- End of Trade History -->
        </div>
    </div>
</div>





<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo base_url('assets/js/jquery-1.11.0.js') ?>"></script>
<!-- Script for detects HTML5 and CSS3 features in the userâ€™s browser -->
<script src="<?php echo base_url('assets/js/modernizr.js') ?>"></script>
<!-- Include all compiled plugins, or include individual files as needed -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<!-- Script for custom script-->
<script src="<?php echo base_url('assets/js/custom-script.js') ?>"></script>
<script src="<?php echo base_url('assets/js/btcvalid.js') ?>"></script>
<script>
  $(".accordion-toggle").click(function() {
    $(this).parent().parent().siblings().removeClass("in");  
//      $(this).parent().parent().next(".panel-collapse").toggle();
});  
</script> 
<script>

    var fees = <?php echo json_encode($fees_data);?>;
    var availableBalance = <?php echo ($coins[$selectedCoin]['balance'] > 0)? $coins[$selectedCoin]['balance']:0;?>;
    var minimum_amount = 0;
    

    var coins = '<?php echo json_encode($coins); ?>';
    coins = JSON.parse(coins)
    function updateCoinData(ticker){

        //console.log(coins[ticker])
        if(coins[ticker]){
            $("#coinTicker").html(coins[ticker].ticker);
            $("#coinImage").attr('src', '<?php echo base_url() ?>'+coins[ticker].coin_dp);
            $("#coinAddress").val(coins[ticker].address);
            var totalBalance = ((parseFloat(coins[ticker].balance)) ? parseFloat(coins[ticker].balance) : 0) + ((parseFloat(coins[ticker].inOrders)) ? parseFloat(coins[ticker].inOrders) : 0);
            if(totalBalance){
                $("#totalBalance").html(totalBalance.toFixed(8) + ' ' + ticker);
            } else {
                $("#totalBalance").html('0.00000000 ' + ticker);
            }
            if(coins[ticker].inOrders){
                $("#inOrdersBalance").html(parseFloat(coins[ticker].inOrders).toFixed(8) + ' ' + ticker);
            } else {
                $("#inOrdersBalance").html('0.00000000 ' + ticker);
            }
            if(coins[ticker].balance){
                $("#availableBalance").html(parseFloat(coins[ticker].balance).toFixed(8) + ' ' + ticker);
            } else {
                $("#availableBalance").html('0.00000000 ' + ticker);
            }
            $(".tickerForChange").html(ticker);
            
            var html = '';
            // withdrawLists
            if(coins[ticker].withdraw.length > 0){

                for(let i = 0; i < coins[ticker].withdraw.length; i++){

                    var status = "";
                    if( coins[ticker].withdraw[i].confirmed == 1 )
                    {
                        status ="Completed";
                    }
                    else if(coins[ticker].withdraw[i].status == 0)
                    {
                        status ="Canceled";
                    }
                    else if(coins[ticker].withdraw[i].status == 1)
                    {
                        status ='Approved <i class="fa fa-info-circle" aria-hidden="true" title="Approved by admin and wating for server approval."></i>';
                    }
                    else if(coins[ticker].withdraw[i].status == 2)
                    {
                        status ="Pending";
                    }
                    else if(coins[ticker].withdraw[i].status == 3)
                    {
                        status ='Approval Pending <i class="fa fa-info-circle" aria-hidden="true" title="Please confirm your request from your mail!"></i>';
                    }

                    var date = new Date(coins[ticker].withdraw[i].request_date);
                    var hour = date.getHours();
                    var amPM = (hour > 11) ? "PM" : "AM";

                    if(hour > 12) {
                        hour -= 12;
                    } else if(hour == 0) {
                        hour = "12";
                    } else if ( hour < 10 )
                    {
                        hour = "0"+hour;
                    }

                    date = date.getDate()+'-'+date.getMonth()+'-'+date.getFullYear()+'<br>'+hour+":"+date.getMinutes()+":"+date.getSeconds()+' '+amPM;
                    

                    html += `<tr>
                                    <td>${status}</td>
                                    <td>${ticker}</td>
                                    <td>${coins[ticker].withdraw[i].amount}</td>
                                    <td><span>${date}</span></td>
                                    <td><a class="accordion-toggle fa" data-toggle="collapse" data-parent="#accordion" href="#collapse_${coins[ticker].withdraw[i].id}"><i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></td>
                                </tr>
                                <tr id="collapse_${coins[ticker].withdraw[i].id}" class="panel-collapse collapse">
                                    <td colspan="4" style="text-align: left;">
                                        <p>Address: ${coins[ticker].withdraw[i].address}</p>
                                        <p>Txid: ${ (coins[ticker].withdraw[i].txid == null)? "":coins[ticker].withdraw[i].txid}</p>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>`;
                }
            } else {
                html    = `<tr>
                            <td colspan="5">There is not withdraw has been made yet!</td>
                        </tr>`;
            }
            $("#depositLists").html(html);


            availableBalance = coins[ticker].balance;

            $("#amount").attr('placeholder', "Available "+coins[ticker].balance + ' ' + ticker)

            $("#fees").text('0.00000000');
            $("#get_amount").text($("#amount").val());
            $("#amount_deduct_fee").val(0);

            var amount          = $.trim($("#amount").val());
            var fees_amount     = fees[$("#currency_name").val()]['WITHDRAW']['fees'];
            minimum_amount      = fees[$("#currency_name").val()]['WITHDRAW']['minimum_amount'];
            $("#minimum_withdraw_val").text(minimum_amount);
            get_amount_calculation(amount, fees_amount, ticker, minimum_amount);

        }

    }

$(document).ready(function(){

    $("#fees").text(fees[$("#currency_name").val()]['WITHDRAW']['fees']);
    minimum_amount      = fees[$("#currency_name").val()]['WITHDRAW']['minimum_amount'];
    $("#minimum_withdraw_val").text(minimum_amount);

    $("#withdraw_btn").on('click',function(){
        
        var address         = $.trim($("#address").val()),
            amount          = $.trim($("#amount").val()),
            currency_name   = $.trim($("#currency_name").val()),
            submit          = true;

        $("#error_address").text('');
        $("#error_amount").text('');

        if(!address)
        {
            $("#error_address").text("Please enter the address!");
            submit = false;
        }
        if(!checkAddress(address))
        {
            $("#error_address").text("Please enter a valid address!");
            submit = false;
        }
        //valid = address + " is " + (checkAddress(address) ? 'Valid':'not a valid Address');
        //$("#error_address").text(valid);

        if(!amount || amount <= 0)
        {
            $("#error_amount").text("Please enter amount!");
            submit = false;
        }
        else if( amount < parseFloat( minimum_amount ) )
        {
            $("#error_amount").text("You have to withdraw minimum "+minimum_amount+" "+currency_name);
            submit = false;
        }
        else if( amount > parseFloat( availableBalance ) )
        {
            $("#error_amount").text("You can't withdraw more than amount from your balance!");
            submit = false;
        }

        if(submit === true)
        {
            $("#amount_balance").val(availableBalance);
            $("#frm_withdr").submit();
        }


    });

    $("#amount").on("keyup change paste blur",function(){
        $(this).val( $(this).val().replace(/[^0-9.]/gi,'') );


        var amount          = $.trim($("#amount").val());
        var fees_amount     = fees[$("#currency_name").val()]['WITHDRAW']['fees'];
        var currency_name   = $.trim($("#currency_name").val());
        var minimum_amount      = fees[$("#currency_name").val()]['WITHDRAW']['minimum_amount'];
        get_amount_calculation(amount, fees_amount, currency_name, minimum_amount);

    });

});

function get_amount_calculation(amount, fees_amount, currency_name, minimum_amount)
{
    var amount          = parseFloat( amount );
    var fees_amount     = parseFloat( fees_amount );
    var minimum_amount  = parseFloat( minimum_amount );

    if(fees_amount !== 'undefined' )
    {
        $("#fees").text(fees_amount);

        if(!amount || amount <= 0)
        {
            $("#error_amount").text("Please enter amount!");
        }
        else if( ( minimum_amount > 0) && ( amount < minimum_amount ) )
        {
            $("#error_amount").text("You have to withdraw minimum "+minimum_amount.toFixed(8)+" "+currency_name);
        }
        else if( amount > 0 )
        {
            var get_fees_amount = 0;
            get_fees_amount     = ( amount - fees_amount ) ;
            $("#get_amount").text(get_fees_amount.toFixed(8));
            $("#amount_deduct_fee").val(get_fees_amount.toFixed(8));
            $("#error_amount").text("");
        }
        else{
            $("#get_amount").text('0.00000000');
            $("#amount_deduct_fee").val(0);
            $("#error_amount").text("");
        }

    }


}
</script>