<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h2><?php echo (!empty($title)?$title:null) ?></h2>
                    <div class="col-sm-3 col-md-3 pull-right">
                        <a class="btn btn-success w-md m-b-5 pull-right" href="<?php echo base_url("backend/coinpair/form") ?>"><i class="fa fa-plus" aria-hidden="true"></i> Coin Pair</a>
                    </div>
                </div>
            </div>
            <div class="panel-body">

                <?php if( $this->session->flashdata('success_message')){?>
                    <div class="alert alert-success">
                        <strong>Success! </strong><?php echo $this->session->flashdata('success_message');?>
                    </div>
                <?php }
                else if( $this->session->flashdata('error_message')){?>
                    <div class="alert alert-danger">
                        <strong>Sorry! </strong><?php echo $this->session->flashdata('error_message');?>
                    </div>
                <?php }?>

                <table class="datatable2 table table-bordered table-hover">
                    <thead>
                        <tr> 
                            <th><?php echo display('sl_no') ?></th>
                            <th>Market</th>
                            <th>Coin</th>
                            <th>Name</th>
                            <th>Full Name</th>
                            <th>Symbol</th>
                            <th>Initial Price</th>
                            <th>Market Price</th>
                            <th><?php echo display('status') ?></th>
                            <th><?php echo display('action') ?></th> 
                        </tr>
                    </thead>    
                    <tbody>
                        <?php if (!empty($coinpair)) ?>
                        <?php $sl = 1; ?>
                        <?php foreach ($coinpair as $value) { ?>
                        <tr>
                            <td><?php echo $sl++; ?></td> 
                            <td>
                                <?php foreach ($market as $mvalue) { ?>
                                <?php echo ($value->market_symbol==$mvalue->symbol)?$mvalue->full_name:'' ?>
                                <?php } ?>
                            </td>
                            <td>
                                <?php foreach ($coins as $cvalue) { ?>
                                <?php echo ($value->currency_symbol==$cvalue->symbol)?$cvalue->full_name:'' ?>
                                <?php } ?>
                            </td>
                            <td><?php echo $value->name; ?></td>
                            <td><?php echo $value->full_name; ?></td>
                            <td><?php echo $value->symbol; ?></td>
                            <td><?php echo $value->initial_price; ?></td>
                            <td id="price_<?php echo $value->symbol ?>"><?php echo @$value->market; ?></td>
                            <td><?php echo (($value->status==1)?display('active'):display('inactive')); ?></td>
                            <td>
                                <a href="<?php echo base_url("backend/coinpair/form/$value->id") ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                <a href="javascript:void(0);" class="btn btn-danger btn-sm remove_js" data-toggle="tooltip" data-placement="right" title="Remove" data-url="<?php echo base_url("backend/coinpair/remove/".$value->id."/".$current_page) ?>"><i class="fa fa-times" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <?php } ?>  
                    </tbody>
                </table>
                <?php echo $links; ?>
            </div> 
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="list_remove" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Warning!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure to delete this record?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Discard</button>
        <button type="button" class="btn btn-primary" id="remove_proceed">Yes</button>
        <input type="hidden" id="remove_url">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready( function() {
    $.getJSON("<?php echo base_url('home/market_streamer'); ?>", function(data) {
        $.each(data.marketstreamer, function(index, element){
            
            $('#price_'+element.market_symbol).text(element.last_price);
            var change = element.price_change_1h/element.last_price;
            if (change>0) {
                $('#price_'+element.market_symbol).addClass("text-success");
            }
            else {
                $('#price_'+element.market_symbol).addClass("text-danger");
            };

        });
    });

    $(".remove_js").on('click',function(){

        var url = $(this).attr('data-url');

        if(url)
        {
            $("#remove_url").val(url);
            $("#list_remove").modal('show');
        }
        
    });

    $("#remove_proceed").on("click",function(){

        var url = $("#remove_url").val();
        window.location.href = url;

    });
});
</script>