<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h2><?php echo (!empty($title)?$title:null) ?></h2>
                    <div class="col-sm-3 col-md-3 pull-right">
                        <a class="btn btn-success w-md m-b-5 pull-right" href="" data-toggle="modal" data-target="#editFees" onclick="updateFeesModal()"><i class="fa fa-plus" aria-hidden="true"></i> Fees</a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
            
                <?php if( $this->session->flashdata('success_message')){?>
                    <div class="alert alert-success">
                        <strong>Success! </strong><?php echo $this->session->flashdata('success_message');?>
                    </div>
                <?php }
                else if( $this->session->flashdata('error_message')){?>
                    <div class="alert alert-danger">
                        <strong>Sorry! </strong><?php echo $this->session->flashdata('error_message');?>
                    </div>
                <?php }?>

                <table id="ajaxcointable" class="table table-bordered table-hover">
                    <thead>
                        <tr> 
                            <th>Coin / Token</th>                            
                            <th>Minimum Withdrawal</th>
                            <th>Withdrawal Fee</th>
                            <th>Buy</th>
                            <th>Sell</th>
                            <th><?php echo display('status') ?></th>
                            <th><?php echo display('action') ?></th> 
                        </tr>
                    </thead>    
                    <tbody>

                    </tbody>
                </table>

            </div> 
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="list_remove" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Warning!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure to delete this record?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Discard</button>
        <button type="button" class="btn btn-primary" onclick="deleteRequest()">Yes</button>
        <input type="hidden" id="remove_url">
      </div>
    </div>
  </div>
</div>

<!-- Edit / Add fees Modal -->
<div id="editFees" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Coin Fees Setting</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" action="/action_page.php">
            <div class="form-group">
                <label class="control-label col-sm-2" for="email">Coin:</label>
                <div class="col-sm-10">
                    <select id="coinSymbol" class="form-control">
                        <?php
                            foreach($coins as $coin){
                                ?>
                                <option value="<?php echo $coin->symbol; ?>"><?php echo $coin->full_name; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="minWithdraw">Minimum Withdraw:</label>
                <div class="col-sm-10"> 
                <input type="text" class="form-control" id="minWithdraw" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 46 || event.charCode == 0 " placeholder="Minimum Withdraw">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="withdrawFee">Withdraw Fees:</label>
                <div class="col-sm-10"> 
                <input type="text" class="form-control" id="withdrawFee" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 46 || event.charCode == 0 " placeholder="Withdraw Fees">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="buyFee">Buy Fees:</label>
                <div class="col-sm-10"> 
                <input type="text" class="form-control" id="buyFee" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 46 || event.charCode == 0 " placeholder="Buy Fees">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="sellFee">Sell Fees:</label>
                <div class="col-sm-10"> 
                <input type="text" class="form-control" id="sellFee" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) || event.charCode == 46 || event.charCode == 0 " placeholder="Sell Fees">
                </div>
            </div>
            <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10">
                <button type="button" onclick="submitFees()" class="btn btn-default">Submit</button>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
var table;

$(document).ready(function() {   

    //datatables
    table = $('#ajaxcointable').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [],        //Initial no order.
        "pageLength": 10,   // Set Page Length
        "lengthMenu":[[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        // "paging": false,
        // "searching": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('backend/feessetting/ajax_list')?>",
            "type": "POST",
            "data": {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>"}
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            // "targets": [0,4,7],
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
       "fnInitComplete": function (oSettings, response) {
        //$("#id_show_total").text(response.recordsTotal);
      }

    });
    $.fn.dataTable.ext.errMode = 'none';

});

$(document.body).on('click', ".remove_js",function(){

    var url = $(this).attr('data-url');

    if(url)
    {
        $("#remove_url").val(url);
        $("#list_remove").modal('show');
    }

});

$(document.body).on("click", "#remove_proceed",function(){

    var url = $("#remove_url").val();
    window.location.href = url;

});

function updateFeesModal(minWithdraw, withdrawFees, buyFees, sellFees, coin){
    // set Values;

    coin = coin || null;
    console.log(coin)
    minWithdraw = minWithdraw || 0;
    withdrawFees = withdrawFees || 0;
    buyFees = buyFees || 0;
    sellFees = sellFees || 0;
    $("#minWithdraw").val(minWithdraw);
    $("#withdrawFee").val(withdrawFees);
    $("#buyFee").val(buyFees);
    // $("#coinSymbol").val(coin);
    $('#coinSymbol option[value="'+coin+'"]').attr('selected', 'selected')
    $('#sellFee').val(sellFees);


    if(coin){
        // $('#coinSymbol').attr('readonly', 'readonly');
        $("select.form-control:not(.dont-select-me)").select2({disabled: true});

        // $("#coinSymbol").select2({readonly: true});;
    } else {
        // $(".disabled-select", $("#coinSymbol")).remove();
        $("select.form-control:not(.dont-select-me)").select2({disabled: false});

    }

}

function submitFees(){
    var minWithdraw = $("#minWithdraw").val();
    var withdrawFees = $("#withdrawFee").val();
    var buyFees = $("#buyFee").val();
    var coin = $("#coinSymbol").val();
    console.log(coin);
    var sellFees = $('#sellFee').val();
    var url = '<?php echo base_url("backend/feessetting/saveFees") ?>';

    $.ajax({
        url,
        data: {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>", minWithdraw, withdrawFees, buyFees, coin, sellFees},
        type: "POST",
        success: function (data){
            console.log(data)
            window.location.reload();
        }
    });

}

function updateDeleteModal(coin){
    coin = coin || null;
    if(coin){
        $("#remove_url").val(coin);
    }
}

function deleteRequest(){
    var coin = $("#remove_url").val();
    if(coin){
        var url = '<?php echo base_url("backend/feessetting/deleteFees") ?>';

        $.ajax({
        url,
        data: {
            "<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>", coin
            },
        type: "POST",
        success: function (data){
            console.log(data)
            window.location.reload();
        }
    });
    }
}
</script>

 