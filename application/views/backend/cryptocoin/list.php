<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h2><?php echo (!empty($title)?$title:null) ?></h2>
                    <div class="col-sm-3 col-md-3 pull-right">
                        <a class="btn btn-success w-md m-b-5 pull-right" href="<?php echo base_url("backend/cryptocoin/form") ?>"><i class="fa fa-plus" aria-hidden="true"></i> Coin</a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
            
                <?php if( $this->session->flashdata('success_message')){?>
                    <div class="alert alert-success">
                        <strong>Success! </strong><?php echo $this->session->flashdata('success_message');?>
                    </div>
                <?php }
                else if( $this->session->flashdata('error_message')){?>
                    <div class="alert alert-danger">
                        <strong>Sorry! </strong><?php echo $this->session->flashdata('error_message');?>
                    </div>
                <?php }?>

                <table id="ajaxcointable" class="table table-bordered table-hover">
                    <thead>
                        <tr> 
                            <th><?php echo display('sl_no') ?></th>
                            <th>Coin Icon</th>                            
                            <th>Coin Name</th>
                            <th>Full Name</th>
                            <th>Symbol</th>
                            <th>Home Page/Serial</th>
                            <th>Rank</th>
                            <th><?php echo display('status') ?></th>
                            <th><?php echo display('action') ?></th> 
                        </tr>
                    </thead>    
                    <tbody>

                    </tbody>
                </table>

            </div> 
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="list_remove" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Warning!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure to delete this record?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Discard</button>
        <button type="button" class="btn btn-primary" id="remove_proceed">Yes</button>
        <input type="hidden" id="remove_url">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

var table;

$(document).ready(function() {   

    //datatables
    table = $('#ajaxcointable').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [],        //Initial no order.
        "pageLength": 10,   // Set Page Length
        "lengthMenu":[[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        // "paging": false,
        // "searching": false,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('backend/cryptocoin/ajax_list')?>",
            "type": "POST",
            "data": {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>"}
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            // "targets": [0,4,7],
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
       "fnInitComplete": function (oSettings, response) {
        //$("#id_show_total").text(response.recordsTotal);
      }

    });
    $.fn.dataTable.ext.errMode = 'none';

});

$(document.body).on('click', ".remove_js",function(){

    var url = $(this).attr('data-url');

    if(url)
    {
        $("#remove_url").val(url);
        $("#list_remove").modal('show');
    }

});

$(document.body).on("click", "#remove_proceed",function(){

    var url = $("#remove_url").val();
    window.location.href = url;

});
</script>

 