<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h2><?php echo (!empty($title)?$title:null) ?></h2>
                </div>
            </div>
            <div class="panel-body">
                <table class="datatable2 table table-bordered table-hover">
                    <thead>
                        <tr> 
                            <th><?php echo display('sl_no') ?></th>
                            <th><?php echo display('user_id') ?></th>
                            <th><?php echo display('amount') ?></th>
                            <th>Currency/Coin</th>
                            <th><?php echo display('payment_method') ?></th>
                            <th><?php echo display('fees') ?></th>
                            <th><?php echo display('comments') ?></th>
                            <th><?php echo display('date') ?></th>
                            <th><?php echo display('action')."/".display('status'); ?></th>
                            
                        </tr>
                    </thead> 
                    <tbody>
                        <?php if (!empty($deposit)) ?>
                        <?php $sl = ( $this->uri->segment(5) == "" )? 1:$this->uri->segment(5) + 1; ?>
                        <?php foreach ($deposit as $value) { ?>
                        <tr>
                            <td><?php echo $sl++; ?></td>
                            <td><?php echo $value->user_id; ?></td>
                            <td class="text-right"><?php echo number_format($value->amount, 8); ?></td>
                            <td><?php echo $value->symbol; ?></td>
                            <td><?php echo $value->method_id; ?></td>
                            <td class="text-right"><?php echo (float)$value->fees_amount; ?></td>
                            <td>
                                <?php
                                    if (is_string($value->comment) && is_array(json_decode($value->comment, true)) && ((json_last_error() == JSON_ERROR_NONE) ? true : false) && $value->method_id=='phone') {

                                       $mobiledata = json_decode($value->comment, true);
                                       echo '<b>OM Name:</b> '.$mobiledata['om_name'].'<br>';
                                       echo '<b>OM Phone No:</b> '.$mobiledata['om_mobile'].'<br>';
                                       echo '<b>Transaction No:</b> '.$mobiledata['transaction_no'].'<br>';
                                       echo '<b>ID Card No:</b> '.$mobiledata['idcard_no'];

                                    }elseif (is_string($value->comment) && is_array(json_decode($value->comment, true)) && ((json_last_error() == JSON_ERROR_NONE) ? true : false) && $value->method_id=='bank') {

                                        $decode_bank = json_decode($value->comment, true);

                                        echo "<b>Account Name: </b>".@$decode_bank['acc_name']."<br>";
                                        echo "<b>Account No: </b>".@$decode_bank['acc_no']."<br>";
                                        echo "<b>Branch Name: </b>".@$decode_bank['branch_name']."<br>";
                                        echo "<b>SWIFT Code: </b>".@$decode_bank['swift_code']."<br>";
                                        echo "<b>ABN No: </b>".@$decode_bank['abn_no']."<br>";
                                        echo "<b>Country: </b>".@$decode_bank['country']."<br>";
                                        echo "<b>Bank Name: </b>".@$decode_bank['bank_name']."<br>";                                        
                                        if (isset($decode_bank['document'])) {
                                            echo "<b>Document: </b>";
                                            echo "<img src='".base_url($decode_bank['document'])."' class='img-responsive' /><a href='".base_url($decode_bank['document'])."' class='btn btn-success' download='".@$decode_bank['acc_name']."'>Download File</a>";                                
                                        }

                                    } else {
                                       echo $value->comment;

                                    }
                                ?>
                            </td>
                            <td>
                                <?php
                                    $date=date_create($value->created_at);
                                    echo date_format($date,"jS F Y"); 
                                ?>
                            </td>
                            <?php if ($value->confirmed==1) { ?>
                            <td><a class="btn btn-success btn-sm"><?php echo display('success')?></a></td>
                            <?php } else if($value->confirmed==2){ ?>
                            <td><a class="btn btn-danger btn-sm"><?php echo display('cancel')?></a></td>
                            <?php } else { ?>
                            <td width="150px">
                                <a href="<?php echo base_url()?>backend/deposit/deposit/confirm_deposit?id=<?php echo $value->id;?>&user_id=<?php echo $value->user_pk_id;?>&set_status=1&csym=<?php echo $value->currency_symbol;?>" class="btn btn-success btn-sm"><?php echo display('confirm')?></a>
                                <a href="<?php echo base_url()?>backend/deposit/deposit/cancel_deposit?id=<?php echo $value->id;?>&user_id=<?php echo $value->user_pk_id;?>&set_status=2&csym=<?php echo $value->currency_symbol;?>" class="btn btn-danger btn-sm"><?php echo display('cancel')?></a>

                            </td>
                           <?php } ?>
                        </tr>
                        <?php } ?>  
                    </tbody>
                </table>
                <?php echo $links; ?>
            </div> 
        </div>
    </div>
</div>

 