<style type="text/css">
    .count_panel{
            text-align: unset;
    }
    .count_panel .fa.fa-info-circle {
        position: absolute;
        right: 4px;
        top: 3px;
        opacity: 1;

    }
    .stats-title i {
        position: absolute;
        opacity: .05;
        right: 10px;
        bottom: 10px;
        font-size: 100px;
        color: #000;
    }
    .stats-title h4 {
        font-size: 24px;
        font-weight: normal;
    }
    .panel-one{
        background: #0D063B;
        color: #fff;
    }
    .panel-one i{
        color: #fff;
        opacity: .05;
    }
    .panel-two{
        background: #1B9A1B;
        color: #fff;
    }

    .panel-three{
        background: #C50B3E;
        color: #fff;
    }
    .panel-four{
        background: #86C20C;
        color: #fff;
    }
    .panel-five{
        background: #033641;
        color: #fff;
    }
    .panel-five i{
        color: #fff;
        opacity: .05;
    }
    .panel-six{
        background: #066C81;
        color: #fff;
    }
    .panel-seven{
        background: #EBBE0C;
        color: #fff;
    }
    .panel-eight{
        background: #1A9CB8;
        color: #fff;
    }
    .panel-nine{
        background: #19B16A;
        color: #fff;
    }

</style>


    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="count_panel panel-one">
                <div class="stats-title">
                    <h4><?php echo display('total_user')?></h4>
                    <i class="fa fa-users"></i>
                </div>
                <h1 class="currency_text "><?php echo (@$total_user?@$total_user:'0'); ?></h1>
            </div>
        </div>

        <div class="col-sm-6 col-md-4">
            <div class="count_panel panel-two">
                <div class="stats-title ">
                    <h4>Exchange Market</h4>
                    <i class="fa fa-bar-chart"></i>
                </div>
                <h1 class="currency_text"><?php echo (@$total_market?@$total_market:'0'); ?></h1>
            </div>
        </div>

        <div class="col-sm-6 col-md-4">
            <div class="count_panel panel-three">
                <div class="stats-title ">
                    <h4>Total Trade</h4>
                    <i class="fa fa-exchange"></i>
                </div>
                <h1 class="currency_text"><?php echo (@$total_trade?@$total_trade:'0'); ?></h1>
            </div>
        </div>

        <div class="col-sm-6 col-md-4">
            <div class="count_panel panel-five">
                <div class="stats-title ">
                    <h4>Total Crypto Fees</h4>
                    <i class="fa fa-btc"></i>                        
                </div>
                <h1 class="currency_text" id="fees_value_BTC">0.00 BTC</h1>
                <i class="fa fa-info-circle" data-toggle="tooltip" data-original-title="Summation of all coins fees are converted to BTC.This value will be BTC current price"></i>
            </div>
        </div>

        <div class="col-sm-6 col-md-4">
            <div class="count_panel panel-six">
                <div class="stats-title ">
                    <h4>Total USD Fees</h4>
                    <i class="fa fa-usd"></i>
                </div>
                <h1 class="currency_text" id="fees_value_USD">$ 0.00</h1>
                <i class="fa fa-info-circle" data-toggle="tooltip" data-original-title="Summation of all coins fees are converted to USD.This value will be USD current price"></i>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="count_panel panel-eight">
                <div class="stats-title">
                    <h4>Referral Bonus USD</h4>
                    <i class="fa fa-usd"></i>
                </div>
                <h1 class="currency_text" id="coin_value_USD">$ 0.00</h1>
                <i class="fa fa-info-circle" data-toggle="tooltip" data-original-title="Total paid REFERRAL bonus in USD"></i>
            </div>
        </div>


        <!-- Flot Filled Area Chart -->
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <h4>Market Deposit</h4>
                            </div>
                            <div class="col-sm-12 col-md-6">
                            <?php echo form_open('#','id="depositsymbolform" name="depositsymbolform" '); ?>

                                <select class="form-control basic-single" name="symbol" id="depositsymbol">
                                    <option><?php echo display('select_option') ?></option>
                                    <?php foreach ($coins as $key => $value) { ?>
                                    <option value="<?php echo $value->symbol; ?>"><?php echo $value->symbol; ?></option>
                                    <?php } ?>
                                </select>

                            <?php echo form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <canvas id="lineChart" height="140"></canvas>   
                </div>
            </div>
        </div>

        <!-- Flot Ber Chart -->
        <div class="col-sm-12 col-md-7">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <h4>Fees Collect</h4>
                            </div>
                            <div class="col-sm-12 col-md-6">
                            <?php echo form_open('#','id="feessymbolform" name="feessymbolform" '); ?>

                                <select class="form-control basic-single" name="symbol" id="feessymbol">
                                    <option><?php echo display('select_option') ?></option>
                                    <?php foreach ($coins as $key => $value) { ?>
                                    <option value="<?php echo $value->symbol; ?>"><?php echo $value->symbol; ?></option>
                                    <?php } ?>
                                </select>

                            <?php echo form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <canvas id="singelBarChart" height="200"></canvas>
                </div>
            </div>
        </div>
        <!-- Flot Pie Chart -->
        <div class="col-sm-12 col-md-5">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <h4>Coin Market</h4>
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <canvas id="pieChart" height="300"></canvas>
                </div>
            </div>
        </div>
        <!-- Flot Pie Chart -->
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>Pending Withdraw</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo display('user_id') ?></th>
                                    <th><?php echo display('payment_method') ?></th>
                                    <th><?php echo display('wallet_id') ?></th>
                                    <th><?php echo display('amount') ?></th>
                                    <th><?php echo display('fees') ?></th>
                                    <th><?php echo display('status') ?></th>
                                    <th><?php echo display('action') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($withdraw)) ?>
                                <?php $sl = 1; ?>
                                <?php foreach ($withdraw as $value) { ?>
                                <tr>
                                    <td><?php echo $value->user_id; ?></td>
                                    <td><?php echo $value->method; ?></td>
                                    <td><?php 

                                        if (is_string($value->wallet_id) && is_array(json_decode($value->wallet_id, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false) {

                                            $decode_bank = json_decode($value->wallet_id, true);

                                            echo "<b>Account Name: </b>".$decode_bank['acc_name']."<br>";
                                            echo "<b>Account No: </b>".$decode_bank['acc_no']."<br>";
                                            echo "<b>Branch Name: </b>".$decode_bank['branch_name']."<br>";
                                            echo "<b>SWIFT Code: </b>".$decode_bank['swift_code']."<br>";
                                            echo "<b>ABN No: </b>".$decode_bank['abn_no']."<br>";
                                            echo "<b>Country: </b>".$decode_bank['country']."<br>";
                                            echo "<b>Bank Name: </b>".$decode_bank['bank_name'];
                                            
                                        }else{

                                            echo $value->wallet_id;
                                        }

                                     ?></td>
                                    <td><?php echo $value->amount; ?></td>
                                    <td><?php echo $value->fees_amount; ?></td>
                                    <td>
                                        <?php if($value->status==2){?>
                                         <a class="btn btn-warning btn-sm"><?php echo display('pending_withdraw')?></a>
                                         <?php } else if($value->status==1){?>
                                         <a class="btn btn-success btn-sm"><?php echo display('success')?></a>
                                         <?php } else if($value->status==0){ ?>
                                         <a class="btn btn-danger btn-sm"><?php echo display('cancel')?></a>
                                         <?php } ?>
                                     </td>
                                     <td>
                                         <a href="<?php echo base_url()?>backend/withdraw/withdraw/confirm_withdraw?id=<?php echo $value->id;?>&user_id=<?php echo $value->user_id;?>&set_status=2" class="btn btn-success btn-sm"><?php echo display('confirm')?></a>
                                         <a href="<?php echo base_url()?>backend/withdraw/withdraw/cancel_withdraw?id=<?php echo $value->id;?>&user_id=<?php echo $value->user_id;?>&set_status=3" class="btn btn-danger btn-sm"><?php echo display('cancel')?></a>
                                     </td>
                                    
                                </tr>
                                <?php } ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Flot Pie Chart -->
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-bd lobidisable">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>Trade History</h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" style="font-size:12px">
                            <thead>
                                <tr class="table-bg">
                                    <th>Trade</th>
                                    <th>Rate</th>
                                    <th>Required QTY</th>
                                    <th>Available QTY</th>
                                    <th>Required Amount</th>
                                    <th>Available Amount</th>
                                    <th>Market</th>
                                    <th>Open</th>
                                    <th>Complete QTY</th>
                                    <th>Complete Amount</th>
                                    <th>Trade Time</th>
                                    <th>Status</th>
                                </tr>
                            </thead>

                            <tbody id="usertradeHistory">
                                <?php  foreach ($trade_history as $key => $value) { ?>
                                    <tr>
                                        <td><?php echo $value->bid_type ?></td>
                                        <td><?php echo $value->bid_price ?></td>
                                        <td><?php echo $value->bid_qty ?></td>
                                        <td><?php echo $value->bid_qty_available ?></td>
                                        <td><?php echo $value->total_amount ?></td>
                                        <td><?php echo $value->amount_available ?></td>
                                        <td><?php echo $value->market_symbol ?></td>
                                        <td><?php echo $value->open_order ?></td>
                                        <td><?php echo $value->complete_qty ?></td>
                                        <td><?php echo $value->complete_amount ?></td>
                                        <td><?php echo $value->success_time ?></td>
                                        <td><?php echo $value->status==0?"<p class='btn-warning text-white text-center'>Canceled</p>":($value->status==1?"<p class='btn-success text-white text-center'>Completed</p>":"<p class='btn-primary text-white text-center'>Running</p>") ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    
    $(document).ready( function() {
        $.getJSON("<?php echo base_url('backend/dashboard/home/total_referral_value'); ?>", function(data) {
            var sum_balance=0;
            $.each(data.referral_value, function(index, element){
                
                var cryptolistfrom  = element.currency_symbol; 
                var cryptolistto    = 'BTC';
                $.getJSON("https://min-api.cryptocompare.com/data/price?fsym="+cryptolistfrom+"&tsyms="+cryptolistto+"", function(result) {

                        var btccoin = parseFloat(parseFloat(result[Object.keys(result)[0]]* +element.transaction_amount).toFixed(8)).toString()
                        var btccoin1=isNaN(btccoin) ? 0 : btccoin;
                        sum_balance = +sum_balance + +btccoin1;
                        $('#coin_value_BTC').text(parseFloat(sum_balance).toFixed(6)+' BTC');
                        usdSum1(sum_balance);

                });

            });


        });

    
    });


    $(document).ready( function() {
        $.getJSON("<?php echo base_url('backend/dashboard/home/all_fees_value'); ?>", function(data) {
            var sum_balance=0;
            $.each(data.fees_value, function(index, element){
                
                var cryptolistfrom  = element.currency_symbol; 
                var cryptolistto    = 'BTC';
                $.getJSON("https://min-api.cryptocompare.com/data/price?fsym="+cryptolistfrom+"&tsyms="+cryptolistto+"", function(result) {

                        var btccoin = parseFloat(parseFloat(result[Object.keys(result)[0]]* +element.total_qty).toFixed(8)).toString()
                        var btccoin1=isNaN(btccoin) ? 0 : btccoin;
                        sum_balance = +sum_balance + +btccoin1;
                        $('#fees_value_BTC').text(parseFloat(sum_balance).toFixed(6)+' BTC');
                        usdSum2(sum_balance);
                });

            });

        });
    
    });


function usdSum1(sum_balance){

     $.getJSON("https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD", function(result) {
            $('#coin_value_USD').text('$'+ parseFloat(result[Object.keys(result)[0]]*sum_balance).toFixed(2));

    });
}

function usdSum2(sum_balance){

     $.getJSON("https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD", function(result) {
            $('#fees_value_USD').text('$'+ parseFloat(result[Object.keys(result)[0]]*sum_balance).toFixed(2));

       

    });
}


</script>