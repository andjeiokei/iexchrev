<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h2><?php echo (!empty($title)?$title:null) ?></h2>
                </div>
            </div>
            <div class="panel-body">
                <div class="border_preview">

                <?php echo form_open_multipart("backend/dashboard/setting/fees_setting_save") ?>
                    <div class="row">
                        <div class="form-group col-lg-4">
                            <label for="coin_id" class="col-sm-4 col-form-label">Coin<i class="text-danger">*</i></label>
                            <select class="form-control basic-single" name="coin_id" required>
                                <option value=""><?php echo display('select_option') ?></option>
                                <?php foreach ($coins as $key => $value) { ?>
                                <option value="<?php echo $value->symbol; ?>"><?php echo $value->coin_name; ?></option>
                                <?php } ?>
                            </select>
                        </div>                       
                        <div class="form-group col-lg-4">
                            <label><?php echo display("select_level") ?> <i class="text-danger">*</i></label>
                           <select class="form-control" name="level" required id="action_type">
                               <option value="">--<?php echo display("select_level") ?>--</option>
                               <option value="BUY"><?php echo display("buy") ?></option>
                               <option value="SELL"><?php echo display("sell") ?></option>
                               <option value="DEPOSIT"><?php echo display("deposit") ?></option>
                               <option value="TRANSFER"><?php echo display("transfer") ?></option>
                               <option value="WITHDRAW"><?php echo display("withdraw") ?></option>
                           </select>
                        </div>

                        <div id="withdraw_hide">
                            <div class="form-group col-lg-4">
                                <label>Fees% <i class="text-danger">*</i></label>
                                <input type="text" class="form-control" name="fees" required >
                            </div>  
                        </div>  

                        <div id="withdraw_show" style="display:none;">

                            <div class="form-group col-lg-4">
                                <label>Fees <i class="text-danger">*</i></label>
                                <input type="text" class="form-control" name="flat_fees">
                            </div>

                            <div class="form-group col-lg-4">
                                <label>Minimum withdraw <i class="text-danger">*</i></label>
                                <input type="text" class="form-control" name="minimum_amount">
                            </div>

                        </div>

                    </div>

                    <div>
                        <button type="submit" class="btn btn-success"><?php echo display("save") ?></button>
                    </div>
                <?php echo form_close() ?>
            </div>

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 " >
        <div class="panel panel-bd">
            <div class="panel-heading ui-sortable-handle">
                <div class="panel-title" style="max-width: calc(100% - 180px);">
                    <h4><?php echo display('setting');?></h4>
                </div>
            </div>
            <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            
                            <thead>
                                <tr>
                                    <th><?php echo display('Level');?></th>
                                    <th><?php echo display('fees');?></th>
                                    <th>Coin</th>
                                   <th><?php echo display('action');?></th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php if(isset($fees_data)){ 
                                    foreach ($fees_data as $key => $value) {  
                                ?>
                                <tr>
                                    <td><?php echo $value->level;?></td>
                                    <td class="text-right"><?php echo $value->fees;?><?php echo ($value->level == 'WITHDRAW')? "/-":"%";?>
                                    <?php if($value->level == 'WITHDRAW'){?>
                                        <br><em>Minimum amount: <?php echo number_format($value->minimum_amount , 8);?></em>
                                    <?php }?>
                                </td>
                                    <td class="text-right"><?php echo $value->currency_symbol;?></td>
                                    <td>
                                        <a href="<?php echo base_url('backend/dashboard/setting/delete_fees_setting/'.$value->id) ?>" onclick="return confirm('<?php echo display("are_you_sure") ?>')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                <?php } } ?>
                            </tbody>
                        </table>
                    </div>
               
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    
    $("#action_type").on("change", function(){
        var action_type = $.trim( $(this).val() );
        
        if(action_type && action_type === "WITHDRAW")
        {
            $("#withdraw_hide").hide();
            $("#withdraw_show").show();
            $("#withdraw_hide > div > input").attr('required',false);
            $("#withdraw_show > div > input").attr('required',true);
        }
        else
        {
            $("#withdraw_show").hide();
            $("#withdraw_hide").show();
            $("#withdraw_show > div > input").attr('required',false);
            $("#withdraw_hide > div > input").attr('required',true);
        }

    });

});
</script>