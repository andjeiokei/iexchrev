<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h2><?php echo (!empty($title)?$title:null) ?></h2>
                </div>
            </div>
            <div class="panel-body">
                <table class="datatable2 table table-bordered table-hover" style="font-size:12px">
                    <thead>
                        <tr class="table-bg">
                            <th>Trade</th>
                            <th>Rate</th>
                            <th>Required QTY</th>
                            <th>Available QTY</th>
                            <th>User ID</th>
                            <th>Market</th>
                            <th>Open</th>
                            <th class="hide">Complete QTY</th>
                            <th class="hide">Complete Amount</th>
                            <th>Trade Time</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody id="usertradeHistory">
                        <?php  foreach ($trade_history as $key => $value) { ?>
                            <tr>
                                <td><?php echo $value->bid_type ?></td>
                                <td><?php echo $value->bid_price ?></td>
                                <td><?php echo $value->bid_qty ?></td>
                                <td><?php echo $value->bid_qty_available ?></td>
                                <td><a target="_blank" href="<?php echo base_url('backend/user/user/user_details/'.$value->user_id);?>"><?php echo $value->user_id;?></a></td>
                                <td><?php echo $value->market_symbol ?></td>
                                <td><?php echo $value->open_order ?></td>
                                <td class="hide"><?php echo $value->complete_qty ?></td>
                                <td class="hide"><?php echo $value->complete_amount ?></td>
                                <td><?php echo $value->success_time ?></td>
                                <td><?php echo ($value->bid_qty_available==0)?'Complete':'Running' ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $links; ?>
            </div> 
        </div>
    </div>
</div>

 