<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h2><?php echo (!empty($title)?$title:null) ?></h2>
                    <div class="col-sm-3 col-md-3 pull-right">
                        <a class="btn btn-success w-md m-b-5 pull-right" href="<?php echo base_url("backend/cms/news/form") ?>"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo display('post_news'); ?></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table class="datatable2 table table-bordered table-hover">
                    <thead>
                        <tr> 
                            <th><?php echo display('sl_no') ?></th>
                            <th><?php echo display('headline_en') ?></th>
                            <th><?php echo display('headline')." ".$web_language->name ?></th>
                            <th><?php echo display('category') ?></th>
                            <th><?php echo display('action') ?></th> 
                        </tr>
                    </thead>    
                    <tbody>
                        <?php if (!empty($article)) ?>
                        <?php $sl = 1; ?>
                        <?php foreach ($article as $value) { ?>
                        <tr>
                            <td><?php echo $sl++; ?></td> 
                            <td><?php echo $value->headline_en; ?></td>
                            <td><?php echo $value->headline_fr; ?></td>
                            <td><?php echo $this->db->select("cat_name_en")->from('web_category')->where('cat_id', $value->cat_id)->get()->row()->cat_name_en; ?></td>
                            <td>
                                <a href="<?php echo base_url("backend/cms/news/form/$value->article_id") ?>" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <a href="<?php echo base_url("backend/cms/news/delete/$value->article_id") ?>" onclick="return confirm('<?php echo display("are_you_sure") ?>')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0)" onclick="openSendMailModal('<?php echo $value->article_id ?>')" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="right" title="Send newsletter"><i class="fa 
fa-envelope-o" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <?php } ?>  
                    </tbody>
                </table>
                <?php echo $links; ?>
            </div> 
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="openSendMailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Send news letter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <?php echo form_open("backend/cms/news/send_news_letter",['class'=>'send_news_letter']) ?>
          <input type="hidden" name="newsletter_id" id="newsletter_id">
          <div class="form-group">
            <label for="sending_mode">Mode</label>
            <select class="form-control" name="sending_mode" id="sending_mode">
              <option value="selected_users">Selected user</option>
              <option value="all_users" disabled>All user</option>
            </select>
          </div>
          <div class="form-group">
            <label for="users">Select User</label>
            <select multiple class="form-control dont-select-me" name="users[]" id="users">
                <?php foreach ($users as $key => $value) { ?>
                    <option value="<?php echo $value->user_id ?>"><?php echo ((empty($value->first_name) && empty($value->last_name))? "No Name" :$value->first_name. " " . $value->last_name) . " (" . $value->email . ")"  ?></option>
                <?php } ?>
            </select>
          </div>
          <div class="form-group hidden sending_limit_input">
            <label for="sending_limit">Sending Limit (Per request)</label>
            <input class="form-control" type="number" min="1" name="sending_limit" id="sending_limit" value="400" />
          </div>
        <?php echo form_close() ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="$('.send_news_letter').submit()">Submit</button>
      </div>
    </div>
  </div>
</div>

 