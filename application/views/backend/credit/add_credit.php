<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                    <h2><?php echo (!empty($title)?$title:null) ?></h2>
                    <div class="col-sm-3 col-md-3 pull-right">
                        <a class="btn btn-success w-md m-b-5 pull-right" href="<?php echo base_url("backend/dashboard/credit/credit_list") ?>">Credit List</a>
                    </div>
                </div>
            </div>
            <div class="panel-body"> 
                <?php echo form_open('backend/dashboard/credit/add_credit','class="form-inner"') ?>
                    <div class="form-group row">
                        <label for="user_id" class="col-xs-3 col-form-label"><?php echo display('user_id') ?> <i class="text-danger">*</i></label>
                        <div class="col-xs-7">
                            <input name="user_id"  type="text" class="form-control" id="user_id" placeholder="<?php echo display('user_id') ?>" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="crypto_coin" class="col-xs-3 col-form-label">Coin <i class="text-danger">*</i></label>
                        <div class="col-sm-7">
                            <select class="form-control basic-single" name="crypto_coin" id="crypto_coin" required>
                                <option><?php echo display('select_option');?></option>
                                <?php foreach ($coin_list as $key => $value) {  ?>
                                <option value="<?php echo $value->symbol; ?>"><?php echo $value->full_name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="amount" class="col-xs-3 col-form-label"><?php echo display('amount') ?> <i class="text-danger">*</i></label>
                        <div class="col-xs-7">
                            <input name="amount" type="text" class="form-control" id="amount" placeholder="<?php echo display('amount') ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="notes" class="col-xs-3 col-form-label"><?php echo display('notes') ?> <i class="text-danger">*</i></label>
                        <div class="col-xs-7">
                            <textarea name="note" class="form-control" placeholder="<?php echo display('notes') ?>"  rows="4"></textarea>
                        </div>
                    </div>  
                    

                    <div class="form-group  row">
                        <div class="col-xs-10 text-right">
                            <a href="<?php echo base_url('admin'); ?>" class="btn btn-primary w-md m-b-5"><?php echo display("cancel") ?></a>
                            <button type="submit" class="btn btn-success w-md m-b-5"><?php echo display('send') ?></button>
                        </div>
                    </div>

                <?php echo form_close() ?>
            </div> 
        </div>
    </div>
</div>








 