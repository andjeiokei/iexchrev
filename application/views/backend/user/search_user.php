<div class="content">
	<div class="row">
		<div class="col-sm-12">

			 <?php echo form_open("backend/user/user/user_details") ?>
			<div class="form-group row">
                <label for="user_id" class="col-sm-2 col-form-label">User ID: </label>
                <div class="col-xs-2 col-sm-10 col-md-4 m-b-20">
                    <input name="user_id" class="form-control" type="search" id="user_id" value="<?php echo @$user->user_id ?>">
                </div>
                <div class="col-xs-2 col-sm-10 col-md-4 m-b-20">
                    <button type="submit" class="btn btn-success  w-md m-b-5">Search</button>
                </div>

            </div>
            <?php echo form_close() ?>

		</div>
	</div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 m-b-20">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab"><?php echo display('user_profile') ?></a></li>
                <li><a href="#tab2" data-toggle="tab">Balance</a></li>
                <li><a href="#tab3" data-toggle="tab">Transaction Log</a></li>
                <li><a href="#tab4" data-toggle="tab">Trade History</a></li>
            </ul>
            <!-- Tab panels -->
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tab1">
                    <div class="panel-body">
		                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                    <div class="form-group row">
		                        <label for="cid" class="col-sm-4 col-form-label"><?php echo display('user_id') ?></label>
		                        <div class="col-sm-8">
		                            <?php echo @$user->user_id ?></span>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label for="cid" class="col-sm-4 col-form-label"><?php echo display('referral_id') ?></label>
		                        <div class="col-sm-8">
		                            <?php echo @$user->referral_id ?></span>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label for="cid" class="col-sm-4 col-form-label"><?php echo display('language') ?></label>
		                        <div class="col-sm-8">
		                            <?php echo @$user->language ?></span>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label for="cid" class="col-sm-4 col-form-label"><?php echo display('firstname') ?></label>
		                        <div class="col-sm-8">
		                            <?php echo @$user->first_name ?></span>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label for="cid" class="col-sm-4 col-form-label"><?php echo display('lastname') ?></label>
		                        <div class="col-sm-8">
		                            <?php echo @$user->last_name ?></span>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label for="cid" class="col-sm-4 col-form-label"><?php echo display('email') ?></label>
		                        <div class="col-sm-8">
		                            <?php echo @$user->email ?></span>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label for="cid" class="col-sm-4 col-form-label"><?php echo display('mobile') ?></label>
		                        <div class="col-sm-8">
		                            <?php echo @$user->phone ?></span>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label for="cid" class="col-sm-4 col-form-label"><?php echo display('registered_ip') ?></label>
		                        <div class="col-sm-8">
		                            <?php echo @$user->ip ?></span>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label for="cid" class="col-sm-4 col-form-label"><?php echo display('status') ?></label>
		                        <div class="col-sm-8">
		                            <?php echo (@$user->status==1)?display('active'):display('inactive'); ?></span>
		                        </div>
		                    </div>
		                    <div class="form-group row">
		                        <label for="cid" class="col-sm-4 col-form-label">Registered Date</label>
		                        <div class="col-sm-8">
		                            <?php 
		                                $date=date_create(@$user->created);
		                                echo date_format($date,"jS F Y");  
		                            ?></span>
		                        </div>
		                    </div>
		                </div>
		            </div>
                </div>
                <div class="tab-pane fade" id="tab2">
                    <div class="panel-body">
                    	<table class="datatable1 table table-bordered table-hover table-striped">
		                    <thead>
		                        <tr> 
		                            <th><?php echo display('sl_no')?></th>
		                            <th><?php echo display('name')?></th>
		                            <th><?php echo display('total')." ".display('balance')?></th>
									<th><?php echo display('available_balance')?></th>
									<th><?php echo display('address')?></th>
		                        </tr>
		                    </thead>
		                    <tbody>
	                       <?php $i=1;  foreach ($balance as $key => $value) {
							   
							   $inOrdersQuery = $this->db->select_sum('total_amount')
											->from('dbt_biding')
											->where('user_id', $value->user_id)
											->where('bid_type','BUY')
											->where('status',2)
											//->like('currency_symbol', $value->currency_symbol)
											->like('market_symbol', "_".$value->currency_symbol, 'before')
											->get()
											->row();
								//echo $this->db->last_query();



								$inOrdersQuery2nd = $this->db->select_sum('total_amount')
											->from('dbt_biding')
											->where('user_id', $value->user_id)
											->where('bid_type','SELL')
											->where('status',2)
											//->like('currency_symbol', $value->currency_symbol)
											->like('market_symbol', $value->currency_symbol."_", 'after')
											->get()
											->row();
								//echo $this->db->last_query();
							
								if($inOrdersQuery->total_amount > 0){ 
									$inOrders = $inOrdersQuery->total_amount;
								} 
								else if($inOrdersQuery2nd->total_amount > 0){
									$inOrders = $inOrdersQuery2nd->total_amount;
								}
								else {
									$inOrders = 0;
								}
							 
							 
							 	?>
	                            <tr>
	                            	<td><?php echo $i; ?></td>
	                                <td>
	                                	<div class="d-flex marks-ico">
	                                        <img src="<?php echo base_url("$value->image") ?>" alt="" style="width:20px">

	                                            <font><?php echo $value->symbol; ?></font>
	                                            <span class="text-muted">(<?php echo $value->coin_name; ?>)</span>
	                                    </div>
	                                </td>
	                                <td><?php echo number_format( ( $value->balance + $inOrders ) , 8 ); ?></td>
                                    <td><?php echo number_format( $value->balance , 8); ?></td>
									<td><?php echo $value->address; ?></td>
	                            </tr>
	                        <?php $i++; } ?>
	                        </tbody>
	                	</table>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab3">
                    <div class="panel-body">
                    	<table class="datatable1 table table-bordered table-hover table-striped">
		                    <thead>
                                <tr class="table-bg">
                                    <th>SL</th>
                                    <th>Transaction</th>
                                    <th>Amount</th>
                                    <th>Fees</th>
                                    <th>Crypto/Dollar Currency</th>
                                </tr>
                            </thead>
		                    <tbody>

                       <?php $i=1;  foreach ($user_balanceLog as $key => $value) { ?>
                       		<tr>
                       			<td><?php echo $i ?></td>
                                <td><?php echo $value->transaction_type ?></td>
                                <td><?php echo $value->transaction_amount ?></td>
                                <td><?php echo $value->transaction_fees ?></td>
                                <td>
	                            	<div class="d-flex marks-ico">
	                                    <img src="<?php echo base_url("$value->image") ?>" alt="" style="width:20px">

	                                        <font><?php echo $value->symbol; ?></font>
	                                        <span class="text-muted">(<?php echo $value->coin_name; ?>)</span>
	                                </div>
	                            </td>
	                        </tr>

	                    <?php $i++; } ?>
	                    	</tbody>
	                	</table>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab4">
                    <div class="panel-body">
                    	<div id="user_tradelist"></div>
                    	<!-- <table id="user_tradelist" class="datatable1 table table-bordered table-hover table-striped" style="font-size:12px">
		                    <thead>
		                        <tr> 
		                            <th class="date">Trade</th>
                                    <th class="date">Rate</th>
                                    <th class="date">Required QTY</th>
                                    <th class="date">Available QTY</th>
                                    <th class="date">Required Amount</th>
                                    <th class="date">Available Amount</th>
                                    <th class="date">Market</th>
                                    <th class="date">Open</th>
                                    <th class="date">Complete QTY</th>
                                    <th class="date">Complete Amount</th>
                                    <th class="date">Trade Time</th>
                                    <th class="date">Status</th>
		                        </tr>
		                    </thead>
		                    <tbody id="user_tradelist"> -->
                    	<?php  //foreach ($user_trade_history as $key => $value) { ?>
                    			<!-- <tr>
                            		<td><?php //echo $value->bid_type ?></td>
                            		<td><?php //echo $value->bid_price ?></td>
                                    <td><?php //echo $value->bid_qty ?></td>
                            		<td><?php //echo $value->bid_qty_available ?></td>
                            		<td><?php //echo $value->total_amount ?></td>
                                    <td><?php //echo $value->amount_available ?></td>
                                    <td><?php //echo $value->market_symbol ?></td>
                                    <td><?php //echo $value->open_order ?></td>
                                    <td><?php //echo $value->complete_qty ?></td>
                                    <td><?php //echo $value->complete_amount ?></td>
                                    <td><?php //echo $value->success_time ?></td>
                            		<td><?php //echo $value->status==0?"<p class='label-warning text-white text-center'>Canceled</p>":($value->status==1?"<p class='label-success text-white text-center'>Completed</p>":"<p class='label-primary text-white text-center'>Running</p>") ?></td>
                            	</tr> -->
	                    <?php //} ?>
	                    	<!-- </tbody>
	                	</table> -->
	                	<span id="pagination_link"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){

 function load_country_data(page, user)
 {
	$.ajax({
		url:"<?php echo base_url(); ?>backend/user/user/ajax_tradelist/"+user+"/"+page,
		method:"GET",
		dataType:"json",
		success:function(data)
		{
			console.log(data);
			$('#user_tradelist').html(data.country_table);
			$('#pagination_link').html(data.pagination_link);
		}
	});
 }
 var user = $('#user_id').val();
 load_country_data(1, user);

	$(document).on("click", ".pagination li a", function(event){
		event.preventDefault();
		var page = $(this).data("ci-pagination-page");
		var user = $('#user_id').val();
		load_country_data(page, user);
	});
});

</script>