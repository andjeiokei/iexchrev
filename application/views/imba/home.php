<style>
.IMbaNbanner-section{
    text-align: center;
}
.IMbaNbody-wrapper{
    padding: 60px 0 80px 0;
}
.IMBAlft-info h2{
    font-size: 28px; 
    font-weight: 500;
    color: #fff;
    margin-bottom:15px;
}
.IMBAlft-info p{
    font-size: 16px; 
    color: #fff;
    margin-top:12px; 
}
.coinD-info{
    text-align: center;
}
body{
    background-color: #02112c;
}
</style>
<!-- Start of banner-->
<section class="IMbaNbanner-section">
    <img style="max-width: 100%; vertical-align: middle;" src="<?php echo base_url('assets/imba/') ?>images/IMBA-header-banner.png" alt="">
</section>  
<!-- End of banner-->
    
<!-- Start of Body-->
<section class="IMbaNbody-wrapper">
    <div class="container">
        <div class="row">
           
            <div class="col-sm-8 col-sm-pull-4 IMBAlft-info">
                <h2>What is IMBA?</h2>
                <p>IMBA, is a cryptocurrency created by Edjowa Holding. IMBA runs natively on the Neblio blockchain and follows the NPT1 token standard. The IMBA was established with a total supply of 200 million. IMBA plans to use 5% of our profits each year to buy back and burn IMBA, until 25% of the total supply (50 million IMBA) is burned.</p>
                <p>IMBA is a gaming token. You can use it to play against other Players in the arena.</p>
                <p>Later more and more features for IMBA will be established.</p>
                <p>IMBA is the bridge between gaming and trading.</p>
                <p>IMBA will be open for trading at the 01.June.2019 it BTC, ICR and NEBL Market</p>
            </div>
            <div class="col-sm-4 col-sm-push-8 coinD-info">
                <img src="<?php echo base_url('assets/imba/') ?>images/coin-distribution.png" alt="">
            </div>
        </div>
   </div><!-- //body-wrapper// -->
</section>
<!-- End of Body-->