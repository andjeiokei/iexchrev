
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo base_url('assets/arenafilesm/')?>css/bootstrap.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/arenafilesm/')?>css/global-style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/arenafilesm/')?>css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/arenafilesm/')?>css/animate.css" rel="stylesheet" type="text/css">
    <!-- Media queries CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/arenafilesm/')?>css/media-queries.css" rel="stylesheet" type="text/css">



    <link href="https://imba-exchange.co/assets/website/css/vendors.bundle.min.css" rel="stylesheet"/>
    <link href="https://imba-exchange.co/assets/website/fontawesome/css/fontawesome-all.min.css" rel="stylesheet">
    <link href="https://imba-exchange.co/assets/website/css/style.css" rel="stylesheet">
    <link href="https://imba-exchange.co/assets/js/sweetalert/sweetalert.css" rel="stylesheet" type="text/css"/>
    <link href="https://imba-exchange.co/assets/website/css/global-style.css" rel="stylesheet" type="text/css"/>


<section class="banner-section">		
<!--        <div class="owl-carousel owl-theme">-->

            <div class="background_top">
                <img class="logo_in_backgound" src="<?php echo base_url('assets/arenafilesm/')?>images/logo-transparent2.png">
            </div>

<!--            <div class="item"><img src="images/game-bg.jpg">
                <div class="caption"><h1 class="animated fadeInRight">Second Caption</h1></div>
            </div>

            <div class="item"><img src="images/game-bg.jpg">
                <div class="caption"><h1 class="animated fadeInRight">Third Caption</h1></div>
            </div>

            <div class="item"><img src="images/game-bg.jpg">
                <div class="caption"><h1 class="animated fadeInRight">Fourth Caption</h1></div>
            </div>

        </div><End Carousel -->
</section>							
			  
<!-- Start of Body-->
<section class="body-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <h1 class="game">Pong</h1>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="<?php echo base_url('arena/pong/');?>">
                    <img class="level-image" src="<?php echo base_url('assets/arenafilesm/')?>images/pong_rookie.png">
                    <h2 class="level">Rookie</h2>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="https://imba-exchange.co/arena/pongpro/">
                    <img class="level-image" src="<?php echo base_url('assets/arenafilesm/')?>images/pong_pro.png">
                    <h2 class="level">Pro</h2>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <img class="level-image" src="<?php echo base_url('assets/arenafilesm/')?>images/pong_imba.png">
                <h2 class="level">IMBA</h2>
            </div>
        </div>
   </div>
</section>

<!-- End of Body-->
    


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo base_url('assets/arenafilesm/')?>js/jquery-1.11.0.js"></script>
<!-- Script for detects HTML5 and CSS3 features in the user’s browser -->
<script src="<?php echo base_url('assets/arenafilesm/')?>js/modernizr.js"></script>
<!-- Include all compiled plugins, or include individual files as needed -->
<script src="<?php echo base_url('assets/arenafilesm/')?>js/bootstrap.min.js"></script>
<!-- Script for custom script-->
<script src="<?php echo base_url('assets/arenafilesm/')?>js/custom-script.js"></script>
<script src="<?php echo base_url('assets/arenafilesm/')?>js/owl.carousel.min.js" type="text/javascript"></script>
