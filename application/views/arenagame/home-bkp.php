<!-- Banner Start -->
<section class="banner-section">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="sf-banner">
                    <img class="desktop" src="<?php echo base_url('assets/arenagame/images/Battlenet_Shop_2280x700xl_tall.jpg')?>">
                    <img class="mobile" src="<?php echo base_url('assets/arenagame/images/1534x640mobile.jpg')?>">
                </div>
                <div class="b-content custom-container">
                    <div class="banner-caption">
                        <div class="animated fadeInRight B-logoImg"><img src="<?php echo base_url('assets/arenagame/images/logo-c.png')?>"></div>
                        <!-- <a class="animated fadeInRight B-buy-btn">Buy in presale</a> -->
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="sf-banner">
                    <img class="desktop" src="<?php echo base_url('assets/arenagame/images/Battlenet_Shop_2280x700xl_tall.jpg')?>">
                    <img class="mobile" src="<?php echo base_url('assets/arenagame/images/1534x640mobile.jpg')?>">
                </div>
                <div class="b-content custom-container">
                    <div class="banner-caption">
                        <div class="animated fadeInRight B-logoImg"><img src="<?php echo base_url('assets/arenagame/images/logo-c.png')?>"></div>
                        <!-- <a class="animated fadeInRight B-buy-btn">Buy in presale</a> -->
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="sf-banner">
                    <img class="desktop" src="<?php echo base_url('assets/arenagame/images/Battlenet_Shop_2280x700xl_tall.jpg')?>">
                    <img class="mobile" src="<?php echo base_url('assets/arenagame/images/1534x640mobile.jpg')?>">
                </div>
                <div class="b-content custom-container">
                    <div class="banner-caption">
                        <div class="animated fadeInRight B-logoImg"><img src="<?php echo base_url('assets/arenagame/images/logo-c.png')?>"></div>
                        <!-- <a class="animated fadeInRight B-buy-btn">Buy in presale</a> -->
                    </div>
                </div>
            </div>
        </div><!-- End Carousel -->
</section>							
<!-- Banner End -->		
    
<!-- Start of Body-->
<section class="body-wrapper">
    <div class="custom-container">
        <div class="title-body">
          <h2>Pong</h2>
        </div>
        <div class="prevu-kit-row">

            <div class="prevu-kit-item">
                <a href="http://imba-exchange.com/arena/pong/">
                <div class="prevu-kit-in">
                    <div class="prevu-img-box">
                        <img src="<?php echo base_url('assets/arenagame/images/product-img-1-full.jpg')?>">
                    </div>
                    <div class="prevu-content">
                        <h6>Rookie</h6>
                    </div>
                </div>
                </a>
            </div>
            <div class="prevu-kit-item">
                <div class="prevu-kit-in">
                    <div class="prevu-img-box">
                        <img src="<?php echo base_url('assets/arenagame/images/product-img-3-full.jpg')?>">
                    </div>
                    <div class="prevu-content">
                        <h6>Pro</h6>
                    </div>
                </div>
            </div>
            <div class="prevu-kit-item">
                <div class="prevu-kit-in">
                    <div class="prevu-img-box">
                        <img src="<?php echo base_url('assets/arenagame/images/product-img-2-full.jpg')?>">
                    </div>
                    <div class="prevu-content">
                        <h6>IMBA</h6>
                    </div>
                </div>
            </div>
           

        </div>
   </div>
</section>
<!-- End of Body-->