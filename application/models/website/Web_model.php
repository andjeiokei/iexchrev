<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Web_model extends CI_Model {

	function __construct() {

    }
    public function checkUseridExist($key)
	{	 
		$query = $this->db->where('user_id', $key)->get('dbt_user')->num_rows();
		
		return ($query > 0) ? true : false;

	}

	public function checkEmailExist($key)
	{	 
		$query = $this->db->where('email', $key)->get('dbt_user')->num_rows();

		return ($query > 0) ? true : false;

	}

	public function checkPhoneExist($key)
	{	 
		$query = $this->db->where('phone', $key)->get('dbt_user')->num_rows();

		return ($query > 0) ? true : false;

	}

	public function registerUser($data = array())
	{	 
		$data['created'] = date("Y-m-d H:i:s");        
		return $this->db->insert('dbt_user',$data);

	}

	public function updateUser($data = array())
	{	        
		return $this->db->where('user_id', $data['user_id'])->update("dbt_user", $data);		 

	}

	public function accountStatusCheck($key)
	{	 
		$query = $this->db->select('status')->from('dbt_user')->where('email', $key)->get()->row();
		
        return $query->status;

	}

	public function loginCheckUser($data = array())
	{

		$where = "(email ='".$data['email']."' OR username = '".$data['email']."') AND password = '".$data['password']."'";

		return $this->db->select("*")
			->from('dbt_user')
			->where($where)
			->get()
			->row();
	}

	public function passwordtokenCheck($key)
	{	 
		$query = $this->db->select('status')->from('dbt_user')->where('password_reset_token', $key)->get()->row();
		
        return $query;

	}

	public function activeUserAccount($key)
	{
		$user = $this->db->where('password_reset_token', $key)->from('dbt_user')->get()->row();

		$usrBal = $this->db->from('dbt_balance')->where('user_id', $user->user_id)
			->where('currency_id', 2340)
			->get()->row();
		if($usrBal){

		} else {
			$this->db->insert('dbt_balance', [
				'user_id' => $user->user_id,
				'currency_id' => 2340, 
				'currency_symbol' => 'IMBA',
				'balance' => 1500
			]);
		}
		if($user->referral_id){
			$refUser = $this->db->where('user_id', $user->referral_id)->from('dbt_user')->get()->row();
			if($refUser){
				$balRow = $this->db->from('dbt_balance')
						->where('currency_id', 2340)
						->where('user_id', $refUser->user_id)
						->get()->row();
				if($balRow){
					$newBal = $balRow->balance + 150;
					$this->db->where('id', $balRow->id);
					$this->db->update('dbt_balance', ['balance' => $newBal]);
				} else {
					$this->db->insert('dbt_balance', [
						'user_id' => $refUser->user_id,
						'currency_id' => 2340, 
						'currency_symbol' => 'IMBA',
						'balance' => 1500
					]);
				}
			}
		}

		return $this->db->set('status', '1')
			->where('password_reset_token', $key)
			->update("dbt_user");
	}

	public function storeUserLogData($data = array())
	{
		return $this->db->insert('dbt_user_log', $data);

	}

	public function userVerifyDataStore($data = array())
	{
		return $this->db->insert('dbt_user_verify_doc', $data);

	}

    public function tradeCreate($data = array())
	{
		$this->db->insert('dbt_biding', $data);
		return  $this->db->insert_id();

	}

	public function pendingTrade($key = null)
	{
		return $this->db->select('*')
			->from('dbt_biding')
			->where('status', 2)
			->where('market_symbol', $key)
			->order_by('bid_price', 'desc')
			->get()
			->result();

	}

	public function openTrade()
	{
		return $this->db->select('*')
			->from('dbt_biding')
			->where('status', 2)
			->where('user_id', $this->session->userdata('user_id'))
			->order_by('id','DESC')
			->get()
			->result();

	}

	public function completeTrade()
	{
		return $this->db->select('*')
			->from('dbt_biding')
			->where('status', 1)
			->where('user_id', $this->session->userdata('user_id'))
			->order_by('id','DESC')
			->get()
			->result();

	}

	public function tradeHistory($key)
	{
		// return $this->db->select('DBL.*, DATE_FORMAT(DBL.success_time, "%H:%i:%S") AS success_time')
		// 	->from('dbt_biding_log DBL')
		// 	->join('dbt_biding DB','DB.id = DBL.bid_id','INNER')
		// 	->where(array('DBL.market_symbol'=>$key, 'DBL.status'=>1, 'DB.status'=>1))
		// 	->group_by('DBL.bid_id')
		// 	->order_by('DBL.log_id', 'DESC')
		// 	->get()
		// 	->result();

		return $this->db->select('*, DATE_FORMAT(success_time, "%H:%i:%S") AS success_time')
			->from('dbt_biding_log')
			->where('market_symbol', $key)
			->group_by('bid_id')
		 	->order_by('log_id', 'DESC')
			->get()
			->result();

	}

	public function coinMarkets()
	{
		return $this->db->select('*')
			->from('dbt_market')
			->where('status', 1)
			->get()
			->result();

	}

	public function coinPairs()
	{
		return $this->db->select('*')
			->from('dbt_coinpair')
			->where('status', 1)
			->order_by('symbol', 'ASC')
			->get()
			->result();

	}

	public function marketDetails($key=null)
	{
		return $this->db->select('*')
			->from('dbt_coinpair')
			->where('symbol', $key)
			->where('status', 1)
			->get()
			->row();

	}

	public function userTradeHistory( $data = array() )
	{
		if(!empty($data['start_date']))
		{
			$temp_date = explode("-", $data['start_date']);
			$start_date = $temp_date[2]."-".$temp_date[0]."-".$temp_date[1];
			$this->db->where('bidmaster.open_order >= ', date("Y-m-d 00:00:00", strtotime($start_date)));
		}

		if(!empty($data['end_date']))
		{
			$temp_date = explode("-", $data['end_date']);
			$end_date = $temp_date[2]."-".$temp_date[0]."-".$temp_date[1];
			$this->db->where('bidmaster.open_order <= ', date("Y-m-d 23:59:59", strtotime($end_date)));
		}

		if(!empty($data['second_pair']) && !empty($data['first_pair']))
		{
			$this->db->where('bidmaster.market_symbol', $data['first_pair']."_".$data['second_pair']);
		}
		else if(!empty($data['first_pair']))
		{
			$this->db->like('bidmaster.market_symbol', $data['first_pair']."_", 'after');
		}
		else if(!empty($data['second_pair']))
		{
			$this->db->like('bidmaster.market_symbol', "_".$data['second_pair'], 'before');
		}

		if( !empty($data['side']) )
		{
			$this->db->where('bidmaster.bid_type', $data['side']);
		}

		if( $data['is_canclled_hide'] == 1 )
		{
			$this->db->where('bidmaster.status<>', 0);
		}

		if( !empty($data['limit']) )
		{
			$this->db->limit($data['limit'], $data['start']);
		}

		

		return $this->db->select('bidmaster.*, biddetail.bid_type as bid_type1, biddetail.bid_price as bid_price1, biddetail.market_symbol as market_symbol1, biddetail.complete_amount as complete_amount1, biddetail.success_time as success_time1, biddetail.complete_qty, biddetail.complete_amount, biddetail.success_time')
			->from('dbt_biding bidmaster')
			->join('dbt_biding_log biddetail', 'biddetail.bid_id = bidmaster.id', 'left')
			->where('bidmaster.user_id', $this->session->userdata('user_id'))
			->order_by('biddetail.success_time DESC, bidmaster.id DESC')
			->get()
			->result();

		// return $this->db->select('*')
		// 	->from('dbt_biding')
		// 	->where('user_id', $this->session->userdata('user_id'))
		// 	->get()
		// 	->result();

	}

	public function checkCoinrate($key)
	{
		return $this->db->select('*')
			->from('dbt_biding_log')
			->where('currency_symbol', $key)
			->where('bid_type', 'BUY')
			->order_by("bid_price", "desc")
			->limit(2, 0)
			->get()
			->result();

	}

	public function checkBalance($key, $user=null)
	{
		if ($user==null) {
			$user = $this->session->userdata('user_id');
		}

		return $this->db->select('*')
			->from('dbt_balance')
			->where('user_id', $user)
			->where('currency_symbol', $key)
			->get()
			->row();

	}

	public function balanceLog($key = null, $user = null)
	{
		if ($user==null) {
			$user = $this->session->userdata('user_id');
		}

		return $this->db->select('*')
			->from('dbt_balance_log balancelog')
			->join('dbt_cryptocoin coin', 'balancelog.currency_symbol = coin.symbol')
			->where('user_id', $user)
			//->where('currency_symbol', $key)
			->order_by('transaction_type', 'desc')
			->get()
			->result();

	}
	
	public function checkFees($type, $coin)
	{
		return $this->db->select('*')
			->from('dbt_fees')
			->where('level', $type)
			->where('currency_symbol', $coin)
			->get()
			->row();

	}

	public function balanceAdd($data = array())
	{
		$this->db->insert('dbt_balance', $data);
		return  $this->db->insert_id();

	}
	public function balanceDebit($data = array())
	{
		$check_user_balance = $this->db->select('*')->from('dbt_balance')->where('user_id', $data->user_id)->where('currency_symbol', $data->currency_symbol)->get()->row();

		$updatebalance = array(
            'balance'     => $check_user_balance->balance-($data->bid_qty+$data->fees_amount),
        );

        return $this->db->where('user_id', $data->user_id)->where('currency_symbol', $data->currency_symbol)->update("dbt_balance", $updatebalance);

	}

	public function balanceCredit($data = array(), $coin_symbol)
	{
		$check_user_balance = $this->db->select('*')->from('dbt_balance')->where('user_id', $data->user_id)->where('currency_symbol', $coin_symbol)->get()->row();

		$updatebalance = array(
            'balance'     => @$check_user_balance->balance-(@$data->total_amount+@$data->fees_amount),
        );

        return $this->db->where('user_id', $data->user_id)->where('currency_symbol', $coin_symbol)->update("dbt_balance", $updatebalance);

	}

	//Return Balance
	public function balanceReturn($data = array()){

		$balance = $this->db->select('*')->from('dbt_balance')->where('user_id', $data['user_id'])->where('currency_symbol',$data['currency_symbol'])->get()->row();

		$updatebalance = array(
            'balance'     => $balance->balance+$data['amount']+$data['return_fees'],
        );

        $this->db->where('user_id', $data['user_id'])->where('currency_symbol',$data['currency_symbol'])->update("dbt_balance", $updatebalance);

        $logdata = array(
        	'balance_id'		=> $balance->id,
        	'user_id' 			=> $data['user_id'],
        	'currency_symbol'	=> $data['currency_symbol'],
        	'transaction_type' 	=> 'ADJUSTMENT',
        	'transaction_amount'=> $data['amount'],
        	'transaction_fees'	=> $data['return_fees'],
        	'ip' 				=> $data['ip'],
        	'date'				=> date('Y-m-d H:i:s')
        );

        $this->db->insert("dbt_balance_log",$logdata);

	}

	public function checkUserAllBalance()
	{
		return $this->db->select('*')
			->from('dbt_balance balance')
			->join('dbt_cryptocoin coin', 'coin.symbol = balance.currency_symbol', 'left')
			->where('balance.user_id', $this->session->userdata('user_id'))
			->get()
			->result();

	}

	public function checkUserAllBalance1()
	{
		return $this->db->select('*')
			->from('dbt_balance')
			->where('user_id', $this->session->userdata('user_id'))
			->get()
			->result();

	}

	public function getAllReceived()
	{
		return $this->db->select('*')
			->from('wallet_receive')
			->where('confirmed', 0)
			->get()
			->result();
	}

	public function getAllSend()
	{
		return $this->db->select('*')
			->from('wallet_send')
			->where('confirmed', 0)
			->get()
			->result();
	}

	public function getAllSendbyCoin($coin_id)
	{
		return $this->db->select('*')
			->from('wallet_send')
			->where('confirmed', 0)
			->where('coin_id', $coin_id)
			->get()
			->result();
	}

	public function updatewalletSend($txId, $id)
	{
		$data = array(
			'txid' => $txId,
			'confirmed' => 1
		);
		
		$this->db->where('id', $id);
		$this->db->update('wallet_send', $data);
	}

	public function getUserbyCoinAddr($address, $coin_id=null)
	{
		$user_id = $this->db->select('user_id')
			->from('address')
			->where('address', $address);
			
			if($coin_id){
				$user_id = $user_id->where('coin_id', $coin_id);
			}
			$user_id = $user_id->get()->row();
		return $user_id;
	}

	public function getCoinById($coin_id)
	{
		return $this->db->select('*')
			->from('dbt_cryptocoin')
			->where('status', 1)
			->where('id', $coin_id)
			->get()->row();
	}

	public function getCoinBySymbol($symbol)
	{
		return $this->db->select('*')
			->from('dbt_cryptocoin')
			->where('status', 1)
			->where('symbol', $symbol)
			->get()->row();
	}
	
	public function withdraw($data)
	{
		$this->db->insert('dbt_withdraw',$data);
		return array('id'=>$this->db->insert_id());
	}

	public function get_withdraw_by_id($id)
	{
		return $this->db->select('*')
		->from('dbt_withdraw')
		->where('id',$id)
		->where('user_id',$this->session->userdata('user_id'))
		->get()->row();

	}

	public function coinpayment_withdraw()
	{
		$data = $this->db->select('data')
		->from('payment_gateway')
		->where('id',8)
		->get()
		->row();

		$data_tbl = json_decode($data->data,true);
		$withdraw = $data_tbl['withdraw'];

		return $withdraw;
	}

	public function transfer($data)
	{
		$this->db->insert('dbt_transfer',$data);
		return array('id'=>$this->db->insert_id());

	}

	public function availableForBuy($key)
	{
		$sum = $this->db->select_sum('bid_qty_available')
			->from('dbt_biding')
			->where('bid_type', 'BUY')
			->where('market_symbol', $key)
			->where('status', 2)
			->get()
			->row();

		return $sum;

	}

	public function verify($data)
	{
		$this->db->insert('dbt_verify',$data);
		return array('id'=>$this->db->insert_id());
		
	}


    public function get_verify_data($id)
    {
        $v = $this->db->select('*')
        ->from('dbt_verify')
        ->where('id',$id)
        ->where('session_id', $this->session->userdata('isLogIn'))
        ->where('ip_address', $this->input->ip_address())
        ->get()
        ->row();

        return $v;
    }

	public function availableForSell($key)
	{
		$sum = $this->db->select_sum('bid_qty_available')
			->from('dbt_biding')
			->where('bid_type', 'SELL')
			->where('market_symbol', $key)
			->where('status', 2)
			->get()
			->row();

		return $sum;

	}

	public function retriveUserInfo()
	{
		return $this->db->select('*')
			->from('dbt_user')
			->where('user_id', $this->session->userdata('user_id'))
			->get()
			->row();

	}

	public function retriveUserlog()
	{
		return $this->db->select('*')
			->from('dbt_user_log')
			->where('user_id', $this->session->userdata('user_id'))
			->order_by('access_time', 'desc')
			->limit(10, 0)
			->get()
			->result();

	}

	public function retrieveAffiliateUsers(){
	    return $this->db->select('*')
            ->from('dbt_user')
            ->where('referral_id', $this->session->userdata('user_id'))
            ->get()
            ->result();

    }

	public function activeCoin($key=null)
	{
		return $this->db->select('*')
			->from('dbt_cryptocoin')
			->where('status', 1)
			->order_by('rank', 'asc')
			->limit($key)
			// ->order_by('name', 'asc')
			->get()
			->result();

	}

	public function activeHomeCoin()
	{
		return $this->db->select('*')
			->from('dbt_cryptocoin')
			// ->where('status', 1)
			->where('show_home', 1)
			->order_by('coin_position', 'asc')
			//->limit($key)
			// ->order_by('name', 'asc')
			->get()
			->result();

	}






	//CMS Query Function
	public function slider()
	{
		return $this->db->select('*')
			->from('web_slider')
			->where('status', 1)
			->order_by('id', 'desc')
			->get()
			->result();
	}

	public function subscribe($data = [])
	{	 
		return $this->db->insert('web_subscriber',$data);
	}

	public function socialLink()
	{
		return $this->db->select('*')
			->from('web_social_link')
			->where('status', 1)
			->order_by('id', 'asc')
			->get()
			->result();
	}
	public function cat_info($slug=NULL){
		return $this->db->select("*")
			->from('web_category')
			->where('slug', $slug)
			->where('status', 1)
			->get()
			->row();
	}

	public function newsCatListBySlug($slug=NULL)
	{	 
		$cat_id = $this->db->select('cat_id')->from('web_category')->where('slug', $slug)->get()->row();

		return $this->db->select('*')
			->from('web_category')
			->where('status', 1)
			->order_by('cat_id', 'desc')
			->where('parent_id', $cat_id->cat_id)
			->get()
			->result();
	}

	public function catidBySlug($slug=NULL){
		return $this->db->select("cat_id")
			->from('web_category')
			->where('slug', $slug)
			->where('status', 1)
			->get()
			->row();
	}
	public function newsDetails($slug = null)
	{
		return $this->db->select('*')
			->from('web_news')
			->where('slug', $slug)
			->get()
			->row();
	}
	public function advertisement($id=NULL){
		return $this->db->select("*")
			->from('advertisement')
			->where('page', $id)
			->where('status', 1)
			->order_by('serial_position', 'asc')
			->get()
			->result();
	}
public function article($id=NULL, $limit=NULL){
		return $this->db->select("*")
			->from('web_article')
			->where('cat_id', $id)
			->order_by('position_serial', 'asc')
			->limit($limit)
			->get()
			->result();
	}
public function tradeNotice($id=NULL, $limit=NULL){
		return $this->db->select("*")
			->from('web_article')
			->where('cat_id', $id)
			->order_by('article_id', 'desc')
			->limit($limit)
			->get()
			->result();
	}

	public function categoryList()
	{	 
		return $this->db->select('*')
			->from('web_category')
			->where('status', 1)
			->order_by('position_serial', 'asc')
			->get()
			->result();
	}

	public function webLanguage(){
		return $this->db->select('*')
			->from('web_language')
			->where('id', 1)
			->get()
			->row();
	}




	public function read($limit, $offset)
	{
		return $this->db->select("*")
			->from('dbt_biding')
			->where('bid_type', 'BUY')
			->order_by('id', 'asc')
			->limit($limit, $offset)
			->get()
			->result();

	}

	public function single($bid_id = null)
	{
		return $this->db->select('*')
			->from('dbt_biding')
			->where('id', $bid_id)
			->get()
			->row();

	}

	public function all()
	{
		return $this->db->select('*')
			->from('dbt_biding')
			->get()
			->result();

	}

	public function pending_trade()
	{
		return $this->db->select('*')
			->from('dbt_biding')
			->where('status', 2)
			->get()
			->result();

	}


	public function delete($bid_id = null)
	{
		return $this->db->where('bid_id', $bid_id)
			->delete("dbt_biding");
	}


	public function checkDuplictemail($data = [])
	{
		return $this->db->select("dbt_user.email")
			->from('dbt_user')
			->where('email', $data['email'])
			->get();
	}

	public function checkDuplictuser($data = [])
	{	 
		return $this->db->select("dbt_user.username")
			->from('dbt_user')
			->where('username', $data['username'])
			->get();
	}

	public function coin_details($where = NULL)
	{
		if(!empty($where))
		{
			$this->db->where($where);
		}
		return $this->db->select("*")
			->from('dbt_cryptocoin')
			->get()
			->row_array();
	}

}
