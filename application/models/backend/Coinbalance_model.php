<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include(APPPATH .'libraries/JsonrpcClient/JsonrpcClient.php');

class Coinbalance_model extends CI_Model {

    function count_filtered()
	{
		$this->_get_datatables_query();
		// for removign the deleted records
		$this->db->where("status",1);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from('dbt_cryptocoin')->where('status', 1);
		return $this->db->count_all_results();
    }
    
    public function count_all_2nd()
	{
		// for removign the deleted records
		$this->db->where("status", 1);
		$query = $this->db->get('dbt_cryptocoin');
		return $query->num_rows();
    }

    var $column_order = array(null, 'cid', 'symbol', 'coin_name', 'full_name', 'algorithm', 'rank', 'show_home', 'coin_position'); //set column field database for datatable orderable
	var $column_search = array('cid', 'symbol', 'coin_name', 'full_name', 'algorithm', 'rank', 'show_home', 'coin_position'); //set column field database for datatable searchable 

	var $order = array('rank' => 'asc'); // default order 

    
    private function _get_datatables_query()
	{
		$this->db->from('dbt_cryptocoin');
		$i = 0;
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
			
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function checkAllUserAvailableBalance($currency_symbol)
	{
		$AllAvail = $this->db->select_sum('balance')
			->from('dbt_balance')
			->where('currency_symbol', $currency_symbol)
			->get()
			->result();
		return $AllAvail[0]->balance;
	}

	public function checkAllUserInOrderBalance($currency_symbol)
	{
		$inOrders = 0;
		$inOrdersQuery = $this->db->select_sum('total_amount')
					->from('dbt_biding')
					->where('bid_type','BUY')
					->where('status',2)
					->like('market_symbol', "_".$currency_symbol, 'before')
					->get()
					->row();

		$inOrdersQuery2nd = $this->db->select_sum('total_amount')
					->from('dbt_biding')
					->where('bid_type','SELL')
					->where('status',2)
					->like('market_symbol', $currency_symbol."_", 'after')
					->get()
					->row();
	
		if($inOrdersQuery->total_amount > 0){ 
			$inOrders = $inOrdersQuery->total_amount;
		} 
		else if($inOrdersQuery2nd->total_amount > 0){
			$inOrders = $inOrdersQuery2nd->total_amount;
		}
		return $inOrders;
	}

	public function checkDaemonBalanceByCoin($currency_symbol)
	{
		$this->load->library('encryption');
		$this->encryption->initialize(
            array(
                    'cipher' => 'aes-256',
                    'mode' => 'ctr',
                    'key' => $this->config->item('enc_key_rpc_password'),
            )
		);
		$this->load->model(array(
            'website/web_model',
        ));
		$CoinDetails = $this->web_model->getCoinBySymbol($currency_symbol);

		if($CoinDetails){
			//if($CoinDetails->proof_type == "PoW"){
				try{
				$walletClient = new jsonRPCClient('http://'.$CoinDetails->username.':'.$this->encryption->decrypt($CoinDetails->password).'@'.$CoinDetails->host.':'.$CoinDetails->port.'/');

				$DaemonBal = $walletClient->getbalance();
				} catch(Exception $e){ print_r($e);
				}
			//}
		}else{
			echo "Coin not activated";
		}
		
		return $DaemonBal;
	}
}