<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exchange_model extends CI_Model {
 	
 	public function read($limit, $offset)
	{
		return $this->db->select("*")
			->from('dbt_biding')
			->order_by('open_order', 'asc')
			->limit($limit, $offset)
			->get()
			->result();
	}

	public function openTrade($limit, $offset)
	{
		return $this->db->select('*')
			->from('dbt_biding')
			->where('status', 2)
			->order_by('id', 'DESC')
			->limit($limit, $offset)
			->get()
			->result();

	}
	public function TradeHistory($limit, $offset)
	{
		if($limit)
		{
			$this->db->limit($limit, $offset);
		}

		return $this->db->select('bidmaster.*, biddetail.bid_type as bid_type1, biddetail.bid_price as bid_price1, biddetail.market_symbol as market_symbol1, biddetail.complete_amount as complete_amount1, biddetail.success_time as success_time1, biddetail.complete_qty, biddetail.complete_amount, biddetail.success_time')
			->from('dbt_biding bidmaster')
			->join('dbt_biding_log biddetail', 'biddetail.bid_id = bidmaster.id', 'left')
			->where('bidmaster.bid_qty_available', 0)
			->order_by('bidmaster.id', 'DESC')
			->get()
			->result();

	}
}
