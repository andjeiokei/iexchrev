<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newscron_model extends CI_Model {
 
	public function create($data = array())
	{
		return $this->db->insert('web_news_cron', $data);
	}

	public function read($limit, $offset)
	{
		return $this->db->select("*")
			->from('web_news_cron')
			->order_by('id', 'asc')
			->limit($limit, $offset)
			->get()
			->result();
	}

	public function single($id = null)
	{
		return $this->db->select('*')
			->from('web_news_cron')
			->where('id', $id)
			->get()
			->row();
	}

	public function all()
	{
		return $this->db->select('*')
			->from('web_news_cron')
			->where('status','1')
			->get()
			->result();
	}

	public function update($data = array())
	{
		return $this->db->where('id', $data["id"])
			->update("web_news_cron", $data);
	}


	public function get_today_cron()
	{
		return $this->db->select('*')
			->from('web_news_cron')
			->where('DATE(created_at)', date('Y-m-d'))
			->where('status', '1')
			->get()
			->num_rows();
	}

}