<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

	private $table = "admin"; 

	public function profile($id = null)
	{
		return $this->db->select("
				admin.*, 
				CONCAT_WS(' ', firstname, lastname) AS fullname, 
				IF (admin.is_admin=1, 'Admin', 'User') as user_level")
			->from("admin") 
			->where('id', $id)
			->get()
			->row();
	} 

	public function update_profile($data = array())
	{
		return $this->db->where('id', $data['id'])
			->update('admin', $data);
	}

	public function userList()
	{
		return $this->db->select("*")
			->from("dbt_user")
			->get();
	}

	public function coinpairList()
	{
		return $this->db->select("*")
			->from("dbt_coinpair")
			->get();
	}

	public function pendingWithdrawList()
	{
		return $this->db->select("*")
			->from("dbt_withdraw")
			->where("status", 2)
			->limit(5)
			->get()
			->result();
	}
	public function marketTradeHistory()
	{
		return $this->db->select('bidmaster.*, biddetail.bid_type as bid_type1, biddetail.bid_price as bid_price1, biddetail.market_symbol as market_symbol1, biddetail.complete_amount as complete_amount1, biddetail.success_time as success_time1, biddetail.complete_qty, biddetail.complete_amount, biddetail.success_time')
			->from('dbt_biding bidmaster')
			->join('dbt_biding_log biddetail', 'biddetail.bid_id = bidmaster.id', 'left')
			//->where('bidmaster.user_id', $this->session->userdata('user_id'))
			->limit(20)
			->get()
			->result();
	}
	public function coinList()
	{
		return $this->db->select('*')
			->from('dbt_cryptocoin')
			->where('status', 1)
			->order_by('rank', 'asc')
			->get()
			->result();
	}
	public function monthlyDeposit($currency = 'USD')
	{
		return $query = $this->db->query("SELECT MONTHNAME(`deposit_date`) as month, SUM(`amount`) as deposit FROM `dbt_deposit` WHERE `currency_symbol`='".$currency."' GROUP BY YEAR(`deposit_date`), MONTH(`deposit_date`)")->result();
	}
	public function monthlyWithdraw($currency = 'USD')
	{
		return $query = $this->db->query("SELECT MONTHNAME(`request_date`) as month, SUM(`amount`) as withdraw FROM `dbt_withdraw` WHERE `currency_symbol`='".$currency."' GROUP BY YEAR(`request_date`), MONTH(`request_date`)")->result();
	}
	public function monthlyTransfer($currency = 'USD')
	{
		return $query = $this->db->query("SELECT MONTHNAME(`date`) as month, SUM(`amount`) as transfer FROM `dbt_transfer` WHERE `currency_symbol`='".$currency."' GROUP BY YEAR(`date`), MONTH(`date`)")->result();
	}
	public function monthlyFees($currency = 'BTC')
	{
		return $query = $this->db->query("SELECT MONTHNAME(`success_time`) as month, SUM(`fees_amount`) as fees FROM `dbt_biding_log` WHERE `currency_symbol`='".$currency."' GROUP BY YEAR(`success_time`), MONTH(`success_time`)")->result();
	}
	public function coinTradeMarket()
	{

		$this->db->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
		
		return $this->db->query("SELECT `currency_symbol` as currency_symbol, SUM(`bid_qty`) as bid_qty FROM `dbt_biding` WHERE `bid_type`='BUY' GROUP BY `currency_symbol`")->result();
	}

	// public function tradeComplete()
	// {
	// 	return $this->db->select("complete_qty")
	// 		->from("dbt_biding_log")
	// 		->where('status', 1)
	// 		->get()
	// 		->row();
	// }




  
}
