<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
 
	public function create($data = array())
	{
		return $this->db->insert('dbt_user', $data);
	}

	public function read($limit, $offset)
	{
		return $this->db->select("
				dbt_user.*, 
				CONCAT_WS(' ', first_name, last_name) AS fullname 
			")
			->from('dbt_user')
			->order_by('id', 'desc')
			->limit($limit, $offset)
			->get()
			->result();
	}

	public function single($id = null)
	{
		return $this->db->select('*')
			->from('dbt_user')
			->where(' ( id LIKE "'.$id.'" OR user_id LIKE "'.$id.'")')
			->get()
			->row();
	}

	public function userByUserId($user_id = null)
	{
		return $this->db->select('*')
			->from('dbt_user')
			->where('user_id', $user_id)
			->get()
			->row();
	}
	public function userTradeHistory($user_id = null)
	{
		return $this->db->select('bidmaster.*, biddetail.bid_type as bid_type1, biddetail.bid_price as bid_price1, biddetail.market_symbol as market_symbol1, biddetail.complete_amount as complete_amount1, biddetail.success_time as success_time1, biddetail.complete_qty, biddetail.complete_amount, biddetail.success_time')
			->from('dbt_biding bidmaster')
			->join('dbt_biding_log biddetail', 'biddetail.bid_id = bidmaster.id', 'left')
			->where('bidmaster.user_id', $user_id)
			->get()
			->result();

	}

	function ajax_trade_fetch_details($limit, $start, $user_id)
	  {
	  $output = '';
	    $this->db->select('bidmaster.*, biddetail.bid_type as bid_type1, biddetail.bid_price as bid_price1, biddetail.market_symbol as market_symbol1, biddetail.complete_amount as complete_amount1, biddetail.success_time as success_time1, biddetail.complete_qty, biddetail.complete_amount, biddetail.success_time');
	    $this->db->from('dbt_biding bidmaster');
	    $this->db->join('dbt_biding_log biddetail', 'biddetail.bid_id = bidmaster.id', 'left');
	    $this->db->where('bidmaster.user_id', $user_id);
	    $this->db->limit($limit, $start);
	    $query = $this->db->get();
	    $output .= '
	    <table class="table table-bordered table-striped" style="font-size:12px">
	      <tr>
	        <th>Trade</th>
	        <th>Rate</th>
	        <th>Required QTY</th>
	        <th>Available QTY</th>
	        <th>Required Amount</th>
	        <th>Available Amount</th>
	        <th>Market</th>
	        <th>Open</th>
	        <th>Complete QTY</th>
	        <th>Complete Amount</th>
	        <th>Trade Time</th>
	        <th>Status</th>
	      </tr>
	    ';

	  foreach($query->result() as $row)
	  {
	    $status = $row->status==0?"<p class='label-warning text-white text-center'>Canceled</p>":($row->status==1?"<p class='label-success text-white text-center'>Completed</p>":"<p class='label-primary text-white text-center'>Running</p>");

	    $output .= "<tr>
	      <td>".$row->bid_type."</td>
	      <td>".$row->bid_price."</td>
	      <td>".$row->bid_qty."</td>
	      <td>".$row->bid_qty_available."</td>
	      <td>".$row->total_amount."</td>
	      <td>".$row->amount_available."</td>
	      <td>".$row->market_symbol."</td>
	      <td>".$row->open_order."</td>
	      <td>".$row->complete_qty."</td>
	      <td>".$row->complete_amount."</td>
	      <td>".$row->success_time."</td>
	      <td>".$status."</td>
	    </tr>";
	  }
	    $output .= '</table>';
	    return $output;
  }
  
	public function userBalanceLog($user_id = null)
	{
		if ($user_id==null) {
			$user_id = $this->session->userdata('user_id');
		}

		return $this->db->select('*')
			->from('dbt_balance_log balancelog')
			->join('dbt_cryptocoin coin', 'balancelog.currency_symbol = coin.symbol')
			->where('user_id', $user_id)
			//->where('currency_symbol', $key)
			->order_by('transaction_type', 'desc')
			->get()
			->result();

	}
	function fetch_details($limit, $start)
	{
		$output = '';
		$this->db->select("*");
		$this->db->from("dbt_country");
		$this->db->order_by("name", "ASC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		$output .= '
		    <table class="table table-bordered">
		     <tr>
		      <th>Country ID</th>
		      <th>Country Name</th>
		     </tr>
		    ';
		foreach($query->result() as $row)
		{
		$output .= '
		<tr>
		<td>'.$row->id.'</td>
		<td>'.$row->name.'</td>
		</tr>
		';
		}
		$output .= '</table>';
		return $output;
	}

	public function checkUserAllBalance($user_id = null)
	{
		return $this->db->select('balance.*, coin.image, coin.symbol, coin.coin_name, user_coin_address.address')
			->from('dbt_balance balance')
			->join('dbt_cryptocoin coin', 'coin.symbol = balance.currency_symbol', 'left')
			->join('dbt_user user', 'user.user_id = balance.user_id', 'inner')
			->join('address user_coin_address', 'user.id = user_coin_address.user_id AND `coin`.`id` = `user_coin_address`.`coin_id`', 'left')
			->where('balance.user_id', $user_id)
			->get()
			->result();

	}
	

	public function singleUserVerifyDoc($user_id = null)
	{
		return $this->db->select('*')
			->from('dbt_user user')
			->join('dbt_user_verify_doc userdoc', 'userdoc.user_id=user.user_id', 'left')
			->where('userdoc.user_id', $user_id)
			->get()
			->row();
	}

	public function update($data = array())
	{
		return $this->db->where('user_id', $data["user_id"])
			->update("dbt_user", $data);
	}

	public function delete($user_id = null)
	{
		return $this->db->where('user_id', $user_id)
			->delete("dbt_user");
	}

	public function dropdown()
	{
		$data = $this->db->select("user_id, CONCAT_WS(' ', first_name, last_name) AS fullname")
			->from("dbt_user")
			->where('status', 1)
			->get()
			->result();
		$list[''] = display('select_option');
		if (!empty($data)) {
			foreach($data as $value)
				$list[$value->id] = $value->fullname;
			return $list;
		} else {
			return false; 
		}
	}

	/*
    |----------------------------------------------
    |   Datatable Ajax data Pagination+Search
    |----------------------------------------------     
    */
	var $table = 'dbt_user';
	var $column_order = array(null, 'user_id','first_name','last_name','username','email','phone','referral_id','language','country','city','address','ip'); //set column field database for datatable orderable
	var $column_search = array('user_id','first_name','last_name','username','email','phone','referral_id','language','country','city','address','ip'); //set column field database for datatable searchable 

	var $order = array('id' => 'asc'); // default order 

	private function _get_datatables_query()
	{
		$this->db->from($this->table);
		$i = 0;
		foreach ($this->column_search as $item) // loop column 
		{
			if(isset($_POST['search']) && $_POST['search']['value']) // if datatable send POST for search
			{
			
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if(isset($_POST['length']) && $_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	function get_user_list($query=null)
	{
		if(isset($query) && !empty($query)){
			$this->db->like('first_name', $query);
			$this->db->or_like('last_name', $query);
			$this->db->or_like('email', $query);
		}
		$this->db->group_by(array('email'));
		$this->db->order_by('id','desc');
		$query = $this->db->limit(200)->get('dbt_user');
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
 
	public function first_and_last_user_id()
	{
	    $this->db->select('(select id from dbt_user order by id asc limit 1) as start_id,(select id from dbt_user order by id desc limit 1) as last_id');
	    $this->db->from('dbt_user');
	    $query = $this->db->get()->row();
		return $query;
	}

	public function getUsers($limit, $offset)
	{
		return $this->db->select("
				dbt_user.*, 
				CONCAT_WS(' ', first_name, last_name) AS fullname 
			")
			->from('dbt_user')
			->order_by('id', 'asc')
			->limit($limit, $offset-1)
			->get()
			->result();
	}
	public function getPrevNotSentUsers($last_id=null)
	{
		return $this->db->select("
				dbt_user.*, 
				CONCAT_WS(' ', first_name, last_name) AS fullname 
			")
			->from('dbt_user')
			->join('web_news_cron_user_record', 'web_news_cron_user_record.user_id = dbt_user.id')
			->order_by('dbt_user.id', 'asc')
			->where('web_news_cron_user_record.is_sent','0')
			->where('dbt_user.id<=',$last_id)
			->get()
			->result();
	}
}
