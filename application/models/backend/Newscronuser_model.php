<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newscronuser_model extends CI_Model {
 
	public function create($data = array())
	{
		return $this->db->insert('web_news_cron_user_record', $data);
	}

	public function read($limit, $offset)
	{
		return $this->db->select("*")
			->from('web_news_cron_user_record')
			->order_by('id', 'asc')
			->limit($limit, $offset)
			->get()
			->result();
	}

	public function single($id = null)
	{
		return $this->db->select('*')
			->from('web_news_cron_user_record')
			->where('id', $id)
			->get()
			->row();
	}

	public function all()
	{
		return $this->db->select('*')
			->from('web_news_cron_user_record')
			->get()
			->result();
	}

	public function update($data = array())
	{
		return $this->db->where('id', $data["id"])
			->update("web_news_cron_user_record", $data);
	}

	public function search($data = null)
	{
		return $this->db->select('*')
			->from('web_news_cron_user_record')
			->where('user_id', $data['user_id'])
			->where('cron_news_id', $data['cron_news_id'])
			->get()
			->row();
	}

}