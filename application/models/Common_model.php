<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class common_model extends CI_Model {

	//Send email via SMTP server in CodeIgniter
	public function send_email($post=array()){

		//$sentmail = $this->send_email_phpmailer($post);
		$sentmail = $this->send_email_server($post);
		if($sentmail){
			return 1;
		} else{
			return 0;
		}
	
		//Load email library
		$this->load->library('email');

		$email = $this->db->select('*')->from('email_sms_gateway')->where('es_id', 2)->get()->row();

		//SMTP & mail configuration
		$config = array(
		    'protocol'  => $email->protocol,
		    'smtp_host' => $email->host,
		    'smtp_port' => $email->port,
		    'smtp_user' => $email->user,
		    'smtp_pass' => $email->password,
		    'mailtype'  => $email->mailtype,
		    'charset'   => $email->charset,
		    'newline'   => "\r\n"
		);
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		$htmlContent = $post['message'];

		$this->email->to($post['to']);
		$this->email->from($email->user ,$post['title']);
		$this->email->subject($post['subject']);
		$this->email->message($htmlContent);

		
		//Send email
		if($this->email->send()){
			return 1;

		} else{
			return 0;

		}
		
	}
	
	public function send_email_server($post=array()){

		//Load email library
		$this->load->library('email');

		$email = $this->db->select('*')->from('email_sms_gateway')->where('es_id', 2)->get()->row();

		//SMTP & mail configuration
		$config = array(
		    'protocol'  => "sendmail",
			'mailpath' => '/usr/sbin/sendmail',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE,
		    //'mailtype'  => $email->mailtype,
		    //'charset'   => $email->charset,
		    'newline'   => "\r\n"
		);
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		$htmlContent = $post['message'];

		$this->email->to($post['to']);
		//$this->email->from($email->user ,$post['title']);
		$this->email->from('verification@imba-exchange.co' ,$post['title']);
		$this->email->subject($post['subject']);
		$this->email->message($htmlContent);

		
		//Send email
		if($this->email->send()){
			return 1;

		} else{
			return 0;

		}
		
	}

	public function send_email_phpmailer($post=array()){
		
		// Instantiation and passing `true` enables exceptions
		$mail = new PHPMailer(true);
		$email = $this->db->select('*')->from('email_sms_gateway')->where('es_id', 2)->get()->row();
		
		try {
			//Server settings
			//$mail->SMTPDebug = 2;
			$mail->isSMTP();
			$mail->Host       = $email->host;
			$mail->SMTPAuth   = true;
			$mail->Username   = $email->user;
			$mail->Password   = $email->password;
			$mail->SMTPSecure = 'ssl';
			$mail->Port       = $email->port;
			$mail->CharSet	  = $email->charset;
			/* $mail->SMTPOptions = array (
				'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true)
			); */

			//Recipients
			$mail->setFrom($email->user ,$post['title']);
			$mail->addAddress($post['to']);
			$mail->addReplyTo($email->user ,$post['title']);
			
			// Content
			$mail->isHTML(true);
			$mail->Subject = $post['subject'];
			$mail->Body    = $post['message'];
			$mail->AltBody = strip_tags($post['message']);
			$mail->DebugOutput = function ($str, $level) {
			  file_put_contents(
				'/var/www/html/application/logs/phpmaillog',
				date('Y-m-d H:i:s') . "\t" . $str,
				FILE_APPEND | LOCK_EX
			  );
			};
			if($mail->send()){
				//echo 'Message has been sent';
				return true;
			} else{
				return false;
			}
		} catch (Exception $e) {
			//echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
			return false;
		}die();
	}
	
	public function email_sms($method)
	{
        
	   return $this->db->select('*')
       ->from('sms_email_send_setup')
       ->where('method',$method)
       ->get()
       ->row();

	}


	public function get_setting(){
		return $settings = $this->db->select("email,phone,time_zone,title")
    		->get('setting')
    		->row();
	}

	public function getFees($table,$id)
	{
		return $this->db->select('*')
		->from($table)
		->where($table.'_id',$id)
		->get()
		->row();
	}


	public function payment_gateway()
	{
		return $this->db->select('*')
		->from('payment_gateway')
		->where('status', 1)
		->get()
		->result();
	}


	public function getCoinList()
	{
		return $this->db->select('*')
			->from('dbt_cryptocoin')
			->where('status', 1)
			->get()->result();
	}

	public function getSiteDetails()
	{
		return $this->db->select("*")
				->get('setting')
				->row();
	}

}