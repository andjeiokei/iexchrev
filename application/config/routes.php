<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] 	= 'home';

$route['admin'] 				= 'backend/dashboard/auth/login';
$route['logout'] 				= 'backend/dashboard/auth/logout';

/*********************************
*  WEBSITE ROUETS
*********************************/
$route['news'] 						= 'home/news';
$route['news/(:any)'] 				= 'home/news/$1';
$route['news/(:any)/(:any)'] 		= 'home/news/$1/$2';

$route['exchange'] 					= 'home/exchange';
$route['contact'] 					= 'home/contact';

$route['balances'] 					= 'home/balances';
$route['paymentform'] 				= 'home/paymentform';
$route['payment-process'] 			= 'home/payment_process';

// $route['deposit'] 					= 'home/deposit';
// $route['deposit/(:any)'] 			= 'home/deposit/$1';
// $route['withdraw'] 					= 'home/withdraw';
// $route['withdraw/(:any)'] 			= 'home/withdraw/$1';
// $route['withdraw-confirm/(:any)']	= 'home/withdraw_confirm/$1';
// $route['withdraw-details/(:any)']	= 'home/withdraw_details/$1';
// $route['transfer'] 					= 'home/transfer';
// $route['transfer/(:any)'] 		= 'home/transfer/$1';
// $route['transfer-confirm/(:any)']	= 'home/transfer_confirm/$1';
// $route['transfer-details/(:any)']	= 'home/transfer_details/$1';
$route['transactions'] 				= 'home/transactions';

$route['register'] 					= 'home/register';
$route['resetPassword'] 			= 'home/resetPassword';
$route['forgotPassword'] 			= 'home/forgotPassword';
$route['login'] 					= 'home/login';
$route['login-verify'] 				= 'home/login_verify';
$route['profile'] 					= 'home/profile';
$route['profile2'] 					= 'home/profile2';
$route['edit-profile'] 				= 'home/edit_profile';
$route['change-password'] 			= 'home/change_password';
$route['profile-verify'] 			= 'home/profile_verify';
$route['googleauth'] 				= 'home/googleauth';

$route['bank-setting'] 				= 'home/bank_setting';
$route['bank-setting/(:any)'] 		= 'home/bank_setting/$1';
$route['payout-setting'] 			= 'home/payout_setting';
$route['payout-setting/(:any)'] 	= 'home/payout_setting/$1';
$route['activate-account/(:any)'] 	= 'home/activate_account/$1';

$route['open-order'] 				= 'home/open_order';
$route['order-cancel/(:any)'] 		= 'home/order_cancel/$1';
$route['complete-order'] 			= 'home/complete_order';
$route['trade-history'] 			= 'home/trade_history';
$route['trade-history/(:num)'] 		= 'home/trade_history/$1';
//$route['gourl/lib/cryptobox.callback.php'] 			= 'cryptobox.callback.php';

//$route['(:any)'] 					= 'home/page';



$route['404_override'] 				= '';
$route['translate_uri_dashes'] 		= FALSE;

// Our Edits

$route['rpccheck'] 					= 'crypto_rpc/index';
$route['deposit'] 					= 'deposit/deposit';
$route['deposit/(:any)'] 			= 'deposit/deposit/$1';
$route['getAddress']                = 'deposit/getAddress';


$route['withdraw'] 					        = 'withdraw';
$route['withdraw/confirm/(:any)'] 	        = 'withdraw/confirm/$1';
$route['withdraw/submit_withdraw_data'] 	= 'withdraw/submit_withdraw_data';
$route['request_action'] 			        = 'home/request_action';
$route['withdraw/(:any)'] 			        = 'withdraw/index/$1';

$route['depositupdate']                     = 'crypto_rpc/depositupdate';
$route['btccheck']                     = 'crypto_rpc/btccheck';
$route['withdrawupdate/(:any)']             = 'crypto_rpc/withdrawupdate/$1';
$route['withdrawupdate']                    = 'crypto_rpc/withdrawupdate';
$route['order_history_csv']                 = 'home/order_history_csv';
