<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include(APPPATH .'libraries/JsonrpcClient/JsonrpcClient.php');

class Arena extends CI_Controller 
{

    var $settings;

    public function __construct()
    {
        parent::__construct(); 
        if (!($this->session->userdata('isLogIn') && $this->session->userdata('user_id')))
            redirect('login');

            $this->load->model(array(
                'common_model',
                'website/web_model',
                'backend/user/user_model'
            ));

        $this->settings = $this->common_model->getSiteDetails();

        $this->load->library('payment');

        date_default_timezone_set($this->settings->time_zone);

        //Language setting
        //$globdata['lang']        = $this->langSet();
        
        $globdata['web_language']= $this->web_model->webLanguage();
        $globdata['social_link'] = $this->web_model->socialLink();
        $globdata['category']    = $this->web_model->categoryList();

        $this->load->vars($globdata);

    }

    public function index($coin = null){
        $data = array();

        $data['settings'] = $this->settings;
        
        $user_data = $this->user_model->single($this->session->userdata('user_id'));

        // if(!empty($user_data))
        // {
        //     $postdata =  array(
        //         "email"         => $user_data->email,
        //         "password"      => $user_data->password,
        //         "user_id"       => $user_data->user_id
        //     );

        //     $make_call = callAPI('POST', ARENAAPI.'arena/settoken/', json_encode($postdata));
        //     $response = json_decode($make_call, true);
        //     if(!empty($response))
        //     {
        //         $cookie= array(
        //             'name'   => 'tnhp',
        //             'value'  => $response['token'],
        //             'expire' => '3600',
        //             'path'   => '/arena',
        //             //'domain' => base_url(),
        //             //'secure' => TRUE,
        //         );
        //         $this->input->set_cookie($cookie);
         
        //     }
            
        // }
        // else {
        //     redirect('login');
        // }

        

        //Theme Settings
        $theme['theme']          = $this->db->select('*')->from('dbt_theme')->where('status', 1)->get()->row();
        $data['theme_settings']  = $this->load->view("website/theme_scriptcss", $theme, true);

        $this->load->view('website/header', $data);     
        $this->load->view('arenagame/home', $data);
        $this->load->view('website/footer', $data);
    }

    public function pong(){
        $this->load->view('website/header', $data);     

        $this->load->view('website/footer', $data);
    }

}