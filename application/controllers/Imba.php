<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imba extends CI_Controller {

    var $settings;

    public function __construct()
    {
        parent::__construct(); 

        $this->load->model(array(
            'common_model',
            'website/web_model',
            'backend/user/user_model'
        ));

        $this->settings = $this->common_model->getSiteDetails();

        $this->load->library('payment');

        date_default_timezone_set($this->settings->time_zone);

        //Language setting
        //$globdata['lang']        = $this->langSet();
        
        $globdata['web_language']= $this->web_model->webLanguage();
        $globdata['social_link'] = $this->web_model->socialLink();
        $globdata['category']    = $this->web_model->categoryList();

        $this->load->vars($globdata);

    }

    public function index(){
        $data = array();

        $data['settings'] = $this->settings;
        $theme['theme']          = $this->db->select('*')->from('dbt_theme')->where('status', 1)->get()->row();
        $data['theme_settings']  = $this->load->view("website/theme_scriptcss", $theme, true);

        $this->load->view('website/header', $data);
        $this->load->view('imba/home');
        $this->load->view('website/footer', $data);
    }
}