<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		
		if (!$this->session->userdata('isAdmin')) 
        redirect('logout');
  
        if (!$this->session->userdata('isLogIn')) 
        redirect('admin'); 
 
		$this->load->model(array(
            'backend/dashboard/home_model',
			'backend/dashboard/dashboard_model',
		));
	}

    public function index()
    {     
        $data['title']   = display('home'); 

        $data['total_user'] = $this->home_model->userList()->num_rows();
        $data['total_market'] = $this->home_model->coinpairList()->num_rows();
        $data['total_trade'] = $this->db->count_all('dbt_biding_log');
        $data['withdraw'] = $this->home_model->pendingWithdrawList();
        $data['trade_history'] = $this->home_model->marketTradeHistory();
        $data['coins'] = $this->home_model->coinList();
        $data['monthly_deposit'] = $this->home_model->monthlyDeposit();
        $data['monthly_withdraw'] = $this->home_model->monthlyWithdraw();
        $data['monthly_transfer'] = $this->home_model->monthlyTransfer();
        $data['monthly_fees'] = $this->home_model->monthlyFees();
        $data['coin_market'] = $this->home_model->coinTradeMarket();

        $data['content'] = $this->load->view('backend/dashboard/home', $data, true);
        $this->load->view('backend/layout/main_wrapper', $data);  
    }


    public function monthly_deposit(){


        $data['monthly_deposit'] = $this->home_model->monthlyDeposit($this->input->post('symbol'));
        $data['monthly_withdraw'] = $this->home_model->monthlyWithdraw($this->input->post('symbol'));
        $data['monthly_transfer'] = $this->home_model->monthlyTransfer($this->input->post('symbol'));
        $this->load->view('backend/dashboard/chart_script', $data);  


    }

    public function monthly_fees(){

        $data['monthly_fees'] = $this->home_model->monthlyFees($this->input->post('symbol'));
        $this->load->view('backend/dashboard/bar_script', $data); 

    }

    public function total_referral_value()
    {
        $this->db->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
        $referral_value = $this->db->query("SELECT `currency_symbol`, SUM(`transaction_amount`) as transaction_amount FROM dbt_balance_log WHERE `transaction_type`='REFERRAL' GROUP BY `currency_symbol` ORDER BY `dbt_balance_log`.`currency_symbol` ASC")->result();

       echo json_encode(array('referral_value' => $referral_value));

    }

    public function all_fees_value()
    {
        
        $result = array();
        $results = array();

        $this->db->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
        $all_fees_balance = $this->db->query("SELECT `currency_symbol`, SUM(`transaction_fees`) as fees_amount FROM dbt_balance_log WHERE `transaction_type`!='REFERRAL' GROUP BY `currency_symbol` ORDER BY `dbt_balance_log`.`currency_symbol` ASC")->result();

        $this->db->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
        $all_fees_trade = $this->db->query("SELECT IF(`bid_type`='BUY', SUBSTRING_INDEX(`market_symbol`,'_',-1), `currency_symbol`) as currency_symbol, SUBSTRING_INDEX(`market_symbol`,'_',-1) as currency_symbol2, SUM(`fees_amount`) as fees_amount FROM dbt_biding WHERE `status`=1 GROUP BY IF(`bid_type`='BUY', `currency_symbol2`, `currency_symbol`) ORDER BY `dbt_biding`.`currency_symbol` ASC")->result();

        $results = array_merge($all_fees_balance, $all_fees_trade);

        foreach ($results as $key => $value) {
            $sum_value =0;
            foreach ($results as $keyt => $valuet) {
                if ($valuet->currency_symbol == $value->currency_symbol) {
                    $sum_value += $valuet->fees_amount;
                }
            }
            
            array_push($result, 
                array(
                    'currency_symbol' => $value->currency_symbol,
                    'total_qty'       => $sum_value,
                )
            );
        }

        echo json_encode(array('fees_value' => $result));

    }

    public function profile()
    {
        $data['title'] = display('profile'); 
        $data['user']  = $this->home_model->profile($this->session->userdata('id'));
        $data['content'] = $this->load->view('backend/dashboard/profile', $data, true);
        $this->load->view('backend/layout/main_wrapper', $data);  
    }

    public function edit_profile()
    { 
        $data['title']    = display('edit_profile');
        $id = $this->session->userdata('id');
        $this->form_validation->set_rules('firstname', 'First Name','required|max_length[50]');
        $this->form_validation->set_rules('lastname', 'Last Name','required|max_length[50]');
        $this->form_validation->set_rules('email', 'Email Address', "required|valid_email|max_length[100]");
        $this->form_validation->set_rules('password', 'Password','required|max_length[32]|md5');
        $this->form_validation->set_rules('about', 'About','max_length[1000]');

        //set config 
        $config = [
            'upload_path'   => 'upload/settings/',
            'allowed_types' => 'gif|jpg|png|jpeg', 
            'overwrite'     => false,
            'maintain_ratio' => true,
            'encrypt_name'  => true,
            'remove_spaces' => true,
            'file_ext_tolower' => true 
        ]; 
        $this->load->library('upload', $config);
 
        if ($this->upload->do_upload('image')) {  
            $data = $this->upload->data();  
            $image = $config['upload_path'].$data['file_name']; 

            $config['image_library']  = 'gd2';
            $config['source_image']   = $image;
            $config['create_thumb']   = false;
            $config['encrypt_name']   = TRUE;
            $config['width']          = 115;
            $config['height']         = 90;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $this->session->set_flashdata('message', display("image_upload_successfully"));
        }

        $data['user'] = (object)$userData = array(
            'id'          => $this->input->post('id'),
            'firstname'   => $this->input->post('firstname'),
            'lastname'    => $this->input->post('lastname'),
            'email'       => $this->input->post('email'),
            'password'    => md5($this->input->post('password')),
            'about'       => $this->input->post('about',true),
            'image'       => (!empty($image)?$image:$this->input->post('old_image')) 
        );

        /*-----------------------------------*/
        if ($this->form_validation->run()) {

            if (empty($userData['image'])) {
                $this->session->set_flashdata('exception', $this->upload->display_errors()); 
            }

            if ($this->home_model->update_profile($userData)) 
            {
                $this->session->set_userdata(array(
                    'fullname'   => $this->input->post('firstname'). ' ' .$this->input->post('lastname'),
                    'email'       => $this->input->post('email'),
                    'image'       => (!empty($image)?$image:$this->input->post('old_image'))
                ));

                $this->session->set_flashdata('message', display('update_successfully'));

            } else {
                $this->session->set_flashdata('exception',  display('please_try_again'));

            }
            redirect("backend/dashboard/home/edit_profile");

        } else { 
            $data['user']   = $this->home_model->profile($id);
            $data['content'] = $this->load->view('backend/dashboard/edit_profile', $data, true);
            $this->load->view('backend/layout/main_wrapper', $data);

        }

    }
    
}