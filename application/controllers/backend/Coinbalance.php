<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coinbalance extends CI_Controller {
 	
 	public function __construct()
 	{
 		parent::__construct();
 		$this->load->model(array(
 			'backend/coinbalance_model'  
 		));
 		
 		if (!$this->session->userdata('isAdmin')) 
                redirect('logout');
 		
		if (!$this->session->userdata('isLogin') 
			&& !$this->session->userdata('isAdmin'))
			redirect('admin');
 	}
 
	public function index($page_number = NULL)
	{
        $data['title']  = "Coin Balance";		
        $data['coins'] = $this->db->where('status', 1)->get('dbt_cryptocoin')->result();
		$data['content'] = $this->load->view("backend/coinbalance/list", $data, true);
		$this->load->view("backend/layout/main_wrapper", $data);
    }
    public function ajax_fetch_daemon()
	{
        $list = $this->db->where('status', 1)->get('dbt_cryptocoin')->result();
        // print_r($list);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $cryptocoin) {

			$status = "Unknown";
			switch ($cryptocoin->status) {
				case 0:
					$status = "Inactive";
					break;
				
				case 1:
					$status = "Active";
					break;
				
				case 2:
					$status = "Deleted";
					break;

				default:
					# code...
					break;
			}

			$no++;
			$row = array();
            $row[] = '<img src="'.base_url("$cryptocoin->image").'" width="18px" /> '.$cryptocoin->full_name;

            // All User Available Balance
            $AllUserABalance = $this->coinbalance_model->checkAllUserAvailableBalance($cryptocoin->symbol);
            $row[] = $AllUserABalance;

            // All User InOrder Balance
            $AllUserIOBalance = $this->coinbalance_model->checkAllUserInOrderBalance($cryptocoin->symbol);
            $row[] = $AllUserIOBalance;

            // Hotwallet Balance(Available + InOrder)
            $HotBal = $AllUserABalance + $AllUserIOBalance;
			$row[] = $HotBal;
            // CW
            $DaemonBal = $this->coinbalance_model->checkDaemonBalanceByCoin($cryptocoin->symbol);
			$row[] = $DaemonBal;
			$row[] = $DaemonBal - $HotBal;
            $row[] = $status;
			#$row[] = '<a href="" onclick="updateFeesModal('.$cryptocoin->symbol.'\')" data-toggle="modal" data-target="#editFees" class="btn btn-info btn-xs" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a> 
			#<a href="" onclick="updateDeleteModal(\''.$cryptocoin->symbol.'\')" data-toggle="modal" data-target="#list_remove" class="btn btn-danger btn-xs remove_js" data-toggle="tooltip" data-placement="left" title="Remove" data-url=""><i class="fa fa-times" aria-hidden="true"></i></a>';

			$data[] = $row;
		}

		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->coinbalance_model->count_all_2nd(),
				"recordsFiltered" => $this->coinbalance_model->count_filtered(),
				"data" => $data,
			);
		//output to json format
		echo json_encode($output);
    }

    function fixhotwallet(){
        $data = $this->input->post();
        $coin = $this->db->where('symbol', $data['coin'])->get('dbt_cryptocoin')->result();
        if($coin){
            $this->db->where('currency_symbol', $data['coin']);
            $this->db->delete('dbt_fees');
            echo true;
        }
    }

}