<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feessetting extends CI_Controller {
 	
 	public function __construct()
 	{
 		parent::__construct();
 		$this->load->model(array(
 			'backend/feessetting_model'  
 		));
 		
 		if (!$this->session->userdata('isAdmin')) 
                redirect('logout');
 		
		if (!$this->session->userdata('isLogin') 
			&& !$this->session->userdata('isAdmin'))
			redirect('admin');
 	}
 
	public function index($page_number = NULL)
	{
        $data['title']  = display('fees_setting');		
        $data['coins'] = $this->db->where('status', 1)->get('dbt_cryptocoin')->result();
		$data['content'] = $this->load->view("backend/feessetting/list", $data, true);
		$this->load->view("backend/layout/main_wrapper", $data);
    }
    public function ajax_list()
	{
        $list = $this->db->where('status', 1)->get('dbt_cryptocoin')->result();
        // print_r($list);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $cryptocoin) {

			$status = "Unknown";
			switch ($cryptocoin->status) {
				case 0:
					$status = "Inactive";
					break;
				
				case 1:
					$status = "Active";
					break;
				
				case 2:
					$status = "Deleted";
					break;

				default:
					# code...
					break;
			}

			$no++;
			$row = array();
            $row[] = '<img src="'.base_url("$cryptocoin->image").'" width="18px" /> '.$cryptocoin->full_name;
            // Minimum Withdraw
            $withdraw = $this->db->where('level', 'WITHDRAW')->where('currency_symbol', $cryptocoin->symbol)->get('dbt_fees')->result();
            if($withdraw){
                $minWithdraw = $withdraw[0]->minimum_amount;
                $withdrawFees = $withdraw[0]->fees;
            } else {
                $minWithdraw = 0;
                $withdrawFees = 0;
            }
            $row[] = $minWithdraw;
            // Withdraw Fee
            $row[] = $withdrawFees;
            // Buy
            $buy = $this->db->where('level', 'BUY')->where('currency_symbol', $cryptocoin->symbol)->get('dbt_fees')->result();
            if($buy){
                $buyFees = $buy[0]->fees;
            } else {
                $buyFees = 0;
            }
			$row[] = $buyFees;
            // Sell
            $sell = $this->db->where('level', 'SELL')->where('currency_symbol', $cryptocoin->symbol)->get('dbt_fees')->result();
            if($sell){
                $sellFees = $sell[0]->fees;
            } else {
                $sellFees = 0;
            }
            $row[] = $sellFees;
			$row[] = $status;
			$row[] = '<a href="" onclick="updateFeesModal('.$minWithdraw.', '.$withdrawFees.', '.$buyFees.', '.$sellFees.',\''.$cryptocoin->symbol.'\')" data-toggle="modal" data-target="#editFees" class="btn btn-info btn-xs" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a> 
			<a href="" onclick="updateDeleteModal(\''.$cryptocoin->symbol.'\')" data-toggle="modal" data-target="#list_remove" class="btn btn-danger btn-xs remove_js" data-toggle="tooltip" data-placement="left" title="Remove" data-url=""><i class="fa fa-times" aria-hidden="true"></i></a>';

			$data[] = $row;
		}

		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->feessetting_model->count_all_2nd(),
				"recordsFiltered" => $this->feessetting_model->count_filtered(),
				"data" => $data,
			);
		//output to json format
		echo json_encode($output);
    }
    
    function saveFees(){
        // echo json_encode($this->input->post());
        $data = $this->input->post();

        $coin = $this->db->where('symbol', $data['coin'])->get('dbt_cryptocoin')->result();
        if($coin){

            $withdraw = $this->db->where('currency_symbol', $data['coin'])->where('level', 'WITHDRAW')->get('dbt_fees')->result();
            if($withdraw){
                // Update
                $this->db->where('id', $withdraw[0]->id);
                $this->db->update('dbt_fees', ['fees' => $data['withdrawFees'], 'minimum_amount' => $data['minWithdraw']]);
            } else {
                // Insert
                $this->db->insert('dbt_fees', ['level' => 'WITHDRAW', 'fees' => $data['withdrawFees'], 'currency_id' => 0, 'currency_symbol' => $data['coin'], 'minimum_amount' => $data['minWithdraw']]);
            }

            $buy = $this->db->where('currency_symbol', $data['coin'])->where('level', 'BUY')->get('dbt_fees')->result();
            if($buy){
                // Update
                $this->db->where('id', $buy[0]->id);
                $this->db->update('dbt_fees', ['fees' => $data['buyFees']]);
            } else {
                // Insert
                $this->db->insert('dbt_fees', ['level' => 'BUY', 'fees' => $data['buyFees'], 'currency_id' => 0, 'currency_symbol' => $data['coin'], 'minimum_amount' => null]);
            }

            $sell = $this->db->where('currency_symbol', $data['coin'])->where('level', 'SELL')->get('dbt_fees')->result();
            if($sell){
                // Update
                $this->db->where('id', $sell[0]->id);
                $this->db->update('dbt_fees', ['fees' => $data['sellFees']]);
            } else {
                // Insert
                $this->db->insert('dbt_fees', ['level' => 'SELL', 'fees' => $data['sellFees'], 'currency_id' => 0, 'currency_symbol' => $data['coin'], 'minimum_amount' => null]);
            }

            echo true;

        } else {
            echo false;
        }
    
    }

    function deleteFees(){
        $data = $this->input->post();
        $coin = $this->db->where('symbol', $data['coin'])->get('dbt_cryptocoin')->result();
        if($coin){
            $this->db->where('currency_symbol', $data['coin']);
            $this->db->delete('dbt_fees');
            echo true;
        }
    }

}