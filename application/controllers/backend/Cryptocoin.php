<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cryptocoin extends CI_Controller {
 	
 	public function __construct()
 	{
 		parent::__construct();
 		$this->load->model(array(
 			'backend/cryptocoin_model'  
 		));
		 $this->load->library('encryption');
 		if (!$this->session->userdata('isAdmin')) 
        redirect('logout');
 		
		if (!$this->session->userdata('isLogin') 
			&& !$this->session->userdata('isAdmin'))
			redirect('admin');
 	}
 
	public function index()
	{  
		$data['title']  = display('cryptocurrency');		

		$data['content'] = $this->load->view("backend/cryptocoin/list", $data, true);
		$this->load->view("backend/layout/main_wrapper", $data);
	}

	public function ajax_list()
	{
		$list = $this->cryptocoin_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $cryptocoin) {

			$status = "Unknown";
			switch ($cryptocoin->status) {
				case 0:
					$status = "Inactive";
					break;
				
				case 1:
					$status = "Active";
					break;
				
				case 2:
					$status = "Deleted";
					break;

				default:
					# code...
					break;
			}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = '<img src="'.base_url("$cryptocoin->image").'" width="50px" />';
			$row[] = $cryptocoin->coin_name;
			$row[] = $cryptocoin->full_name;
			$row[] = $cryptocoin->symbol;
			$row[] = (($cryptocoin->show_home == 1)?'Yes':'No')."/ ".$cryptocoin->coin_position;
			$row[] = $cryptocoin->rank;
			$row[] = $status;
			$row[] = '<a href="'.base_url("backend/cryptocoin/form/$cryptocoin->id").'"'.' class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="left" title="Update"><i class="fa fa-pencil" aria-hidden="true"></i></a> 
			<a href="javascript:void(0);" class="btn btn-danger btn-sm remove_js" data-toggle="tooltip" data-placement="left" title="Remove" data-url="'.base_url("backend/cryptocoin/remove/".$cryptocoin->cid."/").'"><i class="fa fa-times" aria-hidden="true"></i></a>';

			$data[] = $row;
		}

		$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->cryptocoin_model->count_all_2nd(),
				"recordsFiltered" => $this->cryptocoin_model->count_filtered(),
				"data" => $data,
			);
		//output to json format
		echo json_encode($output);
	}


    public function coin_check($coin_name, $id)
    { 
        $coinExists = $this->db->select('*')
            ->where('coin_name',$coin_name) 
            ->where_not_in('id',$id) 
            ->get('dbt_cryptocoin')
            ->num_rows();

        if ($coinExists > 0) {
            $this->form_validation->set_message('coin_check', 'The {field} is already registered.');
            return false;

        } else {
            return true;

        }
    }

    public function coinsym_check($symbol, $id)
    { 
        $coinExists = $this->db->select('*')
            ->where('symbol',$symbol) 
            ->where_not_in('id',$id) 
            ->get('dbt_cryptocoin')
            ->num_rows();

        if ($coinExists > 0) {
            $this->form_validation->set_message('coinsym_check', 'The {field} is already registered.');
            return false;

        } else {
            return true;

        }
    } 

 
	public function form($id = null)
	{ 
		$this->encryption->initialize(
			array(
					'cipher' => 'aes-256',
					'mode' => 'ctr',
					'key' => $this->config->item('enc_key_rpc_password'),
			)
		);
		$data['title']  = display('cryptocurrency');


		if (!empty($id)) {
       		$this->form_validation->set_rules('coin_name', "Coin Name","max_length[100]|required|trim|callback_coin_check[$id]");
       		$this->form_validation->set_rules('symbol', "symbol","max_length[10]|required|trim|callback_coinsym_check[$id]");

		} else {
			$this->form_validation->set_rules('coin_name', 'Coin Name','required|is_unique[dbt_cryptocoin.coin_name]|max_length[100]|trim');
			$this->form_validation->set_rules('symbol', 'symbol','required|is_unique[dbt_cryptocoin.symbol]|max_length[10]|trim');
		}

		
		$this->form_validation->set_rules('full_name', "full_name",'max_length[100]|required|trim');
		$this->form_validation->set_rules('status', display('status'),'max_length[1]|required|trim');
		$this->form_validation->set_rules('cid', 'Coin Id','required|trim|integer');


		//Set Upload File Config 
        $config = [
            'upload_path'   	=> 'upload/coinlist/',
            'allowed_types' 	=> 'gif|jpg|png|jpeg', 
            'overwrite'     	=> false,
            'maintain_ratio' 	=> true,
            'encrypt_name'  	=> true,
            'remove_spaces' 	=> true,
            'file_ext_tolower' 	=> true 
        ]; 
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('image')) {  
            $data = $this->upload->data();  
            $image = $config['upload_path'].$data['file_name'];

        }

		$data['cryptocoin'] = (object)$userdata = array(
			'id'      				=> $this->input->post('id'),
			'cid'      				=> $this->input->post('cid'),
			'image'      			=> (!empty($image)?$image:$this->input->post('image_old')),
			'symbol'      			=> $this->input->post('symbol'),
			'coin_name'      		=> $this->input->post('coin_name'),
			'full_name'      		=> $this->input->post('full_name'),
			'algorithm'      		=> $this->input->post('algorithm'),			
			'rank'      			=> $this->input->post('rank'),
			'show_home'      		=> $this->input->post('show_home'),
			'coin_position'      	=> $this->input->post('coin_position'),
			'status'      			=> $this->input->post('status'),
			'isntp1'      			=> $this->input->post('isntp1'),
			'npt1tokenid'      		=> $this->input->post('npt1tokenid'),
			'host'					=> $this->input->post('host'),
			'port'					=> $this->input->post('port'),
			'username'				=> $this->input->post('username'),
			'password'				=> $this->input->post('password'),
			'passphrase'			=> $this->input->post('passphrase'),
		);


		if ($this->form_validation->run()) 
		{
			$userdata['password'] = $this->encryption->encrypt($userdata['password']);
			$userdata['passphrase'] = $this->encryption->encrypt($userdata['passphrase']);

			// echo "<pre>";print_r($userdata);exit;

			if (empty($id)) 
			{
				if ($this->cryptocoin_model->create($userdata)) {
					$this->session->set_flashdata('message', display('save_successfully'));

				} else {
					$this->session->set_flashdata('exception', display('please_try_again'));

				}
				redirect("backend/cryptocoin/form/");

			} 
			else 
			{
				unset($userdata['password']);
				unset($userdata['passphrase']);
				unset($userdata['username']);
				unset($userdata['port']);
				unset($userdata['host']);

				if ($this->cryptocoin_model->update($userdata)) {
					$this->session->set_flashdata('message', display('update_successfully'));

				} else {
					$this->session->set_flashdata('exception', display('please_try_again'));

				}
				redirect("backend/cryptocoin/form/$id");

			}
		} 
		else 
		{ 	
			$data['mode'] = 0;
			if(!empty($id)) {
				$data['mode'] = 1;

				$data['title'] = display('edit_coin');
				$coin   = $this->cryptocoin_model->single($id);

				$coin->password = $this->encryption->decrypt($coin->password);
				$coin->passphrase = $this->encryption->decrypt($coin->passphrase);

				$data['cryptocoin'] = $coin;

			}
			$data['content'] = $this->load->view("backend/cryptocoin/form", $data, true);
			$this->load->view("backend/layout/main_wrapper", $data);

		}

	}

    function remove($id = NULL)
    {
        if(!empty($id))
        {
            // status 2 for soft delete of the record
            $res = $this->cryptocoin_model->update(array('status'=>2, "cid"=>$id));

            if ( $res ) {
                $this->session->set_flashdata('success_message', 'Record has been removed.');
            } else {
                $this->session->set_flashdata('error_message', 'Failed to remove the record.');
            }
            redirect(base_url('backend/cryptocoin'));
        }
        else{
            redirect(base_url('backend/cryptocoin'));
        }
        
    }

}
