<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
 	
 	public function __construct()
 	{
 		parent::__construct();

 		if (!$this->session->userdata('isAdmin')) 
        	redirect('logout');

		if (!$this->session->userdata('isLogin') 
			&& !$this->session->userdata('isAdmin'))
			redirect('admin');

 		$this->load->model(array(
 			'backend/cms/news_model',
 			'backend/cms/language_model',
			'backend/user/user_model',
			'backend/newscron_model',
			'backend/newscronuser_model',
			'common_model',

 		));

 	}
 
	public function index()
	{  
		$data['title']  = display('news_list');

		$data['web_language'] = $this->language_model->single('1');
 		
 		/******************************
        * Pagination Start
        ******************************/
        $config["base_url"] = base_url('backend/cms/news/index');
        $config["total_rows"] = $this->db->count_all('web_news');
        $config["per_page"] = 25;
        $config["uri_segment"] = 5;
        $config["last_link"] = "Last"; 
        $config["first_link"] = "First"; 
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';  
        $config['full_tag_open'] = "<ul class='pagination col-xs pull-right'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        /* ends of bootstrap */
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data['article'] = $this->news_model->read($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        /******************************
        * Pagination ends
        ******************************/

		$list = $this->user_model->get_user_list(null);
		// print_r($list );die;
        $data["users"] = $list;

		$data['content'] = $this->load->view("backend/news/list", $data, true);
		$this->load->view("backend/layout/main_wrapper", $data);
	}

	public function slug_check($headline_en, $article_id)
    { 
        $packageExists = $this->db->select('*')
            ->where('headline_en',$headline_en) 
            ->where_not_in('article_id',$article_id) 
            ->get('web_news')
            ->num_rows();

        if ($packageExists > 0) {
            $this->form_validation->set_message('headline_en', 'The {field} is already registered.');
            return false;
        } else {
            return true;
        }

    }

 
	public function form($article_id = null)
	{ 
		$data['title']  = display('post_news');

		$data['web_language'] = $this->language_model->single('1');

		//Set Rules From validation
		if (!empty($article_id)) {   
       		$this->form_validation->set_rules('headline_en', display("headline_en"), "required|max_length[255]|callback_slug_check[$article_id]"); 

		} else {
			$this->form_validation->set_rules('headline_en', display('headline_en'),'required|is_unique[web_news.headline_en]|max_length[255]');
			
		}
		
		$this->form_validation->set_rules('cat_id', display('category'),'required|max_length[10]');

		$slug = url_title(strip_tags($this->input->post('headline_en')), 'dash', TRUE);

		//Set Upload File Config
        $config = [
            'upload_path'   	=> 'upload/news/',
            'allowed_types' 	=> 'gif|jpg|png|jpeg', 
            'overwrite'     	=> false,
            'maintain_ratio' 	=> true,
            'encrypt_name'  	=> true,
            'remove_spaces' 	=> true,
            'file_ext_tolower' 	=> true 
        ]; 
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('article_image')) {  
            $data = $this->upload->data();  
            $image = $config['upload_path'].$data['file_name'];

        }

		$data['article']   = (object)$userdata = array(
			'article_id'      	=> $this->input->post('article_id'),
			'slug'      		=> $slug,
			'headline_en'   	=> $this->input->post('headline_en'),
			'headline_fr' 		=> $this->input->post('headline_fr'), 
			'article_image'  	=> (!empty($image)?$image:$this->input->post('article_image_old')), 
			'article1_en' 		=> $this->input->post('article1_en'),
			'article1_fr' 		=> $this->input->post('article1_fr'),
			'cat_id' 			=> $this->input->post('cat_id'),
			'publish_date' 		=> date("Y-m-d h:i:sa"),
			'publish_by' 		=> $this->session->userdata('email'),
		);

		//From Validation Check
		if ($this->form_validation->run()) 
		{

			if (empty($article_id)) 
			{
				if ($this->news_model->create($userdata)) {
					$this->session->set_flashdata('message', display('save_successfully'));

				} else {
					$this->session->set_flashdata('exception', display('please_try_again'));

				}
				redirect("backend/cms/news/form/");

			} 
			else 
			{
				if ($this->news_model->update($userdata)) {
					$this->session->set_flashdata('message', display('update_successfully'));

				} else {
					$this->session->set_flashdata('exception', display('please_try_again'));

				}
				redirect("backend/cms/news/form/$article_id");

			}

		} 
		else
		{ 
			$parent_cat = $this->db->select("cat_id")->from('web_category')->where('slug', 'news')->get()->row();
			$child_cat = $this->db->select("*")->from('web_category')->where('parent_id', $parent_cat->cat_id)->get()->result();

			$data['child_cat'] = $child_cat;
			if(!empty($article_id)) {
				$data['title'] = display('edit_news');
				$data['article']   = $this->news_model->single($article_id);

			}
		}
		
		$data['content'] = $this->load->view("backend/news/form", $data, true);
		$this->load->view("backend/layout/main_wrapper", $data);

	}


	public function delete($article_id = null)
	{  
		if ($this->news_model->delete($article_id)) {
			$this->session->set_flashdata('message', display('delete_successfully'));

		} else {
			$this->session->set_flashdata('exception', display('please_try_again'));

		}
		redirect("backend/cms/news/");

	}

	public function search($query=null)
	{  
		$query = $this->input->get('search');
		$list = $this->user_model->get_user_list($query);
		$data= array();
		$data['total_count'] = count($list);
		$data['items'] = $list;
		echo json_encode($data);
	}

	public function send_news_letter(){ 
		$postArr = $this->input->post();
		$sent = false;
		$message = 'Sending successfull!';
		$this->form_validation->set_rules('newsletter_id',display('newsletter'),'required');
		if(isset($postArr['sending_mode']) && $postArr['sending_mode']=='selected_users'){
			$this->form_validation->set_rules('users[]',display('users'),'required');
		}
		
		$this->form_validation->set_rules('sending_mode',display('mode'),'required');
		
		//From Validation Check
		if (!$this->form_validation->run()) 
		{
			$this->session->set_flashdata('exception', display('please_try_again'));
			redirect("backend/cms/news/");
		}
		$news_details = $this->news_model->single($postArr['newsletter_id']);

		if(isset($postArr['sending_mode']) && $postArr['sending_mode']=='selected_users'){
			$users = $postArr['users'];
			foreach ($users as $key => $user_id) {
				$user_details = $this->user_model->single($user_id);
				$data['title']      = (empty($user_details->first_name) && empty($user_details->last_name))?"IMBA":$user_details->first_name . " " . $user_details->last_name;
				$data['to']         = trim($user_details->email);
				$data['bcc']		= 'edjowa.mail@gmail.com';
				// $data['bcc']		= 'kiran.brainium@gmail.com';
				$data['subject']    = 'News letter (' . date('d/m/Y') . ')';
				$data['message']    = "<br><b>News letter for " . $news_details->headline_en . "</b><br><br><p aline='justify'>".$news_details->article1_en."</p>";
				$message = 'Sending successfull!';
				$sent = $this->common_model->send_email($data);
			}
		}else{
			$users = $this->user_model->first_and_last_user_id();
			$exist = $this->newscron_model->get_today_cron();
			if(!$exist){
				$data = array();
				$data['news_id'] = $postArr['newsletter_id'];
				$data['user_start_id'] = $users->start_id;
				$data['user_last_id'] = $users->last_id;
				$data['sending_limit'] = isset($postArr['sending_limit'])?abs(trim($postArr['sending_limit'])):400;
				$success = $this->newscron_model->create($data);
				if($success){
					$sent=true;
					$message = 'Send success!';
				}else{
					$sent=false;
					$message = 'Sending failed!';
				}
			}else{
				$sent=false;
				$message = 'Error:Sending failed! you have already sent today!';	
			}
		}
		if($sent){			
			$this->session->set_flashdata('message', $message);
		}else{			
			$this->session->set_flashdata('exception', $message);
		}
		redirect("backend/cms/news/");
	}

	public function send_news_letter_cron()
	{  
		$corn_details = $this->newscron_model->all();
		if(count($corn_details)){
			foreach($corn_details as $corn){
				$user1 = $this->user_model->getUsers($corn->sending_limit, $corn->user_start_id);
				$user2 = $this->user_model->getPrevNotSentUsers($corn->user_start_id);
				$users = array_merge($user1,$user2);

				$news_details = $this->news_model->single($corn->news_id);
				if(count($users)>0){
					foreach ($users as $user) {
						if($user->id<=$corn->user_last_id){
							$data['title']      = (empty($user->first_name) && empty($user->last_name))?"IMBA":$user->first_name . " " . $user->last_name;
							// $data['to']         = trim($user->email);
							$data['to']         = 'kiran.brainium@gmail.com';
							// $data['bcc']		= 'edjowa.mail@gmail.com';
							// $data['bcc']		= 'kiran.brainium@gmail.com';
							$data['subject']    = 'News letter (' . date('d/m/Y') . ')';
							$data['message']    = "<br><b>News letter for " . $news_details->headline_en . "</b><br><br><p aline='justify'>".$news_details->article1_en."</p>";
							$message = 'Sending successfull!';
							$sent = $this->common_model->send_email($data);
							$user_corn_data=array();
							if($sent){
								$user_corn_data['user_id']=$user->id;
								$user_corn_data['cron_news_id']=$corn->news_id;
								$search = $this->newscronuser_model->search($user_corn_data);
								if($search){
									$user_corn_data['id']=$search->id;
									$user_corn_data['is_sent']='1';
									$user_corn_data['updated_at']=date('Y-m-d H:i:s');
									$this->newscronuser_model->update($user_corn_data);
								}else{
									$this->newscronuser_model->create($user_corn_data);
								}
							}else{
								$user_corn_data['user_id']=$user->id;
								$user_corn_data['cron_news_id']=$corn->news_id;
								$search = $this->newscronuser_model->search($user_corn_data);
								$user_corn_data['is_sent']='0';
								if(!$search){
									$this->newscronuser_model->create($user_corn_data);
								}
							}
						}
					}
					$corn_data=array();
					$init_last_user = end($user1);
					if($init_last_user->id<=$corn->user_last_id){
						$corn_data['id']=$corn->id;
						$corn_data['user_start_id']=$init_last_user->id;
						$corn_data['status']='1';
						$corn_data['updated_at']= date('Y-m-d H:i:s');
						$this->newscron_model->update($corn_data);
					}else{
						$corn_data['id']=$corn->id;
						$corn_data['status']='0';
						$corn_data['updated_at']= date('Y-m-d H:i:s');
						$this->newscron_model->update($corn_data);
					}
				}
			}
		}
	}
}
