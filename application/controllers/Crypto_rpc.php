<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include(APPPATH .'libraries/JsonrpcClient/JsonrpcClient.php');
class Crypto_rpc extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct(); 

        $this->load->model(array(
            'website/web_model',
            'common_model',
            'payment_model',

        ));

        // $this->load->library('JsonrpcClient');
        $this->load->library('encryption');

        $settings = $this->db->select('time_zone')->get('setting')->row();

        date_default_timezone_set($settings->time_zone);

    }
    public function index()
    {
        die();#tnhp
		try{
         $walletClient = new jsonRPCClient('http://nebl_user:27dTMgjckjhPKMKB125MQcMzhr@173.212.246.158:12035/');
         $walletClient->walletpassphrase("@@edjowaedjowaedjowaedjowa@@", 60);
         $wallet = $walletClient->getnewaddress();
         $wallet_privkey = $walletClient->dumpprivkey($wallet);
		 $walletClient->walletlock();
         echo "<pre>";print_r($wallet_privkey);
		 } catch(Exception $e){ print_r($e);
         }
    }

    public function depositupdate()
    {

        $this->encryption->initialize(
            array(
                    'cipher' => 'aes-256',
                    'mode' => 'ctr',
                    'key' => $this->config->item('enc_key_rpc_password'),
            )
        );
        $AllReceived = $this->web_model->getAllReceived();

        foreach ($AllReceived as $Received) {
            $coin = $this->web_model->getCoinById($Received->coin_id);
            $walletClient = new jsonRPCClient('http://'.$coin->username.':'.$this->encryption->decrypt($coin->password).'@'.$coin->host.':'.$coin->port.'/');
            $transaction = $walletClient->gettransaction($Received->txid);
            $confirmations = $transaction['confirmations']; //TODO

            if ($confirmations >= 3) {
                $address = $Received->address;
                $user_id = $this->web_model->getUserbyCoinAddr($address);

                $userdata = $this->db->select('*')
                            ->from('dbt_user')
                            ->where('id', $user_id->user_id)
                            ->get()->row();
                
                //User Balance Update
                $check_user_balance = $this->db->select('*')
                            ->from('dbt_balance')
                            ->where('user_id', $userdata->user_id)
                            ->where('currency_symbol', $coin->symbol)
                            ->get()->row();

                if ($check_user_balance) {
                    $new_balance = $check_user_balance->balance+$Received->amount;
                
                    $this->db->set('balance', $new_balance)
                            ->where('user_id', $userdata->user_id)
                            ->where('currency_symbol', $coin->symbol)
                            ->update("dbt_balance");

                    //Log Data 
                    $depositdata = array(
                        'user_id'            => $userdata->user_id,
                        'balance_id'         => $check_user_balance->id,
                        'currency_symbol'    => $coin->symbol,
                        'transaction_type'   => 'DEPOSIT',
                        'transaction_amount' => $Received->amount,
                        'transaction_fees'   => '0.00000000',
                        'ip'                 => '::1',
                        'date'               => date('Y-m-d h:i:s')
                    );

                    $this->payment_model->balancelog($depositdata);

                }else{

                    $balance = array(
                                'user_id'         => $userdata->user_id,
                                'currency_symbol' => $coin->symbol,
                                'balance'         => $Received->amount,
                                'last_update'     => date('Y-m-d h:i:s'),
                            );

                    $balance_id = $this->web_model->balanceAdd($balance);

                    //Log Data 
                    $depositdata = array(
                        'user_id'            => $userdata->user_id,
                        'balance_id'         => $balance_id,
                        'currency_symbol'    => $coin->symbol,
                        'transaction_type'   => 'DEPOSIT',
                        'transaction_amount' => $Received->amount,
                        'transaction_fees'   => '0.00000000',
                        'ip'                 => '::1',
                        'date'               => $balance['last_update']
                    );

                    $this->payment_model->balancelog($depositdata);
                }
                //wallet rcvd confirmed
                $this->db->set('confirmed', 1)
                            ->where('id', $Received->id)
                            ->where('user_id', $user_id->user_id)
                            ->update("wallet_receive");
            }
        }
    }

    public function withdrawupdate($coin=null)
    {
        $this->encryption->initialize(
            array(
                    'cipher' => 'aes-256',
                    'mode' => 'ctr',
                    'key' => $this->config->item('enc_key_rpc_password'),
            )
        );
		set_time_limit(0);
        if($coin){
            $CoinDetails = $this->web_model->getCoinBySymbol($coin);
            if($CoinDetails){
                if($CoinDetails->proof_type == "PoW"){
                    $AllSendbyCoin = $this->web_model->getAllSendbyCoin($CoinDetails->id);
                    foreach ($AllSendbyCoin as $Send) {
                        try{
                        $walletClient = new jsonRPCClient('http://'.$CoinDetails->username.':'.$this->encryption->decrypt($CoinDetails->password).'@'.$CoinDetails->host.':'.$CoinDetails->port.'/');
                        //$walletClient->walletlock();
                        $walletClient->walletpassphrase($this->encryption->decrypt($CoinDetails->passphrase), 60);
                        
                        $fee = $walletClient->estimatesmartfee(30, "CONSERVATIVE");
                        $feerate = $fee['feerate'];
                        $settx = $walletClient->settxfee($feerate);

						if($settx){
							$txId = $walletClient->sendtoaddress($Send->address, (float)$Send->amount);
						}

                        $walletClient->walletlock();
                        if($txId){
                            $this->web_model->updatewalletSend($txId, $Send->id);
                        }
                        } catch(Exception $e){ echo"<pre>";print_r($e);
                        }
                    }
                }else{
                    $AllSendbyCoin = $this->web_model->getAllSendbyCoin($CoinDetails->id);
                    foreach ($AllSendbyCoin as $Send) {
                        try{
                        $walletClient = new jsonRPCClient('http://'.$CoinDetails->username.':'.$this->encryption->decrypt($CoinDetails->password).'@'.$CoinDetails->host.':'.$CoinDetails->port.'/');
                        //$walletClient->walletlock();
                        $walletClient->walletpassphrase($this->encryption->decrypt($CoinDetails->passphrase), 60);
                        if($CoinDetails->isntp1 == 1){
                            $txId = $walletClient->sendntp1toaddress($Send->address, (float)$Send->amount, $CoinDetails->npt1tokenid);
                            $walletClient->walletlock();
                        }else{
                            $txId = $walletClient->sendtoaddress($Send->address, (float)$Send->amount);
                            $walletClient->walletlock();
                        }
                        
                        if($txId){
                            $this->web_model->updatewalletSend($txId, $Send->id);
                        }
                        } catch(Exception $e){ echo"<pre>";print_r($e);
                        }
                    }
                }
            }else{
                echo "Coin not activated";
            }

        }else{
            $AllSend = $this->web_model->getAllSend();
            foreach ($AllSend as $Send) {
                $coin = $this->web_model->getCoinById($Send->coin_id);
                $walletClient = new jsonRPCClient('http://'.$coin->username.':'.$this->encryption->decrypt($coin->password).'@'.$coin->host.':'.$coin->port.'/');
                //try{
                $walletClient->walletpassphrase($this->encryption->decrypt($coin->passphrase), 60);
                //echo "<pre>";print_r($walletClient);
                //} catch(Exception $e){ print_r($e);
                //}
                //TODO check if hot wallet has enough to send
                //echo"<pre>"; print_r(gettype($Send->amount));die();
                //$walletClient->settxfee($Send->coin_sending_fee); //priority fee
                
                $txId = $walletClient->sendtoaddress($Send->address, (float)$Send->amount);
                $this->web_model->updatewalletSend($txId, $Send->id);
                $walletClient->walletlock();
            }
        }
    }
	
	
    public function btccheck()
    {
        $this->encryption->initialize(
                array(
                        'cipher' => 'aes-256',
                        'mode' => 'ctr',
                        'key' => $this->config->item('enc_key_rpc_password'),
                )
            );
        try{
            $coin = $this->web_model->getCoinById(2211);
            $walletClient = new jsonRPCClient('http://'.$coin->username.':'.$this->encryption->decrypt($coin->password).'@'.$coin->host.':'.$coin->port.'/');
            $wallet = $walletClient->getbalance();
            print_r($wallet);
        } catch(Exception $e){ print_r($e);
        }
    }
}