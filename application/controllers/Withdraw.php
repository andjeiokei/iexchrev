<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include(APPPATH .'libraries/JsonrpcClient/JsonrpcClient.php');

class Withdraw extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct(); 

        $this->load->model(array(
            'website/web_model',
            'common_model',
            'payment_model',

        ));
        $this->load->library('encryption');

        $globdata['web_language']= $this->web_model->webLanguage();
        $globdata['social_link'] = $this->web_model->socialLink();
        $globdata['category']    = $this->web_model->categoryList();

        $this->load->vars($globdata);
        
    }

    public function index($coin = null){

        if (!($this->session->userdata('isLogIn') && $this->session->userdata('user_id')))
            redirect('login');

        //Theme Settings
        $theme['theme']          = $this->db->select('*')->from('dbt_theme')->where('status', 1)->get()->row();
        $data['theme_settings']  = $this->load->view("website/theme_scriptcss", $theme, true);


        $fees_data= $this->db->select('*')->from('dbt_fees')->get()->result_array();

        if(!empty($fees_data))
        {
            foreach ($fees_data as $key => $fees_details) {
                $data['fees_data'][$fees_details['currency_symbol']][$fees_details['level']] = array(
                                                                                                        "fees"=>$fees_details['fees'],

                                                                                                        "minimum_amount"=>$fees_details['minimum_amount']
                                                                                                    );
            }
        }
    
        if($coin){
            $coinExists = $this->db->select('*')->from('dbt_cryptocoin')->where('symbol', $coin)->get()->row();
            if($coinExists){
                $data['selectedCoin'] = $coin;
            }
        } else {
            $data['selectedCoin'] = 'BTC';
        }

        $user_id = $this->session->userdata('user_id');

        $userinfo = $this->web_model->retriveUserInfo();

        // echo "<pre>";print_r($userinfo);
        $name = $userinfo->first_name.' '.$userinfo->last_name;
        $coin_list = $this->web_model->activeCoin();
        $coinsInfo = [];

        $this->encryption->initialize(
            array(
                    'cipher' => 'aes-256',
                    'mode' => 'ctr',
                    'key' => $this->config->item('enc_key_rpc_password'),
            )
        );

        foreach($coin_list as $coin){
            $inOrders = 0;
            
            $balance = $this->web_model->checkBalance($coin->symbol);
            if($balance){
                $balance = $balance->balance;
            } else {
                $balance = 0;
            }

            $inOrdersQuery = $this->db->select_sum('total_amount')
                        ->from('dbt_biding')
                        ->where('user_id', $user_id)
                        ->where('bid_type','BUY')
                        ->where('status',2)
                        ->like('market_symbol', "_".$coin->symbol, 'before')
                        ->get()
                        ->row();
            //echo $this->db->last_query();



            $inOrdersQuery2nd = $this->db->select_sum('total_amount')
                        ->from('dbt_biding')
                        ->where('user_id', $user_id)
                        ->where('bid_type','SELL')
                        ->where('status',2)
                        ->like('market_symbol', $coin->symbol."_", 'after')
                        ->get()
                        ->row();
            //echo $this->db->last_query();
        
            if($inOrdersQuery->total_amount > 0){ 
                $inOrders = $inOrdersQuery->total_amount;
            } 
            else if($inOrdersQuery2nd->total_amount > 0){
                $inOrders = $inOrdersQuery2nd->total_amount;
            }



            // if($inOrders){
            //     $inOrders = $inOrders->total_amount;
            // } else {
            //     $inOrders = 0;
            // }
            $withdraw = $this->db->select('W.*, WS.txid, WS.confirmed')
                        ->from('dbt_withdraw W')
                        ->join('wallet_send WS',' WS.withdraw_id = W.id ','LEFT')
                        ->where('W.user_id', $userinfo->user_id)
                        ->where('W.currency_id', $coin->id)
                        ->order_by('W.id','desc')
                        ->get()
                        ->result_array();

            $coinsInfo[$coin->symbol] = [
                'coin_id' => $coin->id,
                'coin_dp' => $coin->image,
                'coin_name' => $coin->coin_name,
                'ticker' => $coin->symbol,
                'balance' => $balance,
                'inOrders' => $inOrders,
                'withdraw' => $withdraw,
            ];        
        }
        $data['coins'] = $coinsInfo;
        $data['name'] = $name;
        $this->load->view('website/header', $data);     
        $this->load->view('website/new-withdraw', $data);
        $this->load->view('website/footer', $data);
    }

    function submit_withdraw_data()
    {
        $balance = $this->web_model->checkBalance($this->input->post('currency_name'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('address', 'address', 'required',
            array('required' => 'Please enter %s.')
        );
        $this->form_validation->set_rules('amount', 'amount', 'required|greater_than[0]',
            array(
                    'required' => 'Please enter %s.',
                    'greater_than' => "Amount must be greaterthan 0."
                )
        );

        $this->form_validation->set_rules('amount_deduct_fee', 'amount after deduction', 'required',
            array('required' => 'The %s is not available.')
        );
        

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('error_message',validation_errors('<li>', '</li>'));
            redirect(base_url('withdraw'));
        }
        else
        {

            if(!empty($this->input->post('currency_name')))
            {
                $coin_details = $this->web_model->coin_details(array("symbol"=>$this->input->post('currency_name')));
            }
            

            $data = array(
                "user_id"           => $this->session->userdata('user_id'),
                "wallet_id"         => 0,
                "currency_id"       => $coin_details['id'],
                "currency_symbol"   => $this->input->post('currency_name'),
                "amount"            => $this->input->post('amount_deduct_fee'),
                "method"            => "token",
                'address'           => $this->input->post('address'),
                "fees_amount"       => ( $this->input->post('amount') - $this->input->post('amount_deduct_fee') ),
                "request_date"      => date("Y-m-d H:i:s"),
                "status"            => 3,
                "ip"                => $this->input->ip_address(),
            );
            $inserted_id = $this->web_model->withdraw($data);
            if( $inserted_id['id'] > 0)
            {
                $appSetting = $this->common_model->get_setting();
               // $this->load->library('email');

                //$config['protocol'] = 'smtp';
                //$config['mailpath'] = '/usr/sbin/sendmail';
                //$config['charset'] = 'iso-8859-1';
                //$config['wordwrap'] = TRUE;
                //$config['mailtype'] = 'html';


				
                //$this->email->initialize($config);

                //$this->email->from($appSetting->email, $appSetting->title);
                //$this->email->to($this->session->userdata('email'));

                //$this->email->subject('Withdraw');
                //$this->email->message('Your request will be initiated after confirming your request. Please confirm your request from below link:<br/>
                //<a href="'.base_url().'withdraw/confirm/'.urlencode( base64_encode( $inserted_id['id']."@@@".$this->session->userdata('user_id') ) ).'">Confirm</a>
                //');
                //if($this->email->send()){
                //    $this->session->set_flashdata('success_message',"Your withdraw request has been submitted successfully.");
				//	echo 'Hello16584';
                 //   redirect(base_url('withdraw'));
               // }
			   

            $post = array(
                'title'             => 'Withdraw',
                'subject'           => 'Withdraw!',
                'to'                => $this->session->userdata('email'),
                'message'           => 'Your request will be initiated after confirming your request. Please confirm your request from below link:<br/>
                <a href="'.base_url().'withdraw/confirm/'.urlencode( base64_encode( $inserted_id['id']."@@@".$this->session->userdata('user_id') ) ).'">Confirm</a>'
				);




            //Send Mail Password Reset Verification
            $send = $this->common_model->send_email_phpmailer($post);
			if(isset($send)){
				$this->session->set_flashdata('success_message',"Your withdraw request has been submitted successfully.");
				//	echo 'Hello16584';
                 redirect(base_url('withdraw'));
			}
            }
            else {
                $this->session->set_flashdata('error_message',"<li>Sorry failed to save your request.</li>");
                redirect(base_url('withdraw'));
            }
        }

    
    }

    function confirm($url_data = NULL)
    {
        if(!empty($url_data))
        {
            $data = base64_decode( urldecode($url_data) );
            $data = explode("@@@",$data);
            $id = $data[0];
            $user_id = $data[1];

            if($id > 0 && !empty($user_id))
            {
                 
                $withdraw_details = $this->db
                ->where(array(
                'id'=>$id,
                'user_id'=>$user_id,
                'status'=>3,
                ))->get('dbt_withdraw')
                ->row();

                if(!empty($withdraw_details))
                {
                    $check_user_balance = $this->db->select('*')->from('dbt_balance')->where('user_id', $user_id)->where('currency_symbol', $withdraw_details->currency_symbol)->get()->row();


                    $new_balance = ( $check_user_balance->balance - ( $withdraw_details->amount + $withdraw_details->fees_amount ) );


                    if( $check_user_balance->balance >= ( $withdraw_details->amount + $withdraw_details->fees_amount ) )
                    {
                        $this->db->set('balance', $new_balance)->where('user_id', $user_id)->where('currency_symbol', $withdraw_details->currency_symbol)->update("dbt_balance");

                        if( $this->db->affected_rows() > 0)
                        {
                            $post_data = array('status'=>2);
                            $this->db
                            ->where(array(
                                'id'=>$id,
                                'user_id'=>$user_id,
                                'status'=>3,
                                ))
                            ->update('dbt_withdraw', $post_data);

                            $this->session->set_flashdata('success_message',"Your request has been initiated.");
                            redirect(base_url('request_action'));

                        }
                        else {
                            $this->session->set_flashdata('error_message',"Failed to initiate your withdraw request. Please contact with admin.");
                            redirect(base_url('request_action'));
                        }
                        
                    }
                    else {

                        $post_data = array('status'=>0);
                        $this->db
                        ->where(array(
                            'id'=>$id,
                            'user_id'=>$user_id,
                            'status'=>3,
                            ))
                        ->update('dbt_withdraw', $post_data);

                        $this->session->set_flashdata('error_message',"Failed to initiat your request. Your current request amount is exceeding your wallet balance.");
                        redirect(base_url('request_action'));
                    }
                }
                else {
                    $this->session->set_flashdata('error_message',"Sorry the withdraw request is not active.");
                    redirect(base_url('request_action'));
                }

        
            }
            else {
                $this->session->set_flashdata('error_message',"Data mismatch from your request.");
                redirect(base_url('request_action'));
            }
           

        }
        else {
            redirect(base_url());
        }
        
    }

}
