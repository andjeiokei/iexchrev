<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include(APPPATH .'libraries/JsonrpcClient/JsonrpcClient.php');

class Deposit extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct(); 

        $this->load->model(array(
            'website/web_model',
            'common_model',
            'payment_model',

        ));
        $this->load->library('encryption');
        $globdata['web_language']= $this->web_model->webLanguage();
        $globdata['social_link'] = $this->web_model->socialLink();
        $globdata['category']    = $this->web_model->categoryList();

        $this->load->vars($globdata);
    }

    public function deposit($coin = null){
        if (!($this->session->userdata('isLogIn') && $this->session->userdata('user_id')))
            redirect('login');

        //Theme Settings
        $theme['theme']          = $this->db->select('*')->from('dbt_theme')->where('status', 1)->get()->row();
        $data['theme_settings']  = $this->load->view("website/theme_scriptcss", $theme, true);
        
        if($coin){
            $coinExists = $this->db->select('*')->from('dbt_cryptocoin')->where('symbol', $coin)->get()->row();
            if($coinExists){
                $data['selectedCoin'] = $coin;
            }
        } else {
            $data['selectedCoin'] = 'BTC';
        }

        $user_id = $this->session->userdata('user_id');

        $userinfo = $this->web_model->retriveUserInfo();

        // echo "<pre>";print_r($userinfo);
        $name = $userinfo->first_name.' '.$userinfo->last_name;
        $coin_list = $this->web_model->activeCoin();
        $coinsInfo = [];

        foreach($coin_list as $coin){
            $inOrders = 0;
            $balance = $this->web_model->checkBalance($coin->symbol);
            if($balance){
                $balance = $balance->balance;
            } else {
                $balance = 0;
            }

            
            $inOrdersQuery = $this->db->select_sum('total_amount')
                        ->from('dbt_biding')
                        ->where('user_id', $user_id)
                        ->where('bid_type','BUY')
                        ->where('status',2)
                        ->like('market_symbol', "_".$coin->symbol, 'before')
                        ->get()
                        ->row();
            //echo $this->db->last_query();



            $inOrdersQuery2nd = $this->db->select_sum('total_amount')
                        ->from('dbt_biding')
                        ->where('user_id', $user_id)
                        ->where('bid_type','SELL')
                        ->where('status',2)
                        ->like('market_symbol', $coin->symbol."_", 'after')
                        ->get()
                        ->row();
            //echo $this->db->last_query();
        
            if($inOrdersQuery->total_amount > 0){ 
                $inOrders = $inOrdersQuery->total_amount;
            } 
            else if($inOrdersQuery2nd->total_amount > 0){
                $inOrders = $inOrdersQuery2nd->total_amount;
            }


            $deposits = $this->db->select('*')
                        ->from('wallet_receive')
                        ->where('user_id', $userinfo->id)
                        ->where('coin_id', $coin->id)
                        ->get()
                        ->result();
            $coinsInfo[$coin->symbol] = [
                'coin_id' => $coin->id,
                'coin_dp' => $coin->image,
                'coin_name' => $coin->coin_name,
                'ticker' => $coin->symbol,
                'balance' => $balance,
                'inOrders' => $inOrders,
                'deposits' => $deposits,
            ];
            
        
        }
        $data['coins'] = $coinsInfo;
        $data['name'] = $name;

        // echo "<pre>";print_r($data);exit;
        $this->load->view('website/header', $data);     
        $this->load->view('website/new-deposit', $data);
        $this->load->view('website/footer', $data);
    }

    function getAddress(){
        $data = $this->input->get();
        $coinSymbol = $data['symbol'];
        $coinInfo = $this->db->from('dbt_cryptocoin')
                    ->where('symbol', $coinSymbol)
                    ->get()->row();

        // echo "<pre>";print_r($coinInfo);
        $userinfo = $this->web_model->retriveUserInfo();

        if($coinInfo){
            $coinId = $coinInfo->id;
            if($coinInfo->isntp1 == 1){
                $coinRowInAddress = $this->db->select('*')
                ->from('address')
                ->where('user_id', $userinfo->id)
                ->where('isntp1', 1)
                ->get()
                ->row();
            } else {
                $coinRowInAddress = $this->db->select('*')
                ->from('address')
                ->where('user_id', $userinfo->id)
                ->where('coin_id', $coinId)
                ->get()
                ->row();
            }

            if($coinRowInAddress){
                echo json_encode(['result'=> true, 'address' => $coinRowInAddress->address]);
            } else {
                try{
                    $this->encryption->initialize(
                        array(
                                'cipher' => 'aes-256',
                                'mode' => 'ctr',
                                'key' => $this->config->item('enc_key_rpc_password'),
                        )
                    );
                    $walletClient = new jsonRPCClient('http://'.$coinInfo->username.':'.$this->encryption->decrypt($coinInfo->password).'@'.$coinInfo->host.':'.$coinInfo->port.'/');
                    $walletClient->walletlock();
                    $walletClient->walletpassphrase($this->encryption->decrypt($coinInfo->passphrase), 60);
                    $wallet = $walletClient->getaccountaddress((string)$userinfo->id);
                    $userCoinAddress = [
                        'user_id' => $userinfo->id,
                        'coin_id' => $coinInfo->id,
                        'address' => $wallet,
                        'isntp1' => $coinInfo->isntp1,
                        'last_deposit' => 0,
                        'created_at' => Date('Y-m-d H:i:s'),
                    ];
                    $walletClient->walletlock();
                    $this->db->insert('address', $userCoinAddress);
                    echo json_encode(['result'=> true, 'address' => $wallet]);
                } catch(Exception $e){
                    echo json_encode(['result'=> false]);
                }
            }
        } else {
            echo json_encode(['result'=> false]);
        }
    }
}
