<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Curl {

	public $error;

	function __construct() {}

	function Get($url = "http://hostname.x/api.php?q=jabadoo&txt=gin", $forceSsl = false,$cookie = "", $session = true){
		// $url = $url . "?". http_build_query($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);        
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		if($session){
			curl_setopt($ch, CURLOPT_COOKIESESSION, true );
			curl_setopt($ch , CURLOPT_COOKIEJAR, 'cookies.txt');
			curl_setopt($ch , CURLOPT_COOKIEFILE, 'cookies.txt');
		}
		if($forceSsl){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // 1, 2
		}
		if(!empty($cookie)){            
			curl_setopt($ch, CURLOPT_COOKIE, $cookie); // "token=12345"
		}
		$info = curl_getinfo($ch);
		$res = curl_exec($ch);        
		if (curl_error($ch)) {
			$this->error = curl_error($ch);
			throw new Exception($this->error);
		}else{
			curl_close($ch);
			return $res;
		}        
	}

	function GetArray($url = "http://hostname.x/api.php", $data = array("name" => "Max", "age" => "36"), $forceSsl = false, $cookie = "", $session = true){
		$url = $url . "?". http_build_query($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		if($session){
			curl_setopt($ch, CURLOPT_COOKIESESSION, true );
			curl_setopt($ch , CURLOPT_COOKIEJAR, 'cookies.txt');
			curl_setopt($ch , CURLOPT_COOKIEFILE, 'cookies.txt');
		}
		if($forceSsl){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // 1, 2
		}
		if(!empty($cookie)){
			curl_setopt($ch, CURLOPT_COOKIE, $cookie); // "token=12345"
		}
		$info = curl_getinfo($ch);
		$res = curl_exec($ch);        
		if (curl_error($ch)) {
			$this->error = curl_error($ch);
			throw new Exception($this->error);
		}else{
			curl_close($ch);
			return $res;
		}        
	}

	function PostJson($url = "http://hostname.x/api.php", $data = array("name" => "Max", "age" => "36"), $forceSsl = false, $cookie = "", $session = true){
		$data = json_encode($data);
		$ch = curl_init($url);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		if($session){
			curl_setopt($ch, CURLOPT_COOKIESESSION, true );
			curl_setopt($ch , CURLOPT_COOKIEJAR, 'cookies.txt');
			curl_setopt($ch , CURLOPT_COOKIEFILE, 'cookies.txt');
		}
		if($forceSsl){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // 1, 2
		}
		if(!empty($cookie)){
			curl_setopt($ch, CURLOPT_COOKIE, $cookie); // "token=12345"
		}
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer helo29dasd8asd6asnav7ffa',                                                      
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data))                                                                       
		);        
		$res = curl_exec($ch);
		if (curl_error($ch)) {
			$this->error = curl_error($ch);
			throw new Exception($this->error);
		}else{
			curl_close($ch);
			return $res;
		} 
	}

	function Post($url = "http://hostname.x/api.php", $data = array("name" => "Max", "age" => "36"), $files = array('ads/ads0.jpg', 'ads/ads1.jpg'), $forceSsl = false, $cookie = "", $session = true){

		if(!empty($files))
		{
			foreach ($files as $k => $v) {
				$f = realpath($v);
				if(file_exists($f)){
					$fc = new CurlFile($f, mime_content_type($f), basename($f)); 
					$data["file[".$k."]"] = $fc;
				}
			}
		}

		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");        
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);    
		//curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false); // !!!! required as of PHP 5.6.0 for files !!!
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)");
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		// if($session){
		// 	curl_setopt($ch, CURLOPT_COOKIESESSION, true );
		// 	curl_setopt($ch , CURLOPT_COOKIEJAR, 'cookies.txt');
		// 	curl_setopt($ch , CURLOPT_COOKIEFILE, 'cookies.txt');
		// }
		// if($forceSsl){
		// 	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		// 	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // 1, 2
		// }
		// if(!empty($cookie)){
		// 	curl_setopt($ch, CURLOPT_COOKIE, $cookie); // "token=12345"
		// }
		$res = curl_exec($ch);
		if (curl_error($ch)) {
			$this->error = curl_error($ch);
			throw new Exception($this->error);
		}else{
			curl_close($ch);
			return $res;
		} 
	}
}