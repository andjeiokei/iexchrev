<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('adshow')) {

    function adshow($data = array())
    {
        $ci =& get_instance();
        $ci->load->database();
        $table  = '';

        $width  = ($data['width'] > 0)? 'width:'.$data['width'].'px;':'width:100%;';
        $height = ($data['height'] > 0)? 'height:'.$data['height'].'px;':'';
        $client = $data['client'];
        $slot   = ' data-ad-slot="'.$data['slot'].'"';
        $format = ($data['format'] == 'auto')? ' data-ad-format="auto"':'';
        $responsive = ($data['responsive'] == 'auto')? ' data-full-width-responsive="true"':'';
        

        if(empty($client))
        {
            return false;
        }
        else {
            $html = '
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                    style="display:inline-block;'.$width.$height.'"
                    data-ad-client="'.$client.'"
                    '.$slot.$format.'></ins>
                <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script>';
        }
        
        return $html;

    }

}