<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('outputCsv'))
{
    function outputCsv($fileName, $assocDataArray)
    {
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        

        $fp = fopen('php://output', 'wb');
        foreach ( $assocDataArray as $line ) {
            //$val = explode(",", $line);
            fputcsv($fp, $line);
        }
        fclose($fp);
    }
}


/* End of file csv_helper.php */
/* Location: ./system/helpers/csv_helper.php */